# laJusta-backend

El backend del proyecto La Justa

# Como correr el proyecto

Una vez descargado posicionarse sobre la raiz del proyecto, debemos modificar el archivo application.properties para agregar el connector al a base de datos MariaDB (dato importante la version de MariaDB debe ser 10.2 o superior, la recomendada es 10.4.7). 
En el archivo application.gradle tenemos que setear la properties de conexion de base de datos:

```plaintext
  spring.datasource.url=jdbc:mariadb://<tu_host>:3306/<dataSource>
  spring.datasource.username=<usuario>
  spring.datasource.password=<contraseña>
  useMysqlMetadata=true
  spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
  spring.jpa.show-sql=true
  ```


ver el file gradlew.bat, entonces correr el siguiente comando:

```plaintext
  ./gradlew bootRun
  ```

Vamos a ver mucho output de consola, pero el que nos importa es el siguiente:

```plaintext
  o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
  ```

esto nos dice que la aplicacion esta lista para ejecutarse ingresando desde un browser a la URL:

```plaintext
  http://localhost:8080
  ```

Cuando se corre desde un IDE como intellJ, tenemos que agregar las siguientes variables de Entorno para que levante:

``` 
    SPRING_DATASOURCE_PASSWORD=lajusta;
    SPRING_DATASOURCE_URL=jdbc:mariadb://localhost:3306/la_justa;
    LA_JUSTA_IMAGE_FOLDER_PATH=/home/francisco/Documents/la_justa_images;
    SPRING_DATASOURCE_USERNAME=lajusta;
    CROSS_ORIGIN=http://localhost:4401;
    spring_profiles_active=dev
```
FROM openjdk:14 AS BUILD_IMAGE
ENV APP_HOME=/root
RUN mkdir -p $APP_HOME/src/main/java
WORKDIR $APP_HOME
COPY build.gradle.kts $APP_HOME
COPY gradlew $APP_HOME
COPY settings.gradle.kts $APP_HOME
COPY gradle $APP_HOME/gradle
COPY src $APP_HOME/src
RUN ./gradlew --no-daemon build

FROM openjdk:11.0.9.1-jre
ENV TZ="America/Argentina/Buenos_Aires"
WORKDIR /root/
COPY --from=BUILD_IMAGE /root/build/libs/laJusta-0.0.1-SNAPSHOT.jar .
CMD ["java","-jar","laJusta-0.0.1-SNAPSHOT.jar"]
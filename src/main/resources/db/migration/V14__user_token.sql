create table user_token
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    username        varchar(50) unique default null,
    token           varchar(250),
    time_to_live    timestamp default now(),
    CONSTRAINT      user_token_pk PRIMARY KEY (id)
);
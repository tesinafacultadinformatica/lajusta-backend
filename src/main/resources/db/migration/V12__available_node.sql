create table available_node
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    general_id      mediumint unsigned null default null,
    node_id         mediumint unsigned null default null,
    date_open       timestamp null default null,
    date_close      timestamp null default null,
    CONSTRAINT      date_node_pk PRIMARY KEY (id),
    CONSTRAINT      date_node_general_pk FOREIGN KEY (general_id) REFERENCES general (id),
    CONSTRAINT      date_node_node_pk FOREIGN KEY (node_id) REFERENCES node (id)
);
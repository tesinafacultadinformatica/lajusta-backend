create table producer_image
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    images_id        mediumint unsigned null default null,
    producer_id    mediumint unsigned null default null,
    CONSTRAINT      producer_image_pk PRIMARY KEY (id),
    CONSTRAINT      producer_image_image_pk FOREIGN KEY (images_id) REFERENCES image (id),
    CONSTRAINT      producer_image_producer_pk FOREIGN KEY (producer_id) REFERENCES producer (id)
);
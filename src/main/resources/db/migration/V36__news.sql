create table news
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    title           varchar(50) null default null,
    subtitle        varchar(50) null default null,
    text            varchar(500) null default null,
    image_id        mediumint unsigned null default null,
    url             varchar(200) null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      news_pk PRIMARY KEY (id),
    CONSTRAINT      news_image_pk FOREIGN KEY (image_id) REFERENCES image (id)
);

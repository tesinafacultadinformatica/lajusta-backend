create table product_categories
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    categories_id   mediumint unsigned null default null,
    products_id     mediumint unsigned null default null,
    CONSTRAINT      category_product_pk PRIMARY KEY (id),
    CONSTRAINT      category_product_category_pk FOREIGN KEY (categories_id) REFERENCES category (id),
    CONSTRAINT      category_product_product_pk  FOREIGN KEY (products_id) REFERENCES product (id)
);
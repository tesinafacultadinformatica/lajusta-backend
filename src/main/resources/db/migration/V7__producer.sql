create table producer
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    address_id      mediumint unsigned null default null,
    first_name      varchar(50) null default null,
    last_name       varchar(50) null default null,
    email           varchar(200) null default null,
    description     varchar(350) default null,
    phone           varchar(20) default null,
    youtube         varchar(20) default null,
    is_company      boolean default false,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      producer_pk PRIMARY KEY (id),
    CONSTRAINT      producer_address_pk FOREIGN KEY (address_id) REFERENCES address (id)
);
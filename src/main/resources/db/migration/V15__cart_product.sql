create table cart_product
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    cart_id         mediumint unsigned null default null,
    product_id      mediumint unsigned null default null,
    quantity        smallint unsigned default 0,
    price           double default 0.0,
    is_canceled     tinyint(1) default 0,
    CONSTRAINT      node_pk PRIMARY KEY (id),
    CONSTRAINT      cart_product_cart_pk FOREIGN KEY (cart_id) REFERENCES cart (id),
    CONSTRAINT      cart_product_product_pk FOREIGN KEY (product_id) REFERENCES product (id)
);
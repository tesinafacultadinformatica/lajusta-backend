create table tag
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    name            varchar(70) null default null,
    CONSTRAINT      tag_pk PRIMARY KEY (id)
);
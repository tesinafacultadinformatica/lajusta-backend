create table newsletter
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    email           varchar(200) unique default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      newsletter_pk PRIMARY KEY (id)
);
create table cart
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    user_id         mediumint unsigned null default null,
    node_available_id    mediumint unsigned null default null,
    sale_date       timestamp null default null,
    possible_delivery_date timestamp null default null,
    delivered       timestamp null default null,
    total           double null default null,
    approved_at     timestamp null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      cart_pk PRIMARY KEY (id),
    CONSTRAINT      cart_user_pk FOREIGN KEY (user_id) REFERENCES user (id),
    CONSTRAINT      cart_node_available_pk FOREIGN KEY (node_available_id) REFERENCES available_node (id)
);
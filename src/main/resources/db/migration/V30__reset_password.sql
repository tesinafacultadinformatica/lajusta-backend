create table reset_password
(
    email           varchar(200) unique default null,
    reset_code      varchar(256) null default null,
    time_to_live    timestamp null default null,
    primary key(email)
);

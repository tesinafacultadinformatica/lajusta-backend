create table role
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    description     varchar(200) null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT role_pk PRIMARY KEY (id)
);
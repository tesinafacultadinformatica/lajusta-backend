create table category
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    parent_id       mediumint unsigned null default null,
    image_id        mediumint unsigned null default null,
    name            varchar(50) null default null,
    level           tinyint unsigned null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      category_pk PRIMARY KEY (id),
    CONSTRAINT      category_parent_pk FOREIGN KEY (parent_id) REFERENCES category (id),
    CONSTRAINT      category_image_pk FOREIGN KEY (image_id) REFERENCES image (id)
);
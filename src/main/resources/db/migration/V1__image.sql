create table image
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    route           varchar(100) null default null,
    type            varchar(5) null default null,
    is_main         tinyint(1) default 0,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT image_pk PRIMARY KEY (id)
);
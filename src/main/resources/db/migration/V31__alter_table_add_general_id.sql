
ALTER TABLE cart ADD general_id  mediumint unsigned null default null,
ADD CONSTRAINT      cart_general_pk FOREIGN KEY (general_id) REFERENCES general (id);

ALTER TABLE balance ADD general_id  mediumint unsigned null default null,
ADD total_purchase_producer  double null default null,
ADD CONSTRAINT      balance_general_pk FOREIGN KEY (general_id) REFERENCES general (id);


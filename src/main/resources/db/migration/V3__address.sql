create table address
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    street          varchar(200) default null,
    number          varchar(10) default null,
    floor           varchar(3) default null,
    apartment       varchar(3) default null,
    description     varchar(350) default null,
    latitude        double null default null,
    longitude       double null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      address_pk PRIMARY KEY (id)
);
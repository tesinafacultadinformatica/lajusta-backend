create table general
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    user_id         mediumint unsigned null default null,
    date_down_page  timestamp null default null,
    date_start_up_page  timestamp null default null,
    updated_at      timestamp null default null,
    closed_at       timestamp null default null,
    created_at      timestamp null default null,
    deleted_at      timestamp null default null,
    CONSTRAINT      general_pk PRIMARY KEY (id),
    CONSTRAINT      general_user_pk FOREIGN KEY (user_id) REFERENCES user (id)
);
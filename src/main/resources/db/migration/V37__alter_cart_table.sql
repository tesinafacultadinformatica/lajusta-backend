alter table la_justa.cart drop delivered;
alter table la_justa.cart drop approved_at;

alter table la_justa.cart add column canceled boolean default false;

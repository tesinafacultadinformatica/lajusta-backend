create table producer_tag_producer
(
    tags_id                 mediumint unsigned null default null,
    producer_id             mediumint unsigned null default null,
    CONSTRAINT              producer_tag_producer_pk FOREIGN KEY (tags_id) REFERENCES tag (id),
    CONSTRAINT              producer_tag_producer_producer_pk FOREIGN KEY (producer_id) REFERENCES producer (id)
);
create table staff
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    user_id         mediumint unsigned null default null,
    description     varchar(500) null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      staff_pk PRIMARY KEY (id),
    CONSTRAINT      staff_user_pk FOREIGN KEY (user_id) REFERENCES user (id)
);
create table balance
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    total_sale      double null default null,
    total_expenses  double null default null,
    date_balance    timestamp null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      balance_pk PRIMARY KEY (id)
);
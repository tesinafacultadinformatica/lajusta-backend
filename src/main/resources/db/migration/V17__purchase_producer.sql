create table purchase_producer
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    product_id      mediumint unsigned null default null,
    producer_id     mediumint unsigned null default null,
    general_id      mediumint unsigned null default null,
    quantity        smallint null default null,
    price           double null default null,
    differencial    double null default null,
    total           double null default null,
    aditional       smallint null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      purchase_producer_pk PRIMARY KEY (id),
    CONSTRAINT      purchase_producer_product_pk FOREIGN KEY (product_id) REFERENCES product (id),
    CONSTRAINT      purchase_producer_general_pk FOREIGN KEY (general_id) REFERENCES general (id),
    CONSTRAINT      purchase_producer_producer_pk FOREIGN KEY (producer_id) REFERENCES producer (id)
);
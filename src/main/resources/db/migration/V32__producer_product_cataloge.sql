create table producer_product_cataloge
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    product_id      mediumint unsigned null default null,
    producer_id     mediumint unsigned null default null,
    price           double null default null,
    quantity        smallint unsigned default 0,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      producer_product_cataloge_pk PRIMARY KEY (id),
    CONSTRAINT      producer_product_cataloge_product_pk FOREIGN KEY (product_id) REFERENCES product (id),
    CONSTRAINT      producer_product_cataloge_producer_pk FOREIGN KEY (producer_id) REFERENCES producer (id)
);
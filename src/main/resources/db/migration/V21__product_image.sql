create table product_image
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    product_id      mediumint unsigned null default null,
    images_id        mediumint unsigned null default null,
    CONSTRAINT      product_image_pk PRIMARY KEY (id),
    CONSTRAINT      product_image_product_pk FOREIGN KEY (product_id) REFERENCES product (id),
    CONSTRAINT      product_image_image_pk FOREIGN KEY (images_id) REFERENCES image (id)
);
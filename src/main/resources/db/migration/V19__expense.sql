create table expense
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    general_id      mediumint unsigned null default null,
    staff_id        mediumint unsigned null default null,
    description     varchar(500) null default null,
    quantity        mediumint null default null,
    price           double null default null,
    total           double null default null,
    date            timestamp null default null,
    balance_date    timestamp null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      expense_pk PRIMARY KEY (id),
    CONSTRAINT      expense_general_pk FOREIGN KEY (general_id) REFERENCES general (id),
    CONSTRAINT      expense_staff_pk FOREIGN KEY (staff_id) REFERENCES staff (id)
);
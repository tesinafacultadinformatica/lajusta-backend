create table unit
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    code            varchar(50) null default null,
    description     varchar(500) null default null,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      unit_pk PRIMARY KEY (id)
);
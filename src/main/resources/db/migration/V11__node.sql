create table node
(
    id              mediumint unsigned not null AUTO_INCREMENT,
    address_id      mediumint unsigned null default null,
    name            varchar(50) null default null,
    address         varchar(200) default null,
    image_id        mediumint unsigned null default null,
    description     varchar(500) null default null,
    phone           varchar(20) default null,
    has_fridge      tinyint(1) default 0,
    deleted_at      timestamp null default null,
    updated_at      timestamp null default null,
    created_at      timestamp null default null,
    CONSTRAINT      node_pk PRIMARY KEY (id),
    CONSTRAINT      node_image_pk FOREIGN KEY (image_id) REFERENCES image (id),
    CONSTRAINT      node_address_pk FOREIGN KEY (address_id) REFERENCES address (id)
);
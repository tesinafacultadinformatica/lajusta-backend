package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.CategoryDTO
import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.NodeDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.CategoryService
import edu.unlp.la.justa.services.NodeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/node")
class NodeController(@Autowired val nodeService: NodeService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?) : ResponseEntity<Any> {
        if(filterStr == null && rangeStr == null && sortStr == null){
            return ResponseEntity.ok().body(nodeService.getAll())
        } else {
            return ResponseEntity.ok().body(nodeService.getAll(filterStr, rangeStr, sortStr))
        }
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(nodeService.get(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestBody nodeDTO: NodeDTO): ResponseEntity<NodeDTO> {
        try {
            return ResponseEntity.ok().body(nodeService.save(nodeDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(nodeService.save(nodeDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(nodeService.activate(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/node/activate", Date()))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody nodeDTO: NodeDTO): ResponseEntity<NodeDTO> {
        try {
            return ResponseEntity.ok().body(nodeService.update(nodeDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(nodeService.save(nodeDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(nodeService.delete(id))
}
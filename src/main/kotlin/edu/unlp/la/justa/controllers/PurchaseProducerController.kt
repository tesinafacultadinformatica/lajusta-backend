package edu.unlp.la.justa.controllers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.PurchaseProducerDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.PurchaseProducerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/purchase")
class PurchaseProducerController(@Autowired val purchaseProducerService: PurchaseProducerService) {

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?,
               @RequestParam(required = false, name="properties") properties: String?): ResponseEntity<Any> {
        var prop: Set<PropertiesFilterDTO>? = null
        properties?.let { prop = jacksonObjectMapper().readValue(it) }
        return ResponseEntity.ok().body(purchaseProducerService.getAll(filterStr, rangeStr, sortStr, prop))
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(purchaseProducerService.get(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestBody purchaseProducerDTO: PurchaseProducerDTO): ResponseEntity<PurchaseProducerDTO> {
        try {
            return ResponseEntity.ok().body(purchaseProducerService.save(purchaseProducerDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(purchaseProducerService.save(purchaseProducerDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody purchaseProducerDTO: PurchaseProducerDTO): ResponseEntity<PurchaseProducerDTO> {
        try {
            return ResponseEntity.ok().body(purchaseProducerService.update(purchaseProducerDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(purchaseProducerService.save(purchaseProducerDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(purchaseProducerService.delete(id))
}
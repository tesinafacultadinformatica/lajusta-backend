package edu.unlp.la.justa.controllers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.ProducerDTO
import edu.unlp.la.justa.dtos.ProducerYieldDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.ProducerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/producer")
class ProducerController(@Autowired val producerService: ProducerService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?,
               @RequestParam(required = false, name="properties") properties: String?): ResponseEntity<Any> {
        var prop: Set<PropertiesFilterDTO>? = null
        properties?.let { prop = jacksonObjectMapper().readValue(it) }
        val producers: Any?
        if(filterStr == null && rangeStr == null && sortStr == null && properties == null) {
            producers = producerService.getAll()
        } else {
            producers = producerService.getAll(filterStr, rangeStr, sortStr, prop)
        }
        return ResponseEntity.ok().body(producers)
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(producerService.get(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestBody producerDTO: ProducerDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(producerService.save(producerDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(producerService.activate(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/producer/activate", Date()))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody producerDTO: ProducerDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(producerService.update(producerDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping("/cataloge")
    fun updateProducerYield(@RequestBody producerYieldDto: ProducerYieldDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(producerService.updateProducerYield(producerYieldDto))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @GetMapping("/cataloge/{id}")
    fun getProducerYield(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(producerService.getProducerYield(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(producerService.delete(id))
}
package edu.unlp.la.justa.controllers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import edu.unlp.la.justa.dtos.CategoryDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.CategoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/category")
class CategoryController(@Autowired val categoryService: CategoryService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?,
               @RequestParam(required = false, name="properties") properties: String?): ResponseEntity<Any> {
        var prop: Set<PropertiesFilterDTO>? = null
        properties?.let { prop = jacksonObjectMapper().readValue(it) }
        val categories: Any = if(filterStr == null && rangeStr == null && sortStr == null && properties == null) categoryService.getAll() else categoryService.getAll(filterStr, rangeStr, sortStr, prop)
        return ResponseEntity.ok().body(categories)
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(categoryService.get(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestBody categoryDTO: CategoryDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(categoryService.save(categoryDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        return ResponseEntity.ok().body(categoryService.activate(id))
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody categoryDTO: CategoryDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(categoryService.update(categoryDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(categoryService.delete(id))

}
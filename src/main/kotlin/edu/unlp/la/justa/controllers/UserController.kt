package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.UserDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/user")
class UserController(@Autowired val userService: UserService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name = "sort") sortStr: String?) = ResponseEntity.ok().body(userService.getAll(filterStr, rangeStr, sortStr))

    @GetMapping("/staff")
    fun getStaff(): ResponseEntity<PageableCollectionDTO>{
        return ResponseEntity.ok().body(userService.getStaff())
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(userService.get(id))
        } catch (noSuchElementException: NoSuchElementException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorMessageDTO(HttpStatus.NOT_FOUND.value(), noSuchElementException.message, "/api/user/${id}", Date()))
        }
    }

    @PostMapping("/signup")
    fun save(@RequestBody userDTO: UserDTO): ResponseEntity<UserDTO> {
        try {
            return ResponseEntity.ok().body(userService.save(userDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(userService.save(userDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(userService.activate(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/user/activate", Date()))
        }
    }

    @PutMapping
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    fun update(@RequestHeader("Authorization") token: String, @RequestBody userDTO: UserDTO) = ResponseEntity.ok().body(userService.update(userDTO, token.split(' ').get(1)))

    @DeleteMapping("/{id}")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(userService.delete(id))
}
package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.BalanceDTO
import edu.unlp.la.justa.dtos.GeneralDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.BalanceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/balance")
class BalanceController(@Autowired val balanceService: BalanceService) {

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?) = ResponseEntity.ok().body(balanceService.getAll(filterStr, rangeStr, sortStr))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(balanceService.get(id))


    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/close")
    fun close(@RequestBody generalDto: GeneralDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(balanceService.close(generalDto))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/balance/close", Date()))
        }
    }

}
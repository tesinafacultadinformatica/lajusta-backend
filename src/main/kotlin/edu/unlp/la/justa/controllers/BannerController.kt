package edu.unlp.la.justa.controllers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import edu.unlp.la.justa.dtos.CategoryDTO
import edu.unlp.la.justa.dtos.BannerDTO
import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.CategoryService
import edu.unlp.la.justa.services.BannerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/banner")
class BannerController(@Autowired val bannerService: BannerService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?,
               @RequestParam(required = false, name="properties") properties: String?): ResponseEntity<Any> {
        var prop: Set<PropertiesFilterDTO>? = null
        properties?.let { prop = jacksonObjectMapper().readValue(it) }
        val banners: Any = if(filterStr == null && rangeStr == null && sortStr == null && properties == null) bannerService.getAll() else bannerService.getAll(filterStr, rangeStr, sortStr, prop)
        return ResponseEntity.ok().body(banners)
    }

    @GetMapping("/{id}")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(bannerService.get(id))

    //@PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    fun save(@RequestBody bannerDTO: BannerDTO) = ResponseEntity.ok().body(bannerService.save(bannerDTO))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long) = ResponseEntity.ok().body(bannerService.activate(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody bannerDTO: BannerDTO) = ResponseEntity.ok().body(bannerService.update(bannerDTO))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(bannerService.delete(id))
}
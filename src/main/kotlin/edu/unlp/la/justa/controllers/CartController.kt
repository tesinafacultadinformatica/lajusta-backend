package edu.unlp.la.justa.controllers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import edu.unlp.la.justa.dtos.CartDTO
import edu.unlp.la.justa.dtos.CategoryDTO
import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotStockEnoughException
import edu.unlp.la.justa.services.CartService
import edu.unlp.la.justa.services.CategoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/cart")
class CartController(@Autowired val cartService: CartService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?,
               @RequestParam(required = false, name="properties") properties: String?,
               @RequestHeader("Authorization") token: String) : ResponseEntity<Any> {
        var prop: Set<PropertiesFilterDTO>? = null
        properties?.let { prop = jacksonObjectMapper().readValue(it) }
        val carts: Any = if (filterStr == null && rangeStr == null && sortStr == null && properties == null) cartService.getAll() else cartService.getAll(filterStr, rangeStr, sortStr, prop, token.split(' ').get(1))
        return ResponseEntity.ok().body(carts)
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(cartService.get(id))

    @PreAuthorize("hasRole('USUARIO') or hasRole('ADMINISTRADOR') or hasRole('EQUIPO')")
    @PostMapping
    fun save(@RequestBody cartDTO: CartDTO) = ResponseEntity.ok().body(cartService.save(cartDTO))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(cartService.activate(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/cart/activate", Date()))
        }
    }

    @PreAuthorize("hasRole('USUARIO') or hasRole('ADMINISTRADOR') or hasRole('EQUIPO')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody cartDTO: CartDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(cartService.update(cartDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping("/{id}")
    fun cancel(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(cartService.cancel(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('USUARIO') or hasRole('ADMINISTRADOR') or hasRole('EQUIPO')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping
    fun delete(@RequestBody cartDTO: CartDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(cartService.delete(cartDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }
}

package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.ResetPasswordDTO
import edu.unlp.la.justa.services.email.EmailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

class EmailResetPasswordDTO(val email: String)

@RestController
@RequestMapping("/api/email")
class EmailController(@Autowired val emailService: EmailService) {

    @PostMapping("/recovery")
    fun sendRecoveryPasswordEmail(@RequestBody emailResetPasswordDto: EmailResetPasswordDTO) = emailService.sendRecoveryPasswordEmail(emailResetPasswordDto.email)

    @PostMapping("/recovery/confirm")
    fun sendConfirmationEmail(@RequestBody resetPassword: ResetPasswordDTO) = emailService.sendConfirmationEmail(resetPassword)
}
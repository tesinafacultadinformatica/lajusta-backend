package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.NewsletterDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.NewsletterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/newsletter")
class NewsletterController(@Autowired val newsletterService: NewsletterService) {

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?) = ResponseEntity.ok().body(newsletterService.getAll(filterStr, rangeStr,sortStr))

    @PreAuthorize("hasRole('USUARIO') or hasRole('ADMINISTRADOR')")
    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(newsletterService.get(id))

    @PreAuthorize("hasRole('USUARIO') or hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestBody newsletterDTO: NewsletterDTO): ResponseEntity<NewsletterDTO> {
        try {
            return ResponseEntity.ok().body(newsletterService.save(newsletterDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(newsletterService.save(newsletterDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(newsletterService.activate(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/newsletter/activate", Date()))
        }
    }

    @PreAuthorize("hasRole('USUARIO') or hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody newsletterDTO: NewsletterDTO): ResponseEntity<NewsletterDTO> {
        try {
            return ResponseEntity.ok().body(newsletterService.save(newsletterDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(newsletterService.save(newsletterDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping
    fun delete(@RequestBody newsletterDTO: NewsletterDTO) = newsletterService.delete(newsletterDTO)
}
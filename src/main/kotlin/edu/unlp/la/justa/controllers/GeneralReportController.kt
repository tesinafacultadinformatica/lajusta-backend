package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.GeneralDTO
import edu.unlp.la.justa.services.GeneralService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.*

@RestController
@RequestMapping("/api/reportes")
class GeneralReportController(@Autowired val generalService: GeneralService) {

    @PostMapping("/general/ventas", produces = ["application/xml"])
    fun generatePurchacesReport(@RequestBody id: Long): ResponseEntity<InputStreamResource> {
        val report = generalService.generatePurchacesReport(GeneralDTO(id = id))
        val headers = HttpHeaders()
        val bos = ByteArrayOutputStream()
        report.write(bos)
        bos.close()
        headers.add("Content-Disposition", "attachment; filename=ventas-${id}.xlsx")
        return ResponseEntity.ok()
                .headers(headers)
                .body(InputStreamResource(ByteArrayInputStream(bos.toByteArray())))
    }

    @PostMapping("/general/balance", produces = ["application/xml"])
    fun generateNodesReport(@RequestBody id: Long): ResponseEntity<InputStreamResource> {
        val report = generalService.generatePurchaseReport(GeneralDTO(id = id))
        val headers = HttpHeaders()
        val bos = ByteArrayOutputStream()
        report.write(bos)
        bos.close()
        headers.add("Content-Disposition", "attachment; filename=balance-${id}.xlsx")
        return ResponseEntity.ok()
                .headers(headers)
                .body(InputStreamResource(ByteArrayInputStream(bos.toByteArray())))
    }

    @PostMapping("/general/balances", produces = ["application/xml"])
    fun generateNodesReport(): ResponseEntity<InputStreamResource> {
        val report = generalService.generatePurchaseReport()
        val headers = HttpHeaders()
        val bos = ByteArrayOutputStream()
        report.write(bos)
        bos.close()
        headers.add("Content-Disposition", "attachment; filename=balance-total.xlsx")
        return ResponseEntity.ok()
            .headers(headers)
            .body(InputStreamResource(ByteArrayInputStream(bos.toByteArray())))
    }
}
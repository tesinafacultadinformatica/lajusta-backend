package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.services.PdfGeneratorService
import org.apache.pdfbox.pdmodel.PDDocument
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.*

class PdfResult(val pdf: PDDocument, val title: String)

@RestController
@RequestMapping("/api/generator/pdf")
class PdfGeneratorController(@Autowired val pdfGeneratorService: PdfGeneratorService) {

//    @GetMapping("/purchase/{id}")
//    fun generatePdfGet(@PathVariable id: Long): ResponseEntity<InputStreamResource> {
//        val pdfDocument = pdfGeneratorService.generatePurchasePdf(id)
//        val headers = HttpHeaders()
//        val bos = ByteArrayOutputStream()
//        pdfDocument.pdf.save(bos)
//        bos.close()
//        headers.add("Content-Disposition", "attachment; filename=compra-${pdfDocument.title}.pdf")
//        return ResponseEntity.ok()
//                .headers(headers)
//                .body(InputStreamResource(ByteArrayInputStream(bos.toByteArray())))
//    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/purchase")
    fun generatePdf(@RequestBody id: Long): ResponseEntity<InputStreamResource> {
        val pdfDocument = pdfGeneratorService.generatePurchasePdf(id)
        val headers = HttpHeaders()
        val bos = ByteArrayOutputStream()
        pdfDocument.pdf.save(bos)
        bos.close()
        headers.add("Content-Disposition", "attachment; filename=compra-${pdfDocument.title}.pdf")
        return ResponseEntity.ok()
                .headers(headers)
                .body(InputStreamResource(ByteArrayInputStream(bos.toByteArray())))
    }
}
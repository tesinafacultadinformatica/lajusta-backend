package edu.unlp.la.justa.controllers

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.ProductDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

class PropertiesFilterDTO(val key: String, val value: Any)

@RestController
@RequestMapping("/api/product")
class ProductController(@Autowired val productService: ProductService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?,
               @RequestParam(required = false, name="properties") properties: String?): ResponseEntity<Any> {
        var prop: Set<PropertiesFilterDTO>? = null
        properties?.let { prop = jacksonObjectMapper().readValue(it) }
        return ResponseEntity.ok().body(productService.getAll(filterStr, rangeStr, sortStr, prop))
    }

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(productService.get(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestBody productDTO: ProductDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(productService.save(productDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(productService.activate(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/product/activate", Date()))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody productDTO: ProductDTO): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(productService.update(productDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(malformedException)
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(productService.delete(id))
}
package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.RoleDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.services.RoleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/role")
class RoleController(@Autowired val roleService: RoleService) {

    @GetMapping
    fun getAll() = ResponseEntity.ok().body(roleService.getAll())

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(roleService.get(id))

    @PostMapping
    fun save(@RequestBody roleDTO: RoleDTO): ResponseEntity<RoleDTO> {
        try {
            return ResponseEntity.ok().body(roleService.save(roleDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(roleService.save(roleDTO))
        }
    }

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long): ResponseEntity<Any> {
        try {
            return ResponseEntity.ok().body(roleService.activate(id))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorMessageDTO(HttpStatus.BAD_REQUEST.value(), malformedException.message, "/api/role/activate", Date()))
        }
    }

    @PutMapping
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    fun update(@RequestBody roleDTO: RoleDTO): ResponseEntity<RoleDTO> {
        try {
            return ResponseEntity.ok().body(roleService.save(roleDTO))
        } catch (malformedException: MalformedObjectException) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(roleService.save(roleDTO))
        }
    }

//    @GetMapping("/{id}")
//    fun getAll(@PathVariable id: Long) = roleService.get(id)
}
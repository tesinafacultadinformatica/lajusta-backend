package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.LoginUserDTO
import edu.unlp.la.justa.dtos.TokenDTO
import edu.unlp.la.justa.dtos.UserDTO
import edu.unlp.la.justa.models.UserToken
import edu.unlp.la.justa.repositories.UserRepository
import edu.unlp.la.justa.repositories.UserTokenRepository
import edu.unlp.la.justa.security.TokenProvider
import edu.unlp.la.justa.services.ImageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/token")
class TokenController(@Autowired val authenticationManager: AuthenticationManager,
                      @Autowired val jwtTokenUtil: TokenProvider,
                      @Autowired val userRepository: UserRepository,
                      @Autowired val imageService: ImageService,
                      @Autowired val userTokenRepository: UserTokenRepository) {

    @PostMapping("/generate-token")
    fun register(@RequestBody loginUser: LoginUserDTO): ResponseEntity<TokenDTO> {
        val authentication = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(loginUser.userName, loginUser.userPassword))
        SecurityContextHolder.getContext().authentication = authentication
        val token = jwtTokenUtil.generateToken(authentication)
        token?.let {
            val userToken = userTokenRepository.findByUsername(loginUser.userName) ?: UserToken(username = loginUser.userName, token = "")
            userToken.token = it
            val calendar = Calendar.getInstance()
            calendar.time = Date()
            calendar.add(Calendar.HOUR, 1)
            userToken.timeToLive = calendar.time
            userTokenRepository.save(userToken)
        }
        val userDetail = userRepository.findByEmail(loginUser.userName)
        return ResponseEntity.ok(TokenDTO(token, UserDTO.createUserDTO(userDetail)))
    }
}
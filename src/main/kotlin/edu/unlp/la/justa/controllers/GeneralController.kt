package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.ErrorMessageDTO
import edu.unlp.la.justa.dtos.GeneralDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.UniqueGeneralViolation
import edu.unlp.la.justa.services.GeneralService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/general")
class GeneralController(@Autowired val generalService: GeneralService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?) = ResponseEntity.ok().body(generalService.getAll(filterStr, rangeStr,sortStr))

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(generalService.get(id))

    @GetMapping("/active")
    fun getActive() = ResponseEntity.ok().body(generalService.getActiveGeneral())

    @GetMapping("/nextActive")
    fun getNextActive() = ResponseEntity.ok().body(generalService.getNextActiveGeneral())

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestHeader("Authorization") token: String,@RequestBody generalDTO: GeneralDTO) = ResponseEntity.ok().body(generalService.save(token.split(' ').get(1), generalDTO))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping("/activate/{id}")
    fun activate(@PathVariable id: Long) = ResponseEntity.ok().body(generalService.activate(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestHeader("Authorization") token: String,@RequestBody generalDTO: GeneralDTO) = ResponseEntity.ok().body(generalService.update(token.split(' ').get(1),generalDTO))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(generalService.delete(id))
}
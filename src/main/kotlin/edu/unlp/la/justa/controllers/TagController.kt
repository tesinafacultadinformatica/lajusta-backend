package edu.unlp.la.justa.controllers

import edu.unlp.la.justa.dtos.TagDTO
import edu.unlp.la.justa.services.TagService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/tag")
class TagController(@Autowired val tagService: TagService) {

    @GetMapping
    fun getAll(@RequestParam(required = false, name = "filter") filterStr: String?,
               @RequestParam(required = false, name = "range") rangeStr: String?,
               @RequestParam(required = false, name="sort") sortStr: String?) = ResponseEntity.ok().body(tagService.getAll(filterStr, rangeStr, sortStr))

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long) = ResponseEntity.ok().body(tagService.get(id))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @PostMapping
    fun save(@RequestBody tagDTO: TagDTO) = ResponseEntity.ok().body(tagService.save(tagDTO))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @PutMapping
    fun update(@RequestBody tagDTO: TagDTO) = ResponseEntity.ok().body(tagService.update(tagDTO))

    @PreAuthorize("hasRole('ADMINISTRADOR')")
    @CrossOrigin("\${cross.origin}" , "\${cross.origin.www}")
    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Long) = ResponseEntity.ok().body(tagService.delete(id))

}
package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.models.Cart

interface CartCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?, properties: Set<PropertiesFilterDTO>?, userId: Long): List<Cart>
}
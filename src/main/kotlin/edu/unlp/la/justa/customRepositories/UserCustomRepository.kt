package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.User

interface UserCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?): List<User>
    fun findStaff(): List<User>
}
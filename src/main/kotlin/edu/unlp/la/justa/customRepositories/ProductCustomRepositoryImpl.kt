package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.cutProperties
import edu.unlp.la.justa.models.Category
import edu.unlp.la.justa.models.Product
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Join
import javax.persistence.criteria.Path
import javax.persistence.criteria.Predicate

@Service
class ProductCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : ProductCustomRepository {
    override fun findAll(filterBy: String?, range: String?, sortBy: String?, properties: Set<PropertiesFilterDTO>?): List<Product> {
        val query = em.criteriaBuilder.createQuery(Product::class.java)
        val from = query.from(Product::class.java)

        val filters = mutableListOf<Predicate>()

        filterBy?.let {
            val filterByTitle = em.criteriaBuilder.like(from.get("title"), "%${it.replace(""""""", "")}%")
            val filterByUnitDescription = em.criteriaBuilder.like(from.get("unitDescription"), "%${it.replace(""""""", "")}%")
            filters.add(em.criteriaBuilder.or(filterByTitle, filterByUnitDescription))
        }

        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }

        properties?.forEach{
            if(it.key.contains('.')) {
                val subQuery = query.subquery(Category::class.java)
                val subRoot = subQuery.correlate(from)
                val join: Join<Product, Category> = subRoot.join(it.key.split('.')[0])
                val path2: Path<Set<String>> = join.get(it.key.split('.')[1])
                subQuery.select(join)
                subQuery.where(em.criteriaBuilder.equal(path2, it.value))
                filters.add(em.criteriaBuilder.exists(subQuery))
            } else {
                if("deletedAt" == it.key) {
                    filters.add(em.criteriaBuilder.isNull((from.get<String>("deletedAt"))))
                } else {
                    val path: Path<Set<String>> = from.get(it.key)
                    filters.add(em.criteriaBuilder.equal(path, it.value))
                }
            }
        }
        query.where(*filters.toTypedArray())
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt() * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }
}
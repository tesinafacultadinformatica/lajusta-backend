package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Unit
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Path

@Service
class UnitCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : UnitCustomRepository {
    override fun findAll(filterBy: String?, range: String?, sortBy: String?): List<Unit> {
        val query = em.criteriaBuilder.createQuery(Unit::class.java)
        val from = query.from(Unit::class.java)
        filterBy?.split(',')?.let { query.where(em.criteriaBuilder.like(from.get(it[0]), "%${it[1]}%")) }
        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt() * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }
}
package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Role
import edu.unlp.la.justa.models.User
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Join
import javax.persistence.criteria.Path
import javax.persistence.criteria.Predicate

@Service
class UserCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : UserCustomRepository{
    override fun findAll(filterBy: String?, range: String?, sortBy: String?): List<User> {
        val query = em.criteriaBuilder.createQuery(User::class.java)
        val from = query.from(User::class.java)
        filterBy?.split(',')?.let { query.where(em.criteriaBuilder.like(from.get(it[0]), "%${it[1]}%")) }

        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt() * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }

    override fun findStaff(): List<User> {
        val query = em.criteriaBuilder.createQuery(User::class.java)
        val from = query.from(User::class.java)

        val filters = mutableListOf<Predicate>()


        val path: Path<Set<String>> = from.get("id")
        val order = em.criteriaBuilder.asc(path)
        query.orderBy(listOf(order))


        //filterBy role y no deleted ¿?
        //filters.add(em.criteriaBuilder.isNull((from.get<String>("deletedAt"))))

        val subQuery = query.subquery(Role::class.java)
        val subRoot = subQuery.correlate(from)
        val join: Join<User, Role> = subRoot.join("role")
        val path2: Path<Set<String>> = join.get("id")
        subQuery.select(join)
        subQuery.where(em.criteriaBuilder.notEqual(path2, "2"))
        filters.add(em.criteriaBuilder.exists(subQuery))
        //val path2: Path<Set<String>> = from.get("role.id")
        //filters.add(em.criteriaBuilder.equal(path2, "2"))


        query.where(*filters.toTypedArray())
        val finalQuery = em.createQuery(query.select(from))
        return finalQuery.resultList
    }
}
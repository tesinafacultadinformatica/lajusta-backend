package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.models.Category
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Path

@Service
class CategoryCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : CategoryCustomRepository {
    override fun findAll(filterBy: String?, range: String?, sortBy: String?, properties: Set<PropertiesFilterDTO>?): List<Category> {
        val query = em.criteriaBuilder.createQuery(Category::class.java)
        val from = query.from(Category::class.java)
        filterBy?.split(',')?.let { query.where(em.criteriaBuilder.like(from.get("name"), "%${it[0]}%")) }
        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }
        properties?.forEach{
            val path: Path<Set<String>> = from.get(it.key)
            if (it.value.toString().isEmpty()) {
                query.where(em.criteriaBuilder.isNull(path))
            } else {
                query.where(em.criteriaBuilder.equal(path, it.value))
            }
        }
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt()  * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }
}
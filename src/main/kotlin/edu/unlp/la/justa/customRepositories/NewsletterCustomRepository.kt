package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Newsletter

interface NewsletterCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?): List<Newsletter>
}
package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.models.Banner
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Path

@Service
class BannerCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : BannerCustomRepository {
    override fun findAll(filterBy: String?, range: String?, sortBy: String?, properties:Set<PropertiesFilterDTO>?): List<Banner> {
        val query = em.criteriaBuilder.createQuery(Banner::class.java)
        val from = query.from(Banner::class.java)
        filterBy?.let { query.where(em.criteriaBuilder.or(em.criteriaBuilder.like(from.get("title"), "%${it[0]}%"),
                                                                                em.criteriaBuilder.like(from.get("subtitle"), "%${it[0]}%"),
                                                                                em.criteriaBuilder.like(from.get("text"), "%${it[0]}%"),
                                                                                em.criteriaBuilder.like(from.get("url"), "%${it[0]}%")))  }
        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }
        properties?.forEach{
            val path: Path<Set<String>> = from.get(it.key)
            query.where(if(it.value == "") em.criteriaBuilder.isNull(path) else em.criteriaBuilder.equal(path,it.value))
        }
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt()  * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }
}
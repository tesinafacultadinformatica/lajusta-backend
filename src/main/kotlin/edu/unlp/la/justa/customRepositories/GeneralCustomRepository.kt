package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.General

interface GeneralCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?): List<General>
}
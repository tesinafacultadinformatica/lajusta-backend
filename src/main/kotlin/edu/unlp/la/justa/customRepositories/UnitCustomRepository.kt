package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Unit

interface UnitCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?): List<Unit>
}
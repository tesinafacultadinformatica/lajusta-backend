package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.models.*
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Join
import javax.persistence.criteria.Path
import javax.persistence.criteria.Predicate

@Service
class CartCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : CartCustomRepository {
    override fun findAll(filterBy: String?, range: String?, sortBy: String?, properties: Set<PropertiesFilterDTO>?, userId: Long): List<Cart> {
        val query = em.criteriaBuilder.createQuery(Cart::class.java)
        val from = query.from(Cart::class.java)
        val filters = mutableListOf<Predicate>()
        filterBy?.split(',')?.let { query.where(em.criteriaBuilder.like(from.get("name"), "%${it[0]}%")) }
        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }
        properties?.forEach{
            if("history" == it.key && true == it.value){
                val subQuery = query.subquery(User::class.java)
                val subRoot = subQuery.correlate(from)
                val join: Join<Cart, User> = subRoot.join("user")
                val path2: Path<Set<String>> = join.get("id")
                subQuery.select(join)
                subQuery.where(em.criteriaBuilder.equal(path2, userId))
                filters.add(em.criteriaBuilder.exists(subQuery))
            }
            if(it.key.contains('.')) {
                val subQuery = query.subquery(General::class.java)
                val subRoot = subQuery.correlate(from)
                val join: Join<Cart, General> = subRoot.join(it.key.split('.')[0])
                val path2: Path<Set<String>> = join.get(it.key.split('.')[1])
                subQuery.select(join)
                subQuery.where(em.criteriaBuilder.equal(path2, it.value))
                filters.add(em.criteriaBuilder.exists(subQuery))
            }
        }
        query.where(*filters.toTypedArray())
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt() * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }
}
package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Tag

interface TagCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?): List<Tag>
}
package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Balance

interface BalanceCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?, generalId: Long?): List<Balance>
}
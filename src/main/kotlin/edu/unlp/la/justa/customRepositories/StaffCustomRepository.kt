package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Staff

interface StaffCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?): List<Staff>
}
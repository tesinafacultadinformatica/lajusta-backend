package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.models.Category
import edu.unlp.la.justa.models.Producer
import edu.unlp.la.justa.models.Product
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Join
import javax.persistence.criteria.Path

@Service
class ProducerCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : ProducerCustomRepository {
    override fun findAll(filterBy: String?, range: String?, sortBy: String?, properties: Set<PropertiesFilterDTO>?): List<Producer> {
        val query = em.criteriaBuilder.createQuery(Producer::class.java)
        val from = query.from(Producer::class.java)
        filterBy?.let { query.where(em.criteriaBuilder.like(from.get("firstName"), "%${it[0]}%")) }
        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }
        properties?.forEach{
            if("deletedAt" == it.key){
                query.where(em.criteriaBuilder.isNull(from.get<String>("deletedAt")))
            } else {
                val path: Path<Set<String>> = from.get(it.key)
                query.where(em.criteriaBuilder.equal(path, it.value))
            }
        }
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt() * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }
}
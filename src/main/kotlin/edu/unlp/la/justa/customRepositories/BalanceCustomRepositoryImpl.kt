package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Balance
import edu.unlp.la.justa.models.Category
import edu.unlp.la.justa.models.General
import edu.unlp.la.justa.models.Product
import org.springframework.stereotype.Service
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Join
import javax.persistence.criteria.Path

@Service
class BalanceCustomRepositoryImpl(@PersistenceContext val em: EntityManager) : BalanceCustomRepository {
    override fun findAll(filterBy: String?, range: String?, sortBy: String?, generalId: Long?): List<Balance> {
        val query = em.criteriaBuilder.createQuery(Balance::class.java)
        val from = query.from(Balance::class.java)

        val subQuery = query.subquery(General::class.java)
        val subRoot = subQuery.correlate(from)
        val join: Join<Balance, General> = subRoot.join("general")
        val path2: Path<Set<String>> = join.get("id")
        subQuery.select(join)
        generalId?.let { subQuery.where(em.criteriaBuilder.equal(path2, it)) }
        val dateBalance: Path<Set<String>> = from.get("dateBalance")

        val predicateEqualsGeneral = em.criteriaBuilder.exists(subQuery)
        val predicateDateBalanceNotNull = em.criteriaBuilder.isNotNull(dateBalance)

        query.where(em.criteriaBuilder.or(predicateEqualsGeneral, predicateDateBalanceNotNull))

        filterBy?.split(',')?.let { query.where(em.criteriaBuilder.like(from.get(it[0]), "%${it[1]}%")) }
        sortBy?.split(',')?.let {
            val path: Path<Set<String>> = from.get(it[0])
            val order = if("asc" == it[1].toLowerCase())  em.criteriaBuilder.asc(path) else em.criteriaBuilder.desc(path)
            query.orderBy(listOf(order))
        }
        val finalQuery = em.createQuery(query.select(from))
        range?.split(',')?.let { finalQuery.setFirstResult(it[0].toInt()  * it[1].toInt()).setMaxResults(it[1].toInt()) }
        return finalQuery.resultList
    }
}
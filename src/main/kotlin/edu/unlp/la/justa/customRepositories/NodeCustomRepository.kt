package edu.unlp.la.justa.customRepositories

import edu.unlp.la.justa.models.Node

interface NodeCustomRepository {
    fun findAll(filterBy: String?, range: String?, sortBy: String?): List<Node>
}
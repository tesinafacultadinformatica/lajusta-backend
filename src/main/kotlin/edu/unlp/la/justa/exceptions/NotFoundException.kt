package edu.unlp.la.justa.exceptions

class NotFoundException(message: String) : GeneralException(ErrorType.notFound, message)
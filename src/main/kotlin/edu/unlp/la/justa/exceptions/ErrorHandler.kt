package edu.unlp.la.justa.exceptions

import edu.unlp.la.justa.dtos.ErrorMessageDTO
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.context.request.WebRequest
import java.util.*

@ControllerAdvice
class ErrorHandler {
    @ExceptionHandler(value = [MalformedObjectException::class,
        NotStockEnoughException::class,
        UniqueGeneralViolation::class,
        NotFoundException::class,
        ActiveGeneralNotFound::class,
        TokenNotFoundException::class])
    protected fun handleConflict(exeption: GeneralException?, webRequest: WebRequest?): ResponseEntity<Any> {
        return ResponseEntity.status(exeption?.errorType?.httpStatus ?: HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorMessageDTO(exeption?.errorType?.httpStatus?.value() ?: 500, exeption?.message ?: "Server Error",
                        (webRequest as ServletWebRequest).request.requestURI ?: "Server Error", Date()))
    }
}
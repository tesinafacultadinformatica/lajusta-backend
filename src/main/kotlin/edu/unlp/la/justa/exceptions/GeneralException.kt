package edu.unlp.la.justa.exceptions

open class GeneralException(val errorType: ErrorType, message: String) : Exception(message)
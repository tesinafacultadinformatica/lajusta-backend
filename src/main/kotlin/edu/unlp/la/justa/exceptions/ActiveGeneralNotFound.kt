package edu.unlp.la.justa.exceptions

class ActiveGeneralNotFound(message: String) : GeneralException(ErrorType.notFound, message)
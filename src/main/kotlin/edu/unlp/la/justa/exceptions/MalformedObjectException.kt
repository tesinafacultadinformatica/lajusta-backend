package edu.unlp.la.justa.exceptions

class MalformedObjectException(message: String) : GeneralException(ErrorType.malformedObject, message)
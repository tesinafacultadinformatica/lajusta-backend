package edu.unlp.la.justa.exceptions

class NotStockEnoughException(message: String) : GeneralException(ErrorType.insuficientStock, message)
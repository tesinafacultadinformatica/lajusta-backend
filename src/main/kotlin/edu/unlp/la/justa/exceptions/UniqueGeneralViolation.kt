package edu.unlp.la.justa.exceptions

class UniqueGeneralViolation(message: String) : GeneralException(ErrorType.malformedObject, message)
package edu.unlp.la.justa.exceptions

class TokenNotFoundException(message: String) : GeneralException(ErrorType.notFound, message)
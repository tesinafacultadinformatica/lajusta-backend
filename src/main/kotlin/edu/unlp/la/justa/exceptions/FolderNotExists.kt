package edu.unlp.la.justa.exceptions

import java.lang.Exception

class FolderNotExists(message: String) : Exception(message)
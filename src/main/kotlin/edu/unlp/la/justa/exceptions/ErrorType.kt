package edu.unlp.la.justa.exceptions

import org.springframework.http.HttpStatus

enum class ErrorType(val httpStatus: HttpStatus) {
    malformedObject(HttpStatus.BAD_REQUEST),
    notFound(HttpStatus.NOT_FOUND),
    insuficientStock(HttpStatus.CONFLICT),
    duplicatedData(HttpStatus.CONFLICT)
}
package edu.unlp.la.justa.security

import edu.unlp.la.justa.exceptions.TokenNotFoundException
import edu.unlp.la.justa.services.UserService
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.SignatureException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Service
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class JwtAuthenticationFilter : OncePerRequestFilter() {

    @Autowired
    private val userService: UserService? = null

    @Autowired
    private val jwtTokenUtil: TokenProvider? = null

    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, chain: FilterChain) {
        val header = req.getHeader(HEADER_STRING)
        var username: String? = null
        var authToken: String? = null
        if (header != null && header.startsWith(TOKEN_PREFIX)) {
            authToken = header.replace(TOKEN_PREFIX, "")
            try {
                username = jwtTokenUtil?.getUsernameFromToken(authToken)?.username
            } catch (e: IllegalArgumentException) {
                logger.error("an error occured during getting username from token", e)
            } catch (e: ExpiredJwtException) {
                logger.warn("the token is expired and not valid anymore", e)
            } catch (e: SignatureException) {
                logger.error("Authentication Failed. Username or Password not valid.")
            } catch (e: TokenNotFoundException) {
                logger.error("Authentication Failed. Token $authToken incorrect, it was not found.")
            }
        } else {
            logger.warn("couldn't find bearer string, will ignore the header")
        }
        if (username != null && SecurityContextHolder.getContext().authentication == null) {
            val userDetails = userService?.loadUserByUsername(username)
            if (jwtTokenUtil?.validateToken(authToken, userDetails) ?: false) {
                val authentication: UsernamePasswordAuthenticationToken? = jwtTokenUtil?.getAuthentication(authToken, SecurityContextHolder.getContext().authentication, userDetails)
                authentication?.details = WebAuthenticationDetailsSource().buildDetails(req)
                logger.info("authenticated user $username, setting security context")
                SecurityContextHolder.getContext().authentication = authentication
            }
        }
        chain.doFilter(req, res)
    }

    companion object {
        val HEADER_STRING = "Authorization"
        val TOKEN_PREFIX = "Bearer "
    }
}
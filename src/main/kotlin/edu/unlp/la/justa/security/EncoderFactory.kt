package edu.unlp.la.justa.security

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class EncoderFactory: BCryptPasswordEncoder() {
}
package edu.unlp.la.justa.security

import edu.unlp.la.justa.exceptions.TokenNotFoundException
import edu.unlp.la.justa.models.UserToken
import edu.unlp.la.justa.repositories.UserTokenRepository
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import io.jsonwebtoken.JwtParser
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.*

@Service
class TokenProvider(val userTokenRepository: UserTokenRepository) {

    fun generateToken(authentication: Authentication): String? {
        val authorities: String = authentication.getAuthorities().stream()
                .map({ obj: GrantedAuthority -> obj.authority })
                .collect(Collectors.joining(","))
        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(SignatureAlgorithm.HS256, SIGNING_KEY)
                .setIssuedAt(Date(System.currentTimeMillis()))
                .setExpiration(Date(System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS * 1000))
                .compact()
    }

    fun getAuthentication(token: String?, existingAuth: Authentication?, userDetails: UserDetails?): UsernamePasswordAuthenticationToken? {
        val jwtParser: JwtParser = Jwts.parser().setSigningKey(SIGNING_KEY)
        val claimsJws: Jws<Claims> = jwtParser.parseClaimsJws(token)
        val claims: Claims = claimsJws.getBody()
        val authorities: Collection<GrantedAuthority> = Arrays.stream(claims[AUTHORITIES_KEY].toString().split(",").toTypedArray())
                .map({ role: String? -> SimpleGrantedAuthority(role) })
                .collect(Collectors.toList())
        return UsernamePasswordAuthenticationToken(userDetails, "", authorities)
    }

    fun getUsernameFromToken(authToken: String) : UserToken {
        try {
            val token = userTokenRepository.findByToken(authToken)
            val calendar = Calendar.getInstance()
            calendar.time = Date()
            calendar.add(Calendar.HOUR, 1)
            token.timeToLive = calendar.time
            userTokenRepository.save(token)
            return token
        } catch (exception: Exception){
            throw TokenNotFoundException(exception.message ?: "Error: Token was not found!")
        }
    }

    fun validateToken(authToken: String?, userDetails: UserDetails?): Boolean {
        val userToken = userTokenRepository.findByUsername(userDetails?.username ?: "")
        return !authToken.isNullOrEmpty() && userToken?.token == authToken
    }

    companion object {
        val ACCESS_TOKEN_VALIDITY_SECONDS = 86400
        val AUTHORITIES_KEY = "ROLE_ADMINISTRADOR, ROLE_USUARIO"
        val SIGNING_KEY = "khgi4554^&^4fadfo65sadfew..#$%#$"
    }
}
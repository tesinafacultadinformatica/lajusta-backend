package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.ImageDTO
import edu.unlp.la.justa.exceptions.FolderNotExists
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.models.Image
import edu.unlp.la.justa.repositories.ImageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File
import java.util.*
import kotlin.math.absoluteValue


@Service
class ImageService(@Autowired val imageRepository: ImageRepository,
                   @Value("\${la.justa.image.folder.path}") val imagesFolderPath: String,
                   @Value("\${IMAGE_SERVER}") val imageServerUrl: String) {

    init { checkFolder(imagesFolderPath) }

    fun getAll() = imageRepository.findAll()

    fun get(id: Long) = ImageDTO.createImageDTO(imageRepository.findById(id).get())

    fun findById(id: Long) = imageRepository.findById(id).get()

    fun save(folderName: String?, imageDTO: ImageDTO?): Image? {
        var savedImage: Image? = null
        val imageFolderName = folderName.hashCode().absoluteValue
        val folder = "${imagesFolderPath}/${imageFolderName}"
        val imageName = "${imageDTO?.name.hashCode().absoluteValue}"
        folderName?.let {
            imageDTO?.let {
                saveImage(folder, imageName, imageDTO.type, imageDTO.value)
                imageDTO.id?.let { throw MalformedObjectException("imageDTO.id should be null, please use Update instead") }
                val newImage = Image.createImage(imageDTO)
                newImage?.let {
                    newImage.route = "${imageServerUrl}/${imageFolderName}/${imageName}.${imageDTO.type}"
                    newImage.createdAt = Date()
                    newImage.updatedAt = Date()
                    savedImage = imageRepository.save(newImage)
                }
            }
        }
        return savedImage
    }

    fun update(imageDTO: ImageDTO): Image {
        imageDTO.id ?: throw MalformedObjectException("imageDTO.id should not be null, please use Save instead")
        val updatedImage = imageRepository.findById(imageDTO.id).get()
        updatedImage.isMain = imageDTO.isMain
        updatedImage.updatedAt = Date()
        return imageRepository.save(updatedImage)
    }

    fun delete(imageDTO: ImageDTO): ImageDTO? {
        imageDTO.id ?: throw MalformedObjectException("imageDTO.id should not be null")
        val deletedImage = Image.createImage(imageDTO)
        deletedImage ?: throw MalformedObjectException("Image should not be null or empty!")
        deletedImage.deletedAt = Date()
        return ImageDTO.createImageDTO(imageRepository.save(deletedImage))
    }

    fun saveImage(folderPath: String, fileName: String?, type: String?, base64File: String?): Boolean {
        fileName?.let {
            base64File?.let {
                if (checkFolder(folderPath)) {
                    val imageFile = File(folderPath, "$fileName.$type")
                    val imageByteArray = Base64.getDecoder().decode(if (base64File.contains(","))base64File.split(",")[1] else base64File)
                    imageFile.writeBytes(imageByteArray)
                    return imageFile.exists()
                } else {
                    throw FolderNotExists("Folder ${folderPath} does not exist!")
                }
            }
        }
        return false
    }

    companion object {
        fun checkFolder(folderPath: String): Boolean {
            val folder = File(folderPath)
            if(!folder.exists()) folder.mkdir()
            return folder.isDirectory
        }
    }
}
package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.ProducerDTO
import edu.unlp.la.justa.dtos.ProducerYieldDTO
import edu.unlp.la.justa.dtos.ProductCatalogeDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Image
import edu.unlp.la.justa.models.Producer
import edu.unlp.la.justa.repositories.ProducerRepository
import edu.unlp.la.justa.repositories.ProductCatalogueRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class ProducerService(@Autowired val producerRepository: ProducerRepository,
                      @Autowired val productCatalogueRepository: ProductCatalogueRepository,
                      @Autowired val imageService: ImageService) {

    fun getAll(filterStr: String?, rangeStd:String?, sortStr: String?, properties: Set<PropertiesFilterDTO>?): PageableCollectionDTO {
        val producers = producerRepository.findAll(filterStr, rangeStd, sortStr, properties).map { ProducerDTO.createProducerDTO(it) }.toSet()
        val totalElements = producerRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = producers)
    }

    fun getAll() = producerRepository.findAll().map { ProducerDTO.createProducerDTO(it) }.toSet()

    fun get(id: Long) = ProducerDTO.createProducerDTO(producerRepository.findById(id).get())

    @Transactional
    fun save(producerDTO: ProducerDTO): ProducerDTO {
        producerDTO.id?.let { throw MalformedObjectException("producerDTO.id should be null, please use Update instead") }
        val newProducer = producerRepository.save(Producer.createProducer(producerDTO))
        producerDTO.images?.let {
            val images = mutableSetOf<Image>()
            it.forEach({ image -> imageService.save(newProducer.firstName, image)?.let { savedImage -> images.add(savedImage) } })
            newProducer.images = images
        }
        return ProducerDTO.createProducerDTO(newProducer)
    }

    fun activate(id: Long): ProducerDTO {
        val producer = producerRepository.findById(id)
        if(!producer.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        producer.get().deletedAt = null
        producer.get().updatedAt = Date()
        producerRepository.save(producer.get())
        return ProducerDTO.createProducerDTO(producer.get())
    }

    fun update(producerDTO: ProducerDTO): ProducerDTO {
        producerDTO.id ?: throw MalformedObjectException("producerDTO.id should be null, please use Update instead")
        val updatedProducer = Producer.createProducer(producerDTO)
        producerDTO.images?.let {
            val images = mutableSetOf<Image>()
            it.forEach { image -> if (image.id != null) {
                    imageService.update(image).let { savedImage -> images.add(savedImage) }
                } else {
                    imageService.save(updatedProducer.firstName, image)?.let { savedImage -> images.add(savedImage) }
                }
            }
            updatedProducer.images = images
        }
        val updatedCart = producerRepository.save(updatedProducer)
        updatedCart.updatedAt = Date()
        return ProducerDTO.createProducerDTO(updatedCart)
    }

    fun updateProducerYield(producerYieldDto: ProducerYieldDTO): ProducerDTO {
        producerYieldDto.producer ?: throw MalformedObjectException("producerYieldDto.producer should be null, please use Update instead")
        val producerDB = producerRepository.findById(producerYieldDto.producer)
        if(producerDB.isPresent){
            val producer = producerDB.get()
            producerYieldDto.products?.forEach {  newProduct ->
                producer.productCataloge?.first { product -> product.id == newProduct.id }?.let {
                    it.price = newProduct.price
                    it.quantity = newProduct.quantity
                    it.updatedAt = Date()
                    productCatalogueRepository.save(it)
                }
            }
            return ProducerDTO.createProducerDTO(producer)
        } else {
            throw NotFoundException("Producer with ID: ${producerYieldDto.producer} does not exist")
        }
    }

    fun getProducerYield(id: Long): Set<ProductCatalogeDTO>? {
        val producerDB = producerRepository.findById(id)
        if(producerDB.isPresent){
            return producerDB.get().productCataloge?.mapNotNull { ProductCatalogeDTO.createProductCatalogeDTO(it) }?.toSet()
        } else {
            throw NotFoundException("Producer with ID: ${id} does not exist")
        }
    }

    fun delete(id: Long): ProducerDTO {
        val deletedProcer = producerRepository.findById(id).get()
        deletedProcer.deletedAt = Date()
        return ProducerDTO.createProducerDTO(producerRepository.save(deletedProcer))
    }
}
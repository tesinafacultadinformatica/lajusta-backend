package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.CartDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.ActiveGeneralNotFound
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.exceptions.NotStockEnoughException
import edu.unlp.la.justa.models.*
import edu.unlp.la.justa.repositories.BalanceRepository
import edu.unlp.la.justa.repositories.CartRepository
import edu.unlp.la.justa.repositories.GeneralRepository
import edu.unlp.la.justa.repositories.ProductRepository
import edu.unlp.la.justa.repositories.PurchaseProducerRepository
import edu.unlp.la.justa.repositories.UserRepository
import edu.unlp.la.justa.services.email.EmailService
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.Exception
import java.math.BigDecimal
import java.util.*
import javax.transaction.Transactional
import kotlin.concurrent.thread

@Service
class CartService(@Autowired val cartRepository: CartRepository,
                  @Autowired val generalRepository: GeneralRepository,
                  @Autowired val purchaseRepository: PurchaseProducerRepository,
                  @Autowired val balanceRepository: BalanceRepository,
                  @Autowired val emailService: EmailService,
                  @Autowired val userRepository: UserRepository,
                  @Autowired val productRepository: ProductRepository) {

    val LOG = Logger.getLogger(CartService::class.java)

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?, properties: Set<PropertiesFilterDTO>?, token: String): PageableCollectionDTO {
        var cartsDTO: Set<CartDTO> = setOf()
        var totalElements = 0L
        userRepository.findUserByToken(token).let {
            cartsDTO = cartRepository.findAll(filterStr, rangeStd, sortStr, properties, it.id!!).mapNotNull {CartDTO.createCartDTO(it)}.toSet()
            if(properties?.filter { "history" == it.key }?.isEmpty() ?: true){
                totalElements = cartRepository.count()
            } else {
                totalElements = cartRepository.countByUser(it)
            }
        }
        return PageableCollectionDTO(totalElements = totalElements, page = cartsDTO)
    }

    fun getAll(): PageableCollectionDTO {
        val carts = cartRepository.findAll().mapNotNull { CartDTO.createCartDTO(it) }.toSet()
        val totalElements = cartRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = carts)
    }

    @Transactional
    fun saveCartPurchase(cartDTO: CartDTO): Cart {
        cartDTO.id?.let { throw MalformedObjectException("cartDTO.id should be null, please use Update instead") }
        val activeGeneral = generalRepository.findActive(Date())
        activeGeneral
                ?: throw ActiveGeneralNotFound("No podemos agregar el carrito dado que no existe un general activo")
        cartDTO.nodeDate
                ?: throw MalformedObjectException("Por favor, seleccione un Nodo para retirar su producto")
        val newCart = Cart.createCart(cartDTO)
        newCart.nodeAvailable?.node?.name = (cartDTO.nodeDate?.node as LinkedHashMap<String,String>).get("name")
        newCart.total = BigDecimal.ZERO
        newCart.general = activeGeneral
        newCart.nodeAvailable?.let { it.general = activeGeneral }
        newCart.staffDiscount = cartDTO.staffDiscount
        newCart.cartProducts.forEach { cartProduct ->
            cartProduct.product?.id?.let {
                val product = productRepository.findById(it).get()
                cartProduct.price = product.price
                cartProduct.buyPrice = product.buyPrice
                if ((product.stock?.minus(cartProduct.quantity)) ?: -1 >= 0) {
                    cartProduct.cart = newCart
                    newCart.total = newCart.total!!.plus(cartProduct.getTotalPrice() ?: BigDecimal.ZERO)
                    product.stock = product.stock?.minus(cartProduct.quantity)
                    productRepository.save(product)
                    savePurchace(product, activeGeneral, cartProduct)
                } else {
                    throw NotStockEnoughException("No contamos con el stock suficiente de ${product.title} para realizar la compra")
                }
            }
        }
        newCart.saleDate = Date()
        newCart.createdAt = Date()
        newCart.updatedAt = Date()


        val cartDatabase = cartRepository.save(newCart)
        saveBalance(cartDatabase, activeGeneral.balance)
        return cartDatabase
    }

    fun saveBalance(cart: Cart, currentBalance: Balance?){
        currentBalance?.let {
            it.totalSale = it.totalSale?.add(cart.total)
            it.updatedAt = Date()
            balanceRepository.save(it)
        }
    }

    fun cancelCartRefoundBalance(cart: Cart, currentBalance: Balance?){
        currentBalance?.let {
            it.totalSale = it.totalSale?.minus(cart.total ?: BigDecimal.ZERO)
            it.updatedAt = Date()
            balanceRepository.save(it)
        }
    }

    fun save(cartDTO: CartDTO): CartDTO? {
        val newCart = saveCartPurchase(cartDTO)
        thread(start = true) {
            try {
                emailService.sendNewPurchaseEmail(newCart)
            } catch (exception: Exception) {
                LOG.error("We could not send the email for purchace ${newCart.id} with error ${exception.message}")
            }
        }
        return CartDTO.createCartDTO(newCart)
    }

    private fun savePurchace(product: Product, general: General, cartProduct: CartProduct) {
        val purchase = purchaseRepository.findByProductIdAndGeneralId(product.id!!, general.id!!) ?: run { PurchaseProducer(id = null, general = general, product = product, producer = product.producer, quantity = 0, aditional = 0, price = product.price!!, total = BigDecimal.ZERO, deletedAt = null, updatedAt = Date(), createdAt = Date()) }
        purchase.quantity = purchase.quantity.plus(cartProduct.quantity)
        purchase.total = purchase.total.plus(cartProduct.getTotalPrice() ?: BigDecimal.ZERO)
        purchaseRepository.save(purchase)
    }

    private fun editPurchace(product: Product, general: General, cartProduct: CartProduct) {
        val purchase = purchaseRepository.findByProductIdAndGeneralId(product.id!!, general.id!!)!!
        purchase.quantity = purchase.quantity.minus(cartProduct.quantity)
        purchase.total = purchase.total.minus(cartProduct.getTotalPrice() ?: BigDecimal.ZERO)
        purchaseRepository.save(purchase)
    }

    fun get(id: Long) = CartDTO.createCartDTO(cartRepository.findById(id).get())

    fun update(cartDTO: CartDTO): CartDTO? {
        cartDTO.id ?: throw MalformedObjectException("cartDTO.id should be null, please use Update instead")
        val activeGeneral = generalRepository.findActive(Date())
        activeGeneral ?: throw ActiveGeneralNotFound("No podemos agregar el carrito dado que no existe un general activo")
        val cart = cartRepository.findById(cartDTO.id).get()
        cart.total = BigDecimal.ZERO
        cart.general = activeGeneral
        cartDTO.cartProducts.forEach { cartProduct ->
            val cartProductDB = cart.cartProducts.find { it.id == cartProduct.id }!!
            if(cartProduct.quantity == 0) {
                editPurchace(cartProductDB.product!!, activeGeneral, cartProductDB)
                cartProductDB.product.stock = cartProductDB.product.stock?.plus(cartProductDB.quantity)
                cartProductDB.cart = null
                productRepository.save(cartProductDB.product)
            } else {
                cart.total = (cart.total as BigDecimal).plus(
                    (if(cartProduct.cart?.staffDiscount == true) cartProduct.product?.price else cartProduct.price)
                        ?.multiply(cartProduct.quantity.toBigDecimal()) ?: BigDecimal.ZERO)
            }
        }
        cart.nodeAvailable = AvailableNode.createAvailableNode(cartDTO.nodeDate)
        cart.updatedAt = Date()
        val updatedCart = cartRepository.save(cart)
        return CartDTO.createCartDTO(updatedCart)
    }

    fun activate(id: Long): CartDTO? {
        val cart = cartRepository.findById(id)
        if (!cart.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        cart.get().deletedAt = null
        cart.get().updatedAt = Date()
        cartRepository.save(cart.get())
        return CartDTO.createCartDTO(cart.get())
    }

    fun delete(cartDTO: CartDTO): CartDTO? {
        cartDTO.id ?: throw MalformedObjectException("cartDTO.id should not be null")
        val deletedCart = Cart.createCart(cartDTO)
        deletedCart.deletedAt = Date()
        return CartDTO.createCartDTO(cartRepository.save(deletedCart))
    }

    @Transactional
    fun cancel(id: Long) {
        val cart = cartRepository.findById(id)
        val general = generalRepository.findActive(Date())
        if(cart.isPresent) {
            cart.get().canceled = true
            cartRepository.save(cart.get())
            cancelCartRefoundBalance(cart.get(), general?.balance)
        }
    }
}

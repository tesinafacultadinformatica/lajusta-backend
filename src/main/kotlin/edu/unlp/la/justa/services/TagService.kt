package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.TagDTO
import edu.unlp.la.justa.models.Tag
import edu.unlp.la.justa.repositories.TagRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TagService(@Autowired val tagRepository: TagRepository) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?): Any {
        val tags: Any = if (filterStr == null && rangeStd == null && sortStr == null){
            tagRepository.findAll().map { TagDTO(it.id, it.name) }.toSet()
        } else {
            val totalTagCount = tagRepository.count()
            PageableCollectionDTO(totalElements = totalTagCount, page = tagRepository.findAll(filterStr, rangeStd, sortStr).map{ TagDTO(it.id, it.name)}.toSet())
        }
        return tags
    }

    fun get(id: Long) = tagRepository.findById(id).map { TagDTO(it.id, it.name) }

    fun save(tagDTO: TagDTO) = tagRepository.save(Tag(tagDTO.id, tagDTO.description)).run { TagDTO(this.id, this.name) }

    fun update(tagDTO: TagDTO) = tagRepository.save(Tag(tagDTO.id, tagDTO.description)).run { TagDTO(this.id, this.name) }

    fun delete(id: Long) = tagRepository.delete(Tag(id, name = ""))
}
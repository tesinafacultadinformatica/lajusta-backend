package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.ExpenseDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.models.Balance
import edu.unlp.la.justa.models.Expense
import edu.unlp.la.justa.repositories.BalanceRepository
import edu.unlp.la.justa.repositories.ExpenseRepository
import edu.unlp.la.justa.repositories.GeneralRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class ExpenseService(
    @Autowired val expenseRepository: ExpenseRepository,
    @Autowired val balanceRepository: BalanceRepository,
    @Autowired val generalRepository: GeneralRepository
) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?, properties: Set<PropertiesFilterDTO>?): Any {
        val expenses: Any = if (filterStr == null && rangeStd == null && sortStr == null && properties == null) {
            expenseRepository.findAll().map { ExpenseDTO.createExpenseDTO(it) }.toSet()
        } else {
            val expenses = expenseRepository.findAll(filterStr, rangeStd, sortStr, properties).mapNotNull { ExpenseDTO.createExpenseDTO(it) }.toSet()
            PageableCollectionDTO(totalElements = expenses.size.toLong() , page = expenses)
        }
        return expenses
    }

    fun getAll() = expenseRepository.findAll().map { expense -> expense }.toSet()

    fun get(id: Long): ExpenseDTO? {
        val expense = expenseRepository.findById(id).get()
        return ExpenseDTO.createExpenseDTO(expense)
    }

    @Transactional
    fun save(expenseDTO: ExpenseDTO): ExpenseDTO? {
        expenseDTO.id?.let { throw MalformedObjectException("expenseDTO.id should be null, please use Update instead") }
        val general = generalRepository.findActive(today = Date())
        val newExpense = Expense.createExpense(expenseDTO).apply {
            this?.general = general
        }
        newExpense ?: throw MalformedObjectException("Expense must not be null or empty!")
        val savedExpense = expenseRepository.save(newExpense)
        saveBalance(savedExpense, general?.balance)
        return ExpenseDTO.createExpenseDTO(savedExpense)
    }

    fun saveBalance(expense: Expense, currentBalance: Balance?) {
        currentBalance?.let {
            it.totalExpenses = it.totalExpenses?.add(expense.total)
            it.updatedAt = Date()
            balanceRepository.save(it)
        }
    }

    fun update(expenseDTO: ExpenseDTO): ExpenseDTO? {
        expenseDTO.id ?: throw MalformedObjectException("expenseDTO.id should not be null, please use Save instead")
        val updatedExpense = Expense.createExpense(expenseDTO)
        updatedExpense ?: throw MalformedObjectException("Expense must not be null or empty!")
        updatedExpense.updatedAt = Date()
        return ExpenseDTO.createExpenseDTO(expenseRepository.save(updatedExpense))
    }

    fun delete(id: Long): ExpenseDTO? {
        val deletedExpense = expenseRepository.findById(id).get()
        deletedExpense.updatedAt = Date()
        deletedExpense.deletedAt = Date()
        return ExpenseDTO.createExpenseDTO(expenseRepository.save(deletedExpense))
    }
}
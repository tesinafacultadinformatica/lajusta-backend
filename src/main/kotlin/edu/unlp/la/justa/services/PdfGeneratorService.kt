package edu.unlp.la.justa.services

import be.quodlibet.boxable.BaseTable
import be.quodlibet.boxable.Cell
import be.quodlibet.boxable.HorizontalAlignment
import be.quodlibet.boxable.Row
import be.quodlibet.boxable.VerticalAlignment
import be.quodlibet.boxable.line.LineStyle
import edu.unlp.la.justa.controllers.PdfResult
import edu.unlp.la.justa.repositories.CartRepository
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.font.PDFont
import org.apache.pdfbox.pdmodel.font.PDType1Font
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Service
import java.awt.Color
import java.io.File
import java.net.URL
import java.nio.file.Paths
import java.text.SimpleDateFormat

@Service
class PdfGeneratorService(@Autowired val cartRepository: CartRepository, @Autowired val resourceLoader: ResourceLoader) {

    val fontBold: PDFont = PDType1Font.HELVETICA_BOLD
    val font: PDFont = PDType1Font.HELVETICA
    val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
    val margin = 30f

    private val GREEN_DARK = Color(76, 190, 129)
    private val GREEN = Color(186, 230, 206)
    private val GREEN_2 = Color(216, 255, 236)
    private val ORANGE = Color(243, 212, 159)
    private val WHITE = Color(255, 255, 255)
    private val BLACK = Color(100, 100, 100)

    fun generatePurchasePdf(purchaceId: Long): PdfResult {
        val purchace = cartRepository.findById(purchaceId)

        val document = PDDocument()
        val page = PDPage(PDRectangle.A5)
        document.addPage(page)

        //val path = Paths.get("static/logo.png")
        //val pdImage = PDImageXObject.createFromFile(path.toAbsolutePath().toString(), document)
        val inputStream = URL("http://localhost:8080/static/logo.png").openStream()
        val pdImage = PDImageXObject.createFromByteArray(document, inputStream.readBytes(), "Logo")
        val contents = PDPageContentStream(document, page)
        contents.drawImage(pdImage, 300f, 480f)

        contents.beginText()
        contents.setFont(font, 10.0f)
        contents.newLineAtOffset(margin, 450.0f)
        contents.showText("Detalle de su compra realizada el dia ${simpleDateFormat.format(purchace.get().createdAt)}")
        contents.endText()
        val yStartNewPage = page.mediaBox.height - 2 * margin
        val tableWidth = page.mediaBox.width - 2 * margin
        val drawContent = true
        val yPosition = 420f

        val table = BaseTable(yPosition, yStartNewPage, 0.0f, tableWidth, margin, document, page, true, drawContent)
        this.createRow(table, 10.0f, 10.0f, "Producto", "Cantidad", "Precio", VerticalAlignment.MIDDLE, HorizontalAlignment.CENTER, GREEN_DARK, WHITE, fontBold)
        if (purchace.isPresent) {
            purchace.get().cartProducts.forEachIndexed { index, cartProduct ->
                this.createRow(table, 10.0f, 8.0f, cartProduct.product?.title
                        ?: "", "${cartProduct.quantity}", "${cartProduct.price?.multiply(cartProduct.quantity.toBigDecimal()) ?: 0.0}", VerticalAlignment.MIDDLE, HorizontalAlignment.LEFT, if ((index % 2) == 0) GREEN else ORANGE, BLACK, font)
            }
            this.createRow(table, 10.0f, 10.0f, "TOTAL", purchace.get().cartProducts.sumBy { it.quantity }.toString(), purchace.get().cartProducts.sumByDouble {
                it.price?.multiply(it.quantity.toBigDecimal())?.toDouble() ?: 0.0
            }.toString(), VerticalAlignment.MIDDLE, HorizontalAlignment.CENTER, GREEN_2, BLACK, fontBold)
        }
        table.draw()

        contents.close()
        return PdfResult(document, "${purchace.get().user?.firstName}${purchace.get().user?.lastName}-${simpleDateFormat.format(purchace.get().createdAt)}")
    }

    private fun createRow(table: BaseTable, height: Float, fontSize: Float, product: String, quantity: String, price: String, verticalAlignment: VerticalAlignment, horizontalAlignment: HorizontalAlignment, background: Color, textColor: Color, font: PDFont) {
        val headerRow: Row<PDPage> = table.createRow(height)
        createCell(headerRow, 40.0f, fontSize, product, verticalAlignment, horizontalAlignment, background, textColor, font)
        createCell(headerRow, 25.0f, fontSize, quantity, verticalAlignment, horizontalAlignment, background, textColor, font)
        createCell(headerRow, 35.0f, fontSize, price, verticalAlignment, horizontalAlignment, background, textColor, font)
        table.addHeaderRow(headerRow)
    }

    private fun createCell(headerRow: Row<PDPage>, width: Float, fontSize: Float, value: String, alignment: VerticalAlignment, horizontalAlignment: HorizontalAlignment, background: Color, textColor: Color, font: PDFont) {
        val cell: Cell<PDPage?> = headerRow.createCell(width, value)
        cell.fontSize = fontSize
        cell.valign = alignment
        cell.align = horizontalAlignment
        cell.fillColor = background
        cell.font = font
        cell.textColor = textColor
        cell.setBorderStyle(LineStyle(WHITE, 1.0f))
    }
}
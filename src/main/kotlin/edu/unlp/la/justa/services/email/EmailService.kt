package edu.unlp.la.justa.services.email

import edu.unlp.la.justa.dtos.ResetPasswordDTO
import edu.unlp.la.justa.models.Cart

interface EmailService {
    fun sendRecoveryPasswordEmail(emailAddress: String): Any
    fun sendConfirmationEmail(resetPassword: ResetPasswordDTO): Any
    fun sendNewPurchaseEmail(cart: Cart)
}
package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.PurchaseProducerDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.PurchaseReportDTO
import edu.unlp.la.justa.dtos.UnitDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.PurchaseProducer
import edu.unlp.la.justa.models.Unit
import edu.unlp.la.justa.repositories.PurchaseProducerRepository
import edu.unlp.la.justa.repositories.UnitRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class PurchaseProducerService(@Autowired val purchaseProducerRepository: PurchaseProducerRepository) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?, properties: Set<PropertiesFilterDTO>?): Any {

        val purchaseProducers: Any = if(filterStr == null && rangeStd == null && sortStr == null && properties == null) {
            purchaseProducerRepository.findAll().map{ PurchaseProducerDTO.createPurchaseProducerDTO(it) }.toSet()
        } else {
            val purchases = purchaseProducerRepository.findAll(filterStr, rangeStd, sortStr, properties).mapNotNull{ PurchaseProducerDTO.createPurchaseProducerDTO(it) }.toSet()
            PageableCollectionDTO(totalElements = purchases.size.toLong() , page = purchases)
        }
        return purchaseProducers
    }

    fun getAll() = purchaseProducerRepository.findAll().map { purchaseProducer -> purchaseProducer }.toSet()

    fun get(id: Long): PurchaseProducerDTO? {
        val purchaseProducer = purchaseProducerRepository.findById(id).get()
        return PurchaseProducerDTO.createPurchaseProducerDTO(purchaseProducer)
    }

    fun save(purchaseProducerDTO: PurchaseProducerDTO): PurchaseProducerDTO? {
        purchaseProducerDTO.id?.let { throw MalformedObjectException("purchaseProducerDTO.id should be null, please use Update instead") }
        val newPurchaseProducer = PurchaseProducer.createPurchaseProducer(purchaseProducerDTO)
        val savedPurchaseProducer = purchaseProducerRepository.save(newPurchaseProducer)
        return PurchaseProducerDTO.createPurchaseProducerDTO(savedPurchaseProducer)
    }

    fun update(purchaseProducerDTO: PurchaseProducerDTO): PurchaseProducerDTO? {
        purchaseProducerDTO.id ?: throw MalformedObjectException("purchaseProducerDTO.id should not be null, please use Save instead")
        val updatedPurchaseProducer = PurchaseProducer.createPurchaseProducer(purchaseProducerDTO)
        updatedPurchaseProducer.updatedAt = Date()
        return PurchaseProducerDTO.createPurchaseProducerDTO(purchaseProducerRepository.save(updatedPurchaseProducer))
    }

    fun delete(id: Long): PurchaseProducerDTO? {
        val deletedPurchaseProducer = purchaseProducerRepository.findById(id).get()
        deletedPurchaseProducer.updatedAt = Date()
        deletedPurchaseProducer.deletedAt = Date()
        return PurchaseProducerDTO.createPurchaseProducerDTO(purchaseProducerRepository.save(deletedPurchaseProducer))
    }
}
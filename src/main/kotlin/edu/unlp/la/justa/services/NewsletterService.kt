package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.GeneralDTO
import edu.unlp.la.justa.dtos.NewsletterDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Newsletter
import edu.unlp.la.justa.repositories.NewsletterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class NewsletterService(@Autowired val newsletterRepository: NewsletterRepository) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?) = newsletterRepository.findAll(filterStr, rangeStd, sortStr).map { newsletter -> newsletter }.toList()

    fun get(id: Long): NewsletterDTO? {
        val newsletter = newsletterRepository.findById(id).get()
        return NewsletterDTO.createNewsletterDTO(newsletter)
    }

    fun save(newsletterDTO: NewsletterDTO): NewsletterDTO? {
        newsletterDTO.id?.let { throw MalformedObjectException("newsletterDTO.id should be null, please use Update instead") }
        val newNewsletter = Newsletter.createNewsletter(newsletterDTO)
        newNewsletter ?: throw MalformedObjectException("Newsletter must not be null or empty!")
        val savedNewsletter = newsletterRepository.save(newNewsletter)
        return NewsletterDTO.createNewsletterDTO(savedNewsletter)

    }

    fun activate(id: Long): NewsletterDTO {
        val newsletter = newsletterRepository.findById(id)
        if(!newsletter.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        newsletter.get().deletedAt = null
        newsletter.get().updatedAt = Date()
        newsletterRepository.save(newsletter.get())
        return NewsletterDTO.createNewsletterDTO(newsletter.get())!!
    }

    fun update(newsletterDTO: NewsletterDTO): NewsletterDTO? {
        newsletterDTO.id ?: throw MalformedObjectException("newsletterDTO.id should not be null, please use Save instead")
        val updatednewsletter = Newsletter.createNewsletter(newsletterDTO)
        updatednewsletter ?: throw MalformedObjectException("Newsletter must not be null or empty!")
        updatednewsletter.updatedAt = Date()
        return NewsletterDTO.createNewsletterDTO(newsletterRepository.save(updatednewsletter))
    }

    fun delete(newsletterDTO: NewsletterDTO) {
        newsletterDTO.id ?: throw MalformedObjectException("newsletterDTO.id should not be null")
        val deletedNewsletter = Newsletter.createNewsletter(newsletterDTO)
        deletedNewsletter ?: throw MalformedObjectException("Newsletter must not be null or empty!")
        deletedNewsletter.updatedAt = Date()
        deletedNewsletter.deletedAt = Date()
        newsletterRepository.save(deletedNewsletter)
    }
}
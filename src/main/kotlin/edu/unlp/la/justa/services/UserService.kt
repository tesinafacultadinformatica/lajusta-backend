package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.UserDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Image
import edu.unlp.la.justa.models.Role
import edu.unlp.la.justa.models.User
import edu.unlp.la.justa.repositories.UserRepository
import edu.unlp.la.justa.security.EncoderFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional


@Service
class UserService(@Autowired val userRepository: UserRepository,
                  @Autowired val encoder: EncoderFactory,
                  @Autowired val imageService: ImageService) : UserDetailsService {

    fun getAll(filterStr: String?, rangeStd:String?, sortStr: String?) : PageableCollectionDTO {
        val user =  userRepository.findAll(filterStr, rangeStd, sortStr).mapNotNull { role -> UserDTO.createUserDTO(role) }.toSet()
        val totalElements = userRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = user)
    }

    fun getStaff(): PageableCollectionDTO {
        val user =  userRepository.findStaff().mapNotNull { user -> UserDTO.createUserDTO(user) }.toSet()
        val totalElements = user.size.toLong()

        return PageableCollectionDTO(totalElements = totalElements, page = user)
    }
    @Throws(NoSuchElementException::class)
    fun get(id: Long): UserDTO? {
        val user = userRepository.findById(id).get()
        return UserDTO.createUserDTO(user, user.image?.route)
    }

    fun save(userDTO: UserDTO): UserDTO? {
        userDTO.id?.let { throw MalformedObjectException("userDTO.id should be null, please use update instead") }
        val newImage = imageService.save(userDTO.email, userDTO.image)
        val newUser = User.createUser(userDTO, newImage)
        newUser ?: throw MalformedObjectException("User object should not be null!!!")
        newUser.encryptedPassword = encoder.encode(userDTO.encryptedPassword)
        newUser.updatedAt = Date()
        newUser.createdAt = Date()
        return UserDTO.createUserDTO(userRepository.save(newUser))
    }

    fun activate(id: Long): UserDTO {
        val user = userRepository.findById(id)
        if(!user.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        user.get().deletedAt = null
        user.get().updatedAt = Date()
        userRepository.save(user.get())
        return UserDTO.createUserDTO(user.get())!!
    }

    fun setStaff(userDTO: UserDTO, token: String){
        if (userDTO.role != 1L ){
            update(userDTO, token)
        }
    }

    @Transactional
    fun update(userDTO: UserDTO, token: String): UserDTO? {
        userDTO.id ?: throw MalformedObjectException("userDTO.id should not be null, please use create instead")
        var user = userRepository.findUserByToken(token)
        val image: Image?
        if(user.id != userDTO.id) {
            user = userRepository.findById(userDTO.id).get()
            user.role = Role(id = userDTO.role)
        } else {
            if (userDTO.image?.id == null) {
                image = imageService.save(userDTO.email, userDTO.image)
            } else {
                image = Image.createImage(userDTO.image)
            }
            user.image = image
        }
        user.firstName = userDTO.firstName
        user.lastName = userDTO.lastName
        user.email = userDTO.email
        userDTO.address?.let {
            user.address?.street = it.street
            user.address?.number = it.number
            user.address?.floor = it.floor
            user.address?.apartment = it.apartment
            user.address?.betweenStreets = it.betweenStreets
            user.address?.description = it.description
        }
        user.updatedAt = Date()
        return UserDTO.createUserDTO(userRepository.save(user))
    }

    fun delete(id: Long): UserDTO? {
        val user = userRepository.findById(id).get()
        user.deletedAt = Date()
        user.updatedAt = Date()
        return UserDTO.createUserDTO(userRepository.save(user))
    }

    private fun getAuthority(user: User): Set<SimpleGrantedAuthority> {
        val authorities: MutableSet<SimpleGrantedAuthority> = mutableSetOf()
        user.role?.let { authorities.add(SimpleGrantedAuthority("ROLE_${it.description?.toUpperCase()}")) }
        return authorities
    }

    override fun loadUserByUsername(username: String?): UserDetails? {
        val user: User = userRepository.findByEmail(username!!)
        return org.springframework.security.core.userdetails.User(user.email, user.password, getAuthority(user))
    }
}

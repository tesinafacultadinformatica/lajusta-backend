package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.UnitDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Unit
import edu.unlp.la.justa.repositories.UnitRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class UnitService(@Autowired val unitRepository: UnitRepository) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?): PageableCollectionDTO {
        val units = unitRepository.findAll(filterStr, rangeStd, sortStr).map { unit -> unit }.toSet()
        val totalElements = unitRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = units)
    }

    fun getAll() = unitRepository.findAll().map { unit -> unit }.toSet()

    fun get(id: Long): UnitDTO? {
        val unit = unitRepository.findById(id).get()
        return UnitDTO.createUnitDTO(unit)
    }

    fun save(unitDTO: UnitDTO): UnitDTO? {
        unitDTO.id?.let { throw MalformedObjectException("unitDTO.id should be null, please use Update instead") }
        val newUnit = Unit.createUnit(unitDTO)
        newUnit ?: throw MalformedObjectException("Unit must not be null or empty!")
        val savedUnit = unitRepository.save(newUnit)
        return UnitDTO.createUnitDTO(savedUnit)

    }

    fun activate(id: Long): UnitDTO {
        val unit = unitRepository.findById(id)
        if (!unit.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        unit.get().deletedAt = null
        unit.get().updatedAt = Date()
        unitRepository.save(unit.get())
        return UnitDTO.createUnitDTO(unit.get())!!
    }

    fun update(unitDTO: UnitDTO): UnitDTO? {
        unitDTO.id ?: throw MalformedObjectException("unitDTO.id should not be null, please use Save instead")
        val updatedunit = Unit.createUnit(unitDTO)
        updatedunit ?: throw MalformedObjectException("Unit must not be null or empty!")
        updatedunit.updatedAt = Date()
        return UnitDTO.createUnitDTO(unitRepository.save(updatedunit))
    }

    fun delete(id: Long): UnitDTO? {
        val deletedUnit = unitRepository.findById(id).get()
        deletedUnit.updatedAt = Date()
        deletedUnit.deletedAt = Date()
        return UnitDTO.createUnitDTO(unitRepository.save(deletedUnit))
    }
}
package edu.unlp.la.justa.services.email

import com.google.common.hash.Hashing
import edu.unlp.la.justa.dtos.ResetPasswordDTO
import edu.unlp.la.justa.dtos.ResetPasswordMessage
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.models.Cart
import edu.unlp.la.justa.models.ResetPassword
import edu.unlp.la.justa.repositories.ResetPasswordRepository
import edu.unlp.la.justa.repositories.UserRepository
import edu.unlp.la.justa.security.EncoderFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.nio.charset.StandardCharsets
import java.util.*

@Component
@Profile("!prod")
class EmailServiceLocal(@Autowired val resetPasswordRepository: ResetPasswordRepository,
                        @Autowired val userRepository: UserRepository,
                        @Autowired val encoder: EncoderFactory) : EmailService {

    override fun sendRecoveryPasswordEmail(emailAddress: String): Any {
        val sha256hex = Hashing.sha256().hashString(Random().nextInt().toString(), StandardCharsets.UTF_8).toString()
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE, 30)
        val lastEmailCode = resetPasswordRepository.findById(emailAddress)
        if (lastEmailCode.isPresent) {
            lastEmailCode.get().resetCode = sha256hex
            lastEmailCode.get().timeToLive = calendar.time
            resetPasswordRepository.save(lastEmailCode.get())
        } else {
            val resetPasswordCode = ResetPassword(emailAddress, sha256hex, calendar.time)
            resetPasswordRepository.save(resetPasswordCode)
        }
        return ResetPasswordMessage("Reset code has been created successfully!", sha256hex)
    }

    override fun sendConfirmationEmail(resetPassword: ResetPasswordDTO): Any {
        val user = userRepository.findByEmail(resetPassword.email)
        val dataBaseResetCode = resetPasswordRepository.findById(resetPassword.email)
        val responseMessage: ResetPasswordMessage
        if (dataBaseResetCode.isPresent && dataBaseResetCode.get().resetCode == resetPassword.code) {
            user.encryptedPassword = encoder.encode(resetPassword.newPassword)
            userRepository.save(user)
            resetPasswordRepository.delete(dataBaseResetCode.get())
            responseMessage = ResetPasswordMessage("Password has been restored!")
        } else {
            throw MalformedObjectException("Invalid email or confirmation code, please try again")
        }
        return responseMessage
    }

    override fun sendNewPurchaseEmail(cart: Cart) {

    }
}
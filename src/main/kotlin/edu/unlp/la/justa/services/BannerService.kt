package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.BannerDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Banner
import edu.unlp.la.justa.repositories.BannerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.text.SimpleDateFormat
import java.util.*

@Service
class BannerService(@Autowired val bannerRepository: BannerRepository,
                    @Autowired val imageService: ImageService) {

    val formatDate: SimpleDateFormat = SimpleDateFormat("dd_MM_YYYY_hh_mm_ss")

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?,properties:Set<PropertiesFilterDTO>?): PageableCollectionDTO{
        val banners = bannerRepository.findAll(filterStr, rangeStd, sortStr, properties).map { banner -> BannerDTO.createBannerDTO(banner) }.toSet()
        val totalElements = bannerRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = banners)
    }

    fun getAll() = bannerRepository.findAllByDeletedAtIsNull().map { banner -> BannerDTO.createBannerDTO(banner) }.toSet()

    fun get(id: Long): BannerDTO? {
        val banner = bannerRepository.findById(id).get()
        return BannerDTO.createBannerDTO(banner)
    }

    fun save(bannerDTO: BannerDTO): BannerDTO? {
        bannerDTO.id?.let { throw MalformedObjectException("bannerDTO.id should be null, please use Update instead") }
        val newImage = imageService.save(formatDate.format(Date()), bannerDTO.image)
        val newBanner = Banner.createBanner(bannerDTO, newImage)
        newBanner ?: throw MalformedObjectException("Banner must not be null or empty!")
        val savedBanner = bannerRepository.save(newBanner)
        return BannerDTO.createBannerDTO(savedBanner)
    }

    fun activate(id: Long): BannerDTO {
        val banner = bannerRepository.findById(id)
        if(!banner.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        banner.get().deletedAt = null
        banner.get().updatedAt = Date()
        bannerRepository.save(banner.get())
        return BannerDTO.createBannerDTO(banner.get())
    }

    fun update(bannerDTO: BannerDTO): BannerDTO? {
        bannerDTO.id ?: throw MalformedObjectException("bannerDTO.id should not be null, please use Save instead")
        val updatedbanner: Banner?
        if(bannerDTO.image?.id == null) {
            val newImage = imageService.save(formatDate.format(Date()), bannerDTO.image)
            updatedbanner = Banner.createBanner(bannerDTO, newImage)
        } else {
            updatedbanner = Banner.createBanner(bannerDTO)
        }
        updatedbanner ?: throw MalformedObjectException("Banner must not be null or empty!")
        updatedbanner.updatedAt = Date()
        return BannerDTO.createBannerDTO(bannerRepository.save(updatedbanner))
    }

    fun delete(id: Long): BannerDTO? {
        val deletedBanner = bannerRepository.findById(id).get()
        deletedBanner.updatedAt = Date()
        deletedBanner.deletedAt = Date()
        return BannerDTO.createBannerDTO(bannerRepository.save(deletedBanner))
    }
}
package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.NodeDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Node
import edu.unlp.la.justa.repositories.NodeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class NodeService(@Autowired val nodeRepository: NodeRepository, @Autowired val imageService: ImageService) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?): PageableCollectionDTO {
        val nodes = nodeRepository.findAll(filterStr, rangeStd, sortStr).mapNotNull { node -> NodeDTO.createNodeDTO(node) }.toSet()
        val totalElements = nodeRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = nodes)
    }

    fun getAll() = nodeRepository.findAll().mapNotNull { node -> NodeDTO.createNodeDTO(node) }.toSet()

    fun get(id: Long): NodeDTO? {
        val node = nodeRepository.findById(id).get()
        return NodeDTO.createNodeDTO(node, node.image?.route)
    }

    fun save(nodeDTO: NodeDTO): NodeDTO? {
        nodeDTO.id?.let { throw MalformedObjectException("nodeDTO.id should be null, please use Update instead") }
        val newImage = imageService.save(nodeDTO.name, nodeDTO.image)
        val newNode = Node.createNode(nodeDTO, newImage)
        newNode ?: throw MalformedObjectException("Node must not be null or empty!")
        val savedNode = nodeRepository.save(newNode)
        return NodeDTO.createNodeDTO(savedNode)
    }

    fun activate(id: Long): NodeDTO {
        val node = nodeRepository.findById(id)
        if(!node.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        node.get().deletedAt = null
        node.get().updatedAt = Date()
        nodeRepository.save(node.get())
        return NodeDTO.createNodeDTO(node.get())
    }

    @Transactional
    fun update(nodeDTO: NodeDTO): NodeDTO? {
        nodeDTO.id ?: throw MalformedObjectException("nodeDTO.id should not be null, please use Save instead")
        val updatednode = Node.createNode(nodeDTO)
        updatednode ?: throw MalformedObjectException("Node must not be null or empty!")
        if(nodeDTO.image?.id == null){
            updatednode.image = imageService.save(nodeDTO.name, nodeDTO.image)
        }
        updatednode.updatedAt = Date()
        return NodeDTO.createNodeDTO(nodeRepository.save(updatednode))
    }

    fun delete(id: Long): NodeDTO? {
        val deletedNode = nodeRepository.findById(id).get()
        deletedNode.updatedAt = Date()
        deletedNode.deletedAt = Date()
        return NodeDTO.createNodeDTO(nodeRepository.save(deletedNode))
    }
}
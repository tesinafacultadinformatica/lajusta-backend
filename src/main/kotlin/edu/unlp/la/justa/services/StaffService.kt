package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.StaffDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Staff
import edu.unlp.la.justa.models.User
import edu.unlp.la.justa.repositories.StaffRepository
import edu.unlp.la.justa.repositories.UserRepository
import edu.unlp.la.justa.repositories.UserTokenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class StaffService(@Autowired val staffRepository: StaffRepository,
                   @Autowired val userRepository: UserRepository) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?): Any {
        val staffMembers: Any = if (filterStr == null && rangeStd == null && sortStr == null) {
            staffRepository.findAll().map { StaffDTO.createStaffDTO(it) }.toSet()
        } else {
            val totalElements = staffRepository.count()
            PageableCollectionDTO(totalElements = totalElements, page = staffRepository.findAll(filterStr, rangeStd, sortStr).mapNotNull { StaffDTO.createStaffDTO(it) }.toSet())
        }
        return staffMembers
    }

    fun get(id: Long): StaffDTO? {
        val staff = staffRepository.findById(id).get()
        return StaffDTO.createStaffDTO(staff)
    }

    fun save(token: String, staffDTO: StaffDTO): StaffDTO? {
        staffDTO.id?.let { throw MalformedObjectException("staffDTO.id should be null, please use Update instead") }
        val user = userRepository.findUserByToken(token)
        val newStaff = Staff.createStaff(staffDTO).apply {
            this?.user = user
            this?.createdAt = Date()
            this?.updatedAt = Date()
        }
        newStaff ?: throw MalformedObjectException("Staff must not be null or empty!")
        val savedStaff = staffRepository.save(newStaff)
        return StaffDTO.createStaffDTO(savedStaff)
    }

    fun activate(id: Long): StaffDTO {
        val staff = staffRepository.findById(id)
        if (!staff.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        staff.get().deletedAt = null
        staff.get().updatedAt = Date()
        staffRepository.save(staff.get())
        return StaffDTO.createStaffDTO(staff.get())!!
    }

    fun update(token: String, staffDTO: StaffDTO): StaffDTO? {
        staffDTO.id ?: throw MalformedObjectException("staffDTO.id should not be null, please use Save instead")
        val user = userRepository.findUserByToken(token)
        val updatedstaff = Staff.createStaff(staffDTO).apply {
            this?.user = user
            this?.updatedAt = Date()
        }
        updatedstaff ?: throw MalformedObjectException("Staff must not be null or empty!")
        return StaffDTO.createStaffDTO(staffRepository.save(updatedstaff))
    }

    fun delete(id: Long): StaffDTO? {
        val deletedStaff = staffRepository.findById(id).get()
        deletedStaff.updatedAt = Date()
        deletedStaff.deletedAt = Date()
        return StaffDTO.createStaffDTO(staffRepository.save(deletedStaff))
    }
}
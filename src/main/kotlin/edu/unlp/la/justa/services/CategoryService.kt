package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.CategoryDTO
import edu.unlp.la.justa.dtos.ImageDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Category
import edu.unlp.la.justa.repositories.CategoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

@Service
class CategoryService(@Autowired val categoryRepository: CategoryRepository,
                      @Autowired val imageService: ImageService,
                      @Value("\${la.justa.category.icons.folder.name}") val folderName: String) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?, properties: Set<PropertiesFilterDTO>?): PageableCollectionDTO {
        val categoryDtoList = categoryRepository.findAll(filterStr, rangeStd, sortStr, properties).map {
            val catDto = CategoryDTO.createCategoryDTO(it)
            it.image?.route?.let { route -> catDto.image = ImageDTO.createImageDTO(it.image, route) }
            catDto
        }.toSet()
        val totalElements = categoryRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = categoryDtoList)
    }

    fun getAll() = categoryRepository.findAll().map {
        val catDto = CategoryDTO.createCategoryDTO(it)
        it.image?.route?.let { route -> catDto.image = ImageDTO.createImageDTO(it.image, route) }
        catDto
    }.toSet()

    fun get(id: Long): CategoryDTO {
        val category = categoryRepository.findById(id).get()
        val categoryDto = CategoryDTO.createCategoryDTO(category)
        category.image?.let { categoryDto.image = ImageDTO.createImageDTO(it, it.route) }
        return categoryDto
    }

    fun save(categoryDTO: CategoryDTO): CategoryDTO {
        categoryDTO.id?.let { throw MalformedObjectException("categoryDTO.id should be null, please use Update instead") }
        val category = Category.createCategory(categoryDTO)
        category.image = imageService.save(folderName = folderName, imageDTO = categoryDTO.image)
        val newCategory = categoryRepository.save(category)
        return CategoryDTO.createCategoryDTO(newCategory)
    }

    fun activate(id: Long): CategoryDTO {
        val category = categoryRepository.findById(id)
        if(!category.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        category.get().deletedAt = null
        category.get().updatedAt = Date()
        categoryRepository.save(category.get())
        return CategoryDTO.createCategoryDTO(category.get())
    }

    fun update(categoryDTO: CategoryDTO): CategoryDTO {
        categoryDTO.id ?: throw MalformedObjectException("categoryDTO.id should be null, please use Update instead")
        val updatedCategory = Category.createCategory(categoryDTO)
        if(categoryDTO.image != null){
            updatedCategory.image = if(categoryDTO.image?.id == null) imageService.save(folderName = folderName, imageDTO = categoryDTO.image) else imageService.findById(categoryDTO.image?.id as Long)
        }
        updatedCategory.updatedAt = Date()
        return CategoryDTO.createCategoryDTO(categoryRepository.save(updatedCategory))
    }

    fun delete(id: Long): CategoryDTO {
        val deletedCategory = categoryRepository.findById(id).get()
        deletedCategory.deletedAt = Date()
        return CategoryDTO.createCategoryDTO(categoryRepository.save(deletedCategory))
    }
}
package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.AvailableNodeDTO
import edu.unlp.la.justa.dtos.GeneralDTO
import edu.unlp.la.justa.dtos.NodeDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.*
import edu.unlp.la.justa.models.*
import edu.unlp.la.justa.repositories.*
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*
import javax.transaction.Transactional

data class RowCount(var count: Int = 0)

class TotalProductReport(val id: Long, val title: String, var totalQuantity: Int = 0, var totalPrice: BigDecimal = BigDecimal.ZERO)

@Service
class GeneralService(@Value("\${la.justa.general.error.message.duplicated.open.date}") val errorMessageDuplicatedOpenDate: String,
                     @Value("\${la.justa.general.error.message.duplicated.close.date}") val errorMessageDuplicatedCloseDate: String,
                     @Autowired val generalRepository: GeneralRepository,
                     @Autowired val balanceRepository: BalanceRepository,
                     @Autowired val userRepository: UserRepository,
                     @Autowired val availableNodeRepository: AvailableNodeRepository,
                     @Autowired val purchaseProducerRepository: PurchaseProducerRepository,
                     @Autowired val expenseRepository: ExpenseRepository,
                     @Autowired val cartRepository: CartRepository) {

    val simpleDateFormater = SimpleDateFormat("dd MMM yyyy")
    val rowErrorMessage = "##ERROR##"

    val colors = mapOf(
        0 to IndexedColors.BRIGHT_GREEN,
        1 to IndexedColors.LIGHT_CORNFLOWER_BLUE,
        2 to IndexedColors.LIGHT_TURQUOISE,
        3 to IndexedColors.LIGHT_ORANGE,
        4 to IndexedColors.LIGHT_BLUE,
        5 to IndexedColors.LIGHT_GREEN,
        6 to IndexedColors.LIGHT_YELLOW,
        7 to IndexedColors.RED,
        8 to IndexedColors.BRIGHT_GREEN,
        9 to IndexedColors.BLUE,
        10 to IndexedColors.YELLOW,
        11 to IndexedColors.PINK,
        12 to IndexedColors.TURQUOISE,
        13 to IndexedColors.GREEN,
        14 to IndexedColors.VIOLET,
        15 to IndexedColors.TEAL,
        16 to IndexedColors.GREY_25_PERCENT,
        17 to IndexedColors.CORNFLOWER_BLUE,
        18 to IndexedColors.MAROON,
        19 to IndexedColors.ORCHID,
        20 to IndexedColors.CORAL,
        21 to IndexedColors.ROYAL_BLUE,
        22 to IndexedColors.LIGHT_CORNFLOWER_BLUE,
        23 to IndexedColors.SKY_BLUE,
        24 to IndexedColors.LIGHT_TURQUOISE,
        25 to IndexedColors.PALE_BLUE,
        26 to IndexedColors.ROSE,
        27 to IndexedColors.LAVENDER,
        28 to IndexedColors.TAN,
        29 to IndexedColors.AQUA,
        30 to IndexedColors.LIME,
        31 to IndexedColors.ORANGE,
        32 to IndexedColors.BLUE_GREY,
        33 to IndexedColors.SEA_GREEN,
        34 to IndexedColors.BROWN,
        35 to IndexedColors.PLUM,
        36 to IndexedColors.INDIGO
    )

    companion object {
        fun getOpenAndCloseDate(calendar: Calendar, date: Date, hour: String): Date {
            calendar.time = date
            calendar.set(Calendar.HOUR_OF_DAY, hour.split(':').get(0).toInt())
            calendar.set(Calendar.MINUTE, hour.split(':').get(1).toInt())
            return calendar.time
        }
    }

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?): PageableCollectionDTO {
        val generalList = generalRepository.findAll(filterStr, rangeStd, sortStr).mapNotNull { this.createGeneralDTO(it, false) }.toSet()
        val totalElements = generalRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = generalList)
    }

    fun createGeneralDTO(general: General, showAvailableNodeImage: Boolean = true): GeneralDTO? {
        val generalDTO = GeneralDTO.createGeneralDTO(general)
        generalDTO?.activeNodes = mutableSetOf()
        general.nodes?.forEach {
            val simpleFormatter = SimpleDateFormat("HH:mm")
            val availableNodeDTO = AvailableNodeDTO()
            availableNodeDTO.id = it.id
            availableNodeDTO.node = NodeDTO.createNodeDTO(it.node, showAvailableNodeImage)
            availableNodeDTO.day = it.dateClose
            availableNodeDTO.dateTimeFrom = simpleFormatter.format(it.dateOpen)
            availableNodeDTO.dateTimeTo = simpleFormatter.format(it.dateClose)
            (generalDTO?.activeNodes as MutableSet<AvailableNodeDTO>).add(availableNodeDTO)
        }
        generalDTO?.canClose = generalDTO?.activeNodes?.all { Date().after(it.day) }?.and(general.closedAt == null)!!
        return generalDTO
    }

    fun get(id: Long): GeneralDTO? {
        val general = generalRepository.findById(id).get()
        val generalDTO = this.createGeneralDTO(general)
        generalDTO?.canClose = generalDTO?.activeNodes?.all { Date().after(it.day) }?.and(general.closedAt == null)!!
        return generalDTO
    }

    fun getActiveGeneral(): GeneralDTO? {
        val general = generalRepository.findActive(Date())
        var generalDTO: GeneralDTO? = null
        general?.let { generalDTO = this.createGeneralDTO(it, false) }
        return generalDTO
    }

    fun getNextActiveGeneral(): GeneralDTO? {
        generalRepository.findActive(Date()) ?: generalRepository.findNextActive(Date())?.let { return this.createGeneralDTO(it)}
        return null
    }

    @Transactional
    @Throws(MalformedObjectException::class, UniqueGeneralViolation::class)
    fun save(token: String, generalDTO: GeneralDTO): GeneralDTO? {
        generalDTO.id?.let { throw MalformedObjectException("GeneralDTO.id should be null, please use Update instead") }
        val user = userRepository.findUserByToken(token)
        var general = generalRepository.findActive(generalDTO.dateActivePage)
        if (general != null) throw UniqueGeneralViolation(errorMessageDuplicatedOpenDate)

        general = generalRepository.findActive(generalDTO.dateDownPage)
        if (general != null) throw UniqueGeneralViolation(errorMessageDuplicatedCloseDate)

        general = General.createGeneral(generalDTO)
        general.user = user
        val calendar = Calendar.getInstance()
        calendar.time = general.dateDownPage
        calendar.add(Calendar.HOUR, 23)
        calendar.add(Calendar.MINUTE, 59)
        general.dateDownPage = calendar.time
        generalDTO.activeNodes?.let {
            general.nodes = mutableListOf()
            it.forEach { ndto ->
                val availableNode = AvailableNode()
                availableNode.general = general
                availableNode.node = Node(id = (ndto.node as Int).toLong())
                availableNode.dateOpen = getOpenAndCloseDate(calendar, ndto.day ?: Date(), ndto.dateTimeFrom ?: "00:00")
                availableNode.dateClose = getOpenAndCloseDate(calendar, ndto.day ?: Date(), ndto.dateTimeTo ?: "00:00")
                (general.nodes as MutableList).add(availableNode)
            }
        }
        val newGeneral = generalRepository.save(general)
        balanceRepository.save(Balance(general = newGeneral, createdAt = Date(), updatedAt = Date(), openDate = newGeneral.dateStartUpPage))
        return GeneralDTO.createGeneralDTO(newGeneral)
    }

    fun activate(id: Long): GeneralDTO {
        val general = generalRepository.findById(id)
        if (!general.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        val generalOverlaying = generalRepository.findGeneralOverlaying(general.get().id, general.get().dateStartUpPage, general.get().dateDownPage)
        if(generalOverlaying != 0) throw ActiveGeneralNotFound("No podemos activar esta Ronda porque las fechas se superponene con otra Ronda")
        general.get().deletedAt = null
        general.get().updatedAt = Date()
        generalRepository.save(general.get())
        return GeneralDTO.createGeneralDTO(general.get())!!
    }

    fun update(token: String, generalDTO: GeneralDTO): GeneralDTO? {
        generalDTO.id ?: throw MalformedObjectException("cartDTO.id should be null, please use Update instead")
        val user = userRepository.findUserByToken(token)
        val general = General.createGeneral(generalDTO)
        general.user = user
        val availableNodes = mutableListOf<AvailableNode>()
        val calendar = Calendar.getInstance()
        calendar.time = general.dateDownPage
        calendar.add(Calendar.HOUR, 23)
        calendar.add(Calendar.MINUTE, 59)
        general.dateDownPage = calendar.time
        generalDTO.activeNodes?.forEach {
            availableNodes.add(AvailableNode(id = it.id,
                general = general,
                node = Node(id = (it.node as Int).toLong()),
                dateClose = getOpenAndCloseDate(calendar, it.day ?: Date(), it.dateTimeTo ?: "00:00"),
                dateOpen  = getOpenAndCloseDate(calendar, it.day ?: Date(), it.dateTimeFrom ?: "00:00")))
        }
        removeNodes(general.id, availableNodes)
        general.nodes = availableNodes
        general.updatedAt = Date()
        val updatedGeneral = generalRepository.save(general)
        return GeneralDTO.createGeneralDTO(updatedGeneral)
    }

    private fun removeNodes(generalId: Long?, availableNodes: List<AvailableNode>){
        generalId?.let {
            val general = generalRepository.findById(generalId)
            if(general.isPresent) {
                general.get().nodes?.filter { dataBaseNode -> availableNodes.find({ newNode -> newNode.id == dataBaseNode.id }) == null }?.forEach { node ->
                    node.general = null
                    availableNodeRepository.delete(node)
                }
            }
        }
    }

    fun delete(id: Long): GeneralDTO? {
        val deletedCart = generalRepository.findById(id).get()
        deletedCart.deletedAt = Date()
        return GeneralDTO.createGeneralDTO(generalRepository.save(deletedCart))
    }

    fun generatePurchacesReport(generalDTO: GeneralDTO): XSSFWorkbook {
        generalDTO.id ?: throw MalformedObjectException("generalDto.id should not be null!!!")
        val carts = cartRepository.findCartByGeneralAndCanceledFalse(General(id = generalDTO.id))
        val products = mutableSetOf<Product>()
        val finalRowTotalProducts = mutableListOf<TotalProductReport>()
        carts.forEach { it.cartProducts.forEach { it.product?.let { products.add(it) } } }

        val workbook = XSSFWorkbook()
        val sheet = workbook.createSheet("Pedidos")
        val header = sheet.createRow(0)

        val noneCellStyle = createCellStyle(workbook, IndexedColors.WHITE, BorderStyle.THIN)
        val grayCellStyle = createCellStyle(workbook, IndexedColors.GREY_25_PERCENT, BorderStyle.THIN)
        val gray50CellStyle = createCellStyle(workbook, IndexedColors.GREY_50_PERCENT, BorderStyle.THIN)


        val headerStyle: CellStyle = workbook.createCellStyle()
        headerStyle.fillForegroundColor = IndexedColors.WHITE.index
        headerStyle.setFillPattern(FillPatternType.BRICKS)
        val headerCell = header.createCell(0)
        headerCell.setCellStyle(headerStyle)

        val font = workbook.createFont()
        font.fontName = "Arial"
        font.fontHeightInPoints = 12.toShort()
        headerStyle.setFont(font)

        products.forEachIndexed { index, product ->
            finalRowTotalProducts.add(TotalProductReport(id = product.id!!, title = product.title!!))
            val productHeadStyle: CellStyle = workbook.createCellStyle()
            productHeadStyle.fillForegroundColor = colors[index]?.index ?: IndexedColors.GREY_25_PERCENT.index
            productHeadStyle.setFillPattern(FillPatternType.BRICKS)

            sheet.addMergedRegion(CellRangeAddress(0, 0, (index * 3) + 1, (index * 3) + 3))
            val productHeadCell = header.createCell((index * 3) + 1)
            productHeadCell.setCellValue(product.title)
            productHeadCell.setCellStyle(productHeadStyle)
        }

        var index = 0
        carts.groupBy(Cart::user).toSortedMap(UserComparator()).forEach { user ->
            user.value.forEach { cart ->
                val newCartRow = sheet.createRow(index + 1)
                val dateCell = newCartRow.createCell(0)
                dateCell.setCellValue(cart.createdAt.toString())
                sheet.autoSizeColumn(dateCell.columnIndex)
                var totalItemQuantity = 0
                var totalItemPrice = BigDecimal.ZERO
                products.forEachIndexed { indexProduct, product ->
                    val cartProduct = cart.cartProducts.filter { it.product!!.id == product.id }.firstOrNull()
                    val cartTotalProduct = finalRowTotalProducts.filter { it.id == product.id }.firstOrNull()
                    val productoCellQuantity = newCartRow.createCell((indexProduct * 3) + 1)
                    val productoCellPrice = newCartRow.createCell((indexProduct * 3) + 2)
                    val productoCellTotal = newCartRow.createCell((indexProduct * 3) + 3)
                    if (cartProduct != null) {
                        productoCellQuantity.setCellValue(cartProduct.quantity.toDouble())
                        cartTotalProduct!!.totalQuantity = cartTotalProduct.totalQuantity + cartProduct.quantity
                        totalItemQuantity = totalItemQuantity + cartProduct.quantity
                        productoCellPrice.setCellValue(cartProduct.getPriceOfProduct()?.toDouble()!!)
                        val total = cartProduct.getTotalPrice()
                        cartTotalProduct.totalPrice = cartTotalProduct.totalPrice + total!!
                        productoCellTotal.setCellValue(total.toDouble())
                        totalItemPrice = totalItemPrice + total
                    } else {
                        productoCellQuantity.setCellValue(0.0)
                        productoCellPrice.setCellValue(0.0)
                        productoCellTotal.setCellValue(0.0)
                    }
                }
                var pos = 1
                val totalAmount = newCartRow.createCell((products.size * 3) + pos++)
                totalAmount.setCellValue(totalItemQuantity.toDouble())

                val totalPrice = newCartRow.createCell((products.size * 3) + pos++)
                totalPrice.setCellValue(totalItemPrice.toDouble())

                val name = newCartRow.createCell((products.size * 3) + pos++)
                name.setCellValue("${cart.user?.firstName} ${cart.user?.lastName}")
                sheet.autoSizeColumn(name.columnIndex)

                val address = newCartRow.createCell((products.size * 3) + pos++)
                address.setCellValue("${cart.user?.address?.street ?: ""} " +
                        "${cart.user?.address?.number ?: ""} " +
                        "${cart.user?.address?.betweenStreets ?: ""}" +
                        "${cart.user?.address?.floor ?: ""}" +
                        "${cart.user?.address?.apartment ?: ""}")
                sheet.autoSizeColumn(address.columnIndex)

                val phone = newCartRow.createCell((products.size * 3) + pos++)
                phone.setCellValue(cart.user?.phone)
                sheet.autoSizeColumn(phone.columnIndex)

                val observation = newCartRow.createCell((products.size * 3) + pos++)
                observation.setCellValue(cart.observation)
                sheet.autoSizeColumn(observation.columnIndex)

                val email = newCartRow.createCell((products.size * 3) + pos++)
                email.setCellValue(cart.user?.email)
                sheet.autoSizeColumn(email.columnIndex)

                val node = newCartRow.createCell((products.size * 3) + pos++)
                node.setCellValue(cart.nodeAvailable?.node?.name)
                sheet.autoSizeColumn(node.columnIndex)

                val purchaseID = newCartRow.createCell((products.size * 3) + pos)
                purchaseID.setCellValue(cart.id.toString())
                sheet.autoSizeColumn(purchaseID.columnIndex)

                index++
            }
        }

        val totalsRow = sheet.createRow(carts.size + 1)
        val productHeadStyle: CellStyle = workbook.createCellStyle()
        productHeadStyle.fillForegroundColor = IndexedColors.LEMON_CHIFFON.index
        productHeadStyle.setFillPattern(FillPatternType.BRICKS)
        val finalTotalsStyle: CellStyle = workbook.createCellStyle()
        finalTotalsStyle.fillForegroundColor = IndexedColors.GOLD.index
        finalTotalsStyle.setFillPattern(FillPatternType.BRICKS)

        finalRowTotalProducts.forEachIndexed { i, product ->
            val totalQuantityCell = totalsRow.createCell((i * 3) + 1)
            totalQuantityCell.setCellStyle(productHeadStyle)
            totalQuantityCell.setCellValue(product.totalQuantity.toDouble())

            val totalPriceCell = totalsRow.createCell((i * 3) + 3)
            totalPriceCell.setCellStyle(productHeadStyle)
            totalPriceCell.setCellValue(product.totalPrice.toDouble())
        }
        val totalQuantityForAllProductsCell = totalsRow.createCell((finalRowTotalProducts.size * 3) + 1)
        totalQuantityForAllProductsCell.setCellValue(finalRowTotalProducts.map { it.totalQuantity }.sum().toDouble())
        totalQuantityForAllProductsCell.setCellStyle(finalTotalsStyle)
        val totalPriceForAllProductsCell = totalsRow.createCell((finalRowTotalProducts.size * 3) + 2)
        totalPriceForAllProductsCell.setCellValue(finalRowTotalProducts.map { it.totalPrice.toDouble() }.sum())
        totalPriceForAllProductsCell.setCellStyle(finalTotalsStyle)

        createNodeSummarySheet(carts, workbook, noneCellStyle, grayCellStyle, gray50CellStyle)
        createPaymentSheet(carts, workbook, noneCellStyle, grayCellStyle, gray50CellStyle)

        val cartsGroupedByNode = carts.groupBy { it.nodeAvailable?.node }
        createNodeReportSheet(cartsGroupedByNode, workbook, noneCellStyle, grayCellStyle, gray50CellStyle)

        return workbook
    }

    private fun createPaymentSheet(carts: List<Cart>, workbook: XSSFWorkbook, noneCellStyle: CellStyle, grayCellStyle: CellStyle, gray50CellStyle: CellStyle){
        val sheet = workbook.createSheet("Pago productores")
        val productRowStyle = workbook.createCellStyle()
        productRowStyle.wrapText = true

        var rowCount = 0

        val titleRow = sheet.createRow(rowCount)
        createStringCell(titleRow, 1, "PAGO PRODUCTORES", grayCellStyle, sheet)

        rowCount += 2

        val products = carts.map { cart -> cart.cartProducts }.flatten().groupBy { cp -> cp.product?.producer }
        products.forEach { producer, cartProducts ->
            val producerRow = sheet.createRow(rowCount)
            var totalPrice = BigDecimal.ZERO
            var totalQuantity = 0

            createStringCell(producerRow, 0, producer?.firstName?.trimMargin() ?: "No pudimos obtener el nombre del productor", noneCellStyle, sheet )
            createStringCell(producerRow, 1, "PRODUCTO", grayCellStyle, sheet)
            createStringCell(producerRow, 2, "CANTIDAD", gray50CellStyle, sheet)
            createStringCell(producerRow, 3, "PRECIO DE COMPRA", grayCellStyle, sheet)
            createStringCell(producerRow, 4, "MONTO", grayCellStyle, sheet)
            rowCount++

            cartProducts.groupBy { cp -> cp.product }.forEach { product, cartProducts ->
                val productColumn = sheet.createRow(rowCount)
                val quantity = cartProducts.sumBy { cp -> cp.quantity }
                val buyPrice = product?.buyPrice ?: BigDecimal.ZERO
                createStringCell(productColumn, 1, product?.title ?: "No pudimos obtener el nombre del producto", noneCellStyle, sheet)
                createNumericCell(productColumn, 2, quantity, noneCellStyle, sheet)
                createNumericCell(productColumn, 3, buyPrice, noneCellStyle, sheet)
                createNumericCell(productColumn, 4, buyPrice.multiply(quantity.toBigDecimal()), noneCellStyle, sheet)
                productColumn.rowStyle = productRowStyle
                productColumn.height = -1
                totalPrice = totalPrice.add(buyPrice.multiply(quantity.toBigDecimal()))
                totalQuantity += quantity
                rowCount++
            }

            val totalProducerBuyPrice = sheet.createRow(rowCount)
            createStringCell(totalProducerBuyPrice, 1, "TOTAL", gray50CellStyle, sheet, false)
            createNumericCell(totalProducerBuyPrice, 2, totalQuantity, noneCellStyle, sheet)
            createNumericCell(totalProducerBuyPrice, 4, totalPrice, noneCellStyle, sheet)

            rowCount += 2
        }

    }

    private fun createNodeSummarySheet(carts: List<Cart>, workbook: XSSFWorkbook, noneCellStyle: CellStyle, grayCellStyle: CellStyle, gray50CellStyle: CellStyle) {
        val nodeSummarySheet = workbook.createSheet("Resumen")
        val nodes = carts.groupBy{cartProduct -> cartProduct.nodeAvailable?.node}.map { it.key }.toList().sortedBy { node -> node?.id }
        val listOfProducts = mutableListOf<List<CartProduct>>()
        carts.forEach { listOfProducts.add(it.cartProducts.toList()) }
        val firstRowHeader = nodeSummarySheet.createRow(0)
        createStringCell(firstRowHeader, 0, "PRODUCTO", noneCellStyle, nodeSummarySheet)
        createStringCell(firstRowHeader, 1, "CANTIDAD", grayCellStyle, nodeSummarySheet)
        createStringCell(firstRowHeader, 2, "P. PRODUCTOR", grayCellStyle, nodeSummarySheet)
        createStringCell(firstRowHeader, 3, "P. TOTAL PRODUCTOR", grayCellStyle, nodeSummarySheet)
        nodes.forEachIndexed {
                index, node ->
            createStringCell(firstRowHeader, 2*index+4, "PRODUCTO", grayCellStyle, nodeSummarySheet)
            createStringCell(firstRowHeader, 2*index+5, node?.name ?: rowErrorMessage, grayCellStyle, nodeSummarySheet) }
        var index = 1
        var products = listOfProducts.flatten()
        products.groupBy(CartProduct::product).toList()
            .sortedBy { (k,v) -> k?.title }.toMap()
            .forEach{ product ->

            val productRow = nodeSummarySheet.createRow(index)
            val quantity: BigDecimal = product.value.sumBy(CartProduct::quantity).toBigDecimal()
            createStringCell(productRow, 0, product.key?.title ?: rowErrorMessage, noneCellStyle, nodeSummarySheet)
            createNumericCell(productRow, 1, quantity, grayCellStyle, nodeSummarySheet)
            createNumericCell(productRow,2, product.key?.buyPrice, noneCellStyle, nodeSummarySheet)
            createNumericCell(productRow, 3, product.key?.buyPrice?.times(quantity), noneCellStyle, nodeSummarySheet)
            nodes.forEachIndexed {
                    index, n ->
                createStringCell(productRow, 2*index+4, product.key?.title ?: rowErrorMessage, noneCellStyle, nodeSummarySheet)
                createNumericCell(productRow, 2*index+5, (product.value.filter{ p -> p.cart?.nodeAvailable?.node?.id == n?.id }.sumBy (CartProduct::quantity)), noneCellStyle, nodeSummarySheet)}
            index++
        }
        val itemsAmmountRow = nodeSummarySheet.createRow(index)
        createStringCell(itemsAmmountRow, 0, "CANT. BULTOS", grayCellStyle, nodeSummarySheet)
        createNumericCell(itemsAmmountRow, 1, products.sumBy(CartProduct::quantity), grayCellStyle, nodeSummarySheet)
        nodes.forEachIndexed { i, n ->
            createStringCell(itemsAmmountRow, 2*i+4, "CANT. BULTOS", grayCellStyle, nodeSummarySheet)
            createNumericCell(itemsAmmountRow, 2*i+5, products.filter{ p -> p.cart?.nodeAvailable?.node?.id == n?.id }.sumBy (CartProduct::quantity), noneCellStyle, nodeSummarySheet)}

        val totalRow = nodeSummarySheet.createRow(index+1)
        createStringCell(totalRow, 0, """TOTAL $""", grayCellStyle, nodeSummarySheet)

    
        createNumericCell(totalRow, 1, products.sumByDouble { cp -> cp.getTotalPrice()?.toDouble() ?: 0.0}, grayCellStyle, nodeSummarySheet)

       
        nodes.forEachIndexed { i, n ->
            createStringCell(totalRow, 2*i+4, """TOTAL $""", grayCellStyle, nodeSummarySheet)
            createNumericCell(totalRow, 2*i+5, products.filter{ p -> p.cart?.nodeAvailable?.node?.id == n?.id }.sumByDouble { p -> p.getTotalPrice()?.toDouble() ?: 0.0}, noneCellStyle, nodeSummarySheet)}

        val totalUsersRow = nodeSummarySheet.createRow(index+2)
        createStringCell(totalUsersRow, 0, "CANT. PERSONAS", grayCellStyle, nodeSummarySheet)
        createNumericCell(totalUsersRow, 1, products.groupBy { cp -> cp.cart?.user?.id }.count(), grayCellStyle, nodeSummarySheet)
        nodes.forEachIndexed { i, n ->
            createStringCell(totalUsersRow, 2*i+4, "CANT. PERSONAS", grayCellStyle, nodeSummarySheet)
            createNumericCell(totalUsersRow, 2*i+5, products.filter{ p -> p.cart?.nodeAvailable?.node?.id == n?.id }.groupBy { cp -> cp.cart?.user?.id }.count(), noneCellStyle, nodeSummarySheet)}
    }

    private fun createNodeReportSheet(cartsGroupedByNode: Map<Node?, List<Cart>>, workbook: XSSFWorkbook, noneCellStyle: CellStyle, grayCellStyle: CellStyle, gray50CellStyle: CellStyle) {
        val productRowStyle = workbook.createCellStyle()
        productRowStyle.wrapText = true

        cartsGroupedByNode.forEach { node, purchases ->
            val re = Regex("[^A-Za-z0-9 ]")
            val nodeName = re.replace(node?.name ?: "NO SE PUDO ENCONTRAR EL NOMBRE DEL NODO", "")
            val sheet = workbook.createSheet(nodeName)


            val nodeNameCell = sheet.createRow(0)
            createStringCell(nodeNameCell, 0, "Nodo: ${nodeName.toUpperCase()}".trimMargin(), noneCellStyle, sheet)
            createStringCell(nodeNameCell, 1, "Monto total: ${purchases.map { it.getTotalPrice() }.fold(BigDecimal.ZERO, BigDecimal::add)}; " +
                    "Bultos totales: ${purchases.flatMap { it.cartProducts }.sumBy { it.quantity }}", noneCellStyle, sheet)

            var rowCount = 2
            purchases.groupBy(Cart::user).toSortedMap(UserComparator()).forEach { userPurchase ->
                userPurchase.value.forEach { purchase ->
                    val nameAndPhone = sheet.createRow(rowCount)
                    var total = BigDecimal.ZERO
                    var totalElement = 0
                    nameAndPhone.height = -1
                    createStringCell(nameAndPhone, 0,""" Nombre: ${purchase.user?.firstName ?: ""} ${purchase.user?.lastName ?: ""} """.trimMargin(), noneCellStyle, sheet)
                    createStringCell(sheet.createRow(rowCount + 1),0, """ Telefono: ${purchase.user?.phone ?: ""} """.trimMargin(),noneCellStyle, sheet)
                    createStringCell(nameAndPhone, 1, "PRODUCTO", grayCellStyle, sheet)
                    createStringCell(nameAndPhone, 2, "CANTIDAD", grayCellStyle, sheet)
                    createStringCell(nameAndPhone, 3, "PRECIO UNITARIO", grayCellStyle, sheet, false)
                    createStringCell(nameAndPhone, 4, "MONTO", grayCellStyle, sheet)
                    rowCount++
                    purchase.cartProducts.forEach { cartProduct ->

                        val productColumn = sheet.getRow(rowCount) ?: sheet.createRow(rowCount)

                        createStringCell(productColumn,1,cartProduct.product?.title ?: "No pudimos obtener el nombre del producto",noneCellStyle,sheet, autosize = false)

                        createNumericCell(productColumn, 2, cartProduct.quantity, noneCellStyle, sheet)
                        totalElement += cartProduct.quantity
                        total = total.add(cartProduct.getTotalPrice())
                        createNumericCell(productColumn, 3, cartProduct.getPriceOfProduct(), noneCellStyle, sheet)
                        createNumericCell(productColumn, 4, cartProduct.getTotalPrice(), noneCellStyle, sheet)
                        productColumn.rowStyle = productRowStyle
                        productColumn.height = -1;
                        rowCount++
                    }
                    val totalProducerBuyPrice = sheet.createRow(rowCount)
                    createStringCell(totalProducerBuyPrice, 1, "TOTAL", gray50CellStyle, sheet, false)
                    createNumericCell(totalProducerBuyPrice, 2, totalElement, noneCellStyle, sheet)
                    createNumericCell(totalProducerBuyPrice, 4, total, noneCellStyle, sheet)


                    if(purchase.staffDiscount){
                        rowCount++
                        createStringCell(sheet.createRow(rowCount), 1, "Descuento equipo la justa", gray50CellStyle, sheet)
                    }

                    rowCount += 2
                }
            }
            setPrintFormat(sheet)
        }
    }

    private fun setPrintFormat(sheet: XSSFSheet) {
        sheet.printSetup.setPaperSize(PaperSize.A4_PAPER)
        sheet.printSetup.orientation = PrintOrientation.PORTRAIT
        sheet.printSetup.fitWidth = 1
        sheet.autoSizeColumn(0)
        sheet.setColumnWidth(0, 25 * 256)
        sheet.setColumnWidth(1, 36 * 256)
    }

    //modifico los strings hasta acá por si belén no quiere tocar esta tabla

    private fun createIncomesTable(sheet: Sheet, general: General, rowCount: RowCount, goldCellStyle: CellStyle, purpleCellStyle: CellStyle, noneCellStyle: CellStyle, totalCellStyle: CellStyle) {
        val purchases = purchaseProducerRepository.findByGeneralIdAndDeletedAtNull(general.id!!)

        sheet.addMergedRegion(CellRangeAddress(rowCount.count, 0, 0, 5))
        val header = sheet.createRow(rowCount.count)
        createStringCell(header, 0, "INGRESOS", goldCellStyle, sheet)
        createTableDescription(sheet, goldCellStyle)
        rowCount.count = 2
        val dateRow = sheet.createRow(rowCount.count)
        sheet.addMergedRegion(CellRangeAddress(rowCount.count, 2, 0, 8))
        createStringCell(dateRow, 0, simpleDateFormater.format(general.dateStartUpPage), purpleCellStyle, sheet)

        var totalQuantity = 0.0
        var totalProducerPrice = BigDecimal.ZERO
        var totalProductPrice = BigDecimal.ZERO
        var totalEarnings = BigDecimal.ZERO

        rowCount.count = 3
        purchases.sortedBy { purchase -> purchase.producer?.id }.forEach { purchase ->
            val purchaseRow = sheet.createRow(rowCount.count)
            val quantity = purchase.quantity
            totalQuantity += quantity
            val producerPrice = purchase.product?.buyPrice
            val productPrice = purchase.product?.price
            val earnings = productPrice?.minus(producerPrice!!)

            val totalProductPriceByProduct = productPrice?.multiply(quantity.toBigDecimal())
            val totalProducerExchangeByProduct = producerPrice?.multiply(quantity.toBigDecimal())
            val totalEarningsByProduct = earnings?.multiply(quantity.toBigDecimal())

            totalProducerPrice += totalProducerExchangeByProduct!!
            totalProductPrice += totalProductPriceByProduct!!
            totalEarnings += totalEarningsByProduct!!

            createStringCell(purchaseRow, 0, "${purchase.producer?.firstName ?: ""} ${purchase.producer?.lastName ?: ""}", noneCellStyle, sheet)
            createStringCell(purchaseRow, 1, purchase.product.title ?: "", noneCellStyle, sheet)
            createStringCell(purchaseRow, 2, "${quantity}", noneCellStyle, sheet)
            createStringCell(purchaseRow, 3, "\$${productPrice}", noneCellStyle, sheet)
            createStringCell(purchaseRow, 4, "\$${producerPrice}", noneCellStyle, sheet)
            createStringCell(purchaseRow, 5, "\$${earnings}", noneCellStyle, sheet)
            createStringCell(purchaseRow, 6, "\$${totalProducerExchangeByProduct}", noneCellStyle, sheet)
            createStringCell(purchaseRow, 7, "\$${totalProductPriceByProduct}", noneCellStyle, sheet)
            createStringCell(purchaseRow, 8, "\$${totalEarningsByProduct}", noneCellStyle, sheet)
            rowCount.count++
        }
        val totalRow = sheet.createRow(rowCount.count)
        createStringCell(totalRow, 0, "TOTAL PARCIAL: ${simpleDateFormater.format(general.dateStartUpPage)}", totalCellStyle, sheet)
        createStringCell(totalRow, 1, "", totalCellStyle, sheet)
        createStringCell(totalRow, 2, "${totalQuantity.toInt()}", totalCellStyle, sheet)
        createStringCell(totalRow, 3, "", totalCellStyle, sheet)
        createStringCell(totalRow, 4, "", totalCellStyle, sheet)
        createStringCell(totalRow, 5, "", totalCellStyle, sheet)
        createStringCell(totalRow, 6, "\$${totalProducerPrice}", totalCellStyle, sheet)
        createStringCell(totalRow, 7, "\$${totalProductPrice}", totalCellStyle, sheet)
        createStringCell(totalRow, 8, "\$${totalEarnings}", totalCellStyle, sheet)
        rowCount.count = rowCount.count.plus(3)
    }

    private fun createOutcomesTable(sheet: Sheet, general: General, rowCount: RowCount, goldCellStyle: CellStyle, noneCellStyle: CellStyle, totalCellStyle: CellStyle) {
        val expenses = expenseRepository.findByGeneralId(general.id!!)
        sheet.addMergedRegion(CellRangeAddress(rowCount.count, rowCount.count, 0, 5))
        val header = sheet.createRow(rowCount.count)
        createStringCell(header, 0, "EGRESOS", goldCellStyle, sheet)
        rowCount.count++
        createOutComesTableDescription(sheet, rowCount, goldCellStyle)
        expenses.forEach {
            val expenseRow = sheet.createRow(rowCount.count)
            createStringCell(expenseRow, 0, it.staff?.description ?: rowErrorMessage, noneCellStyle, sheet)
            createStringCell(expenseRow, 1, it.description ?: rowErrorMessage, noneCellStyle, sheet)
            createStringCell(expenseRow, 2, "${it.quantity}", noneCellStyle, sheet)
            createStringCell(expenseRow, 3, "${it.price}", noneCellStyle, sheet)
            createStringCell(expenseRow, 4, "${it.quantity?.toBigDecimal()?.multiply(it.price) ?: rowErrorMessage}", noneCellStyle, sheet)
            createStringCell(expenseRow, 5, simpleDateFormater.format(it.date), noneCellStyle, sheet)
            createStringCell(expenseRow, 6, simpleDateFormater.format(it.balanceDate), noneCellStyle, sheet)
            rowCount.count++
        }
        val total = sheet.createRow(rowCount.count)
        createStringCell(total, 0, "TOTAL", totalCellStyle, sheet)
        createStringCell(total, 4, "${expenses.sumByDouble { it.quantity?.toBigDecimal()?.multiply(it.price)?.toDouble() ?: 0.0}}", totalCellStyle, sheet)
        rowCount.count = rowCount.count.plus(3)
    }

    private fun createBalanceTable(sheet: Sheet, general: General?, rowCount: RowCount, goldCellStyle: CellStyle, greenCellStyle: CellStyle, redCellStyle: CellStyle, noneCellStyle: CellStyle) {
        val balances = if(general != null) balanceRepository.findHistoryAndActual(general.id!!) else balanceRepository.findAll()
        sheet.addMergedRegion(CellRangeAddress(rowCount.count, rowCount.count, 0, 5))
        val header = sheet.createRow(rowCount.count)
        createStringCell(header, 0, "BALANCE", goldCellStyle, sheet)
        rowCount.count++
        createBalanceTableDescription(sheet, rowCount, goldCellStyle)
        balances.forEach {
            val expenseRow = sheet.createRow(rowCount.count)
            createStringCell(expenseRow, 0, if(it.dateBalance == null) simpleDateFormater.format(it.updatedAt) else simpleDateFormater.format(it.dateBalance), noneCellStyle, sheet)
            createStringCell(expenseRow, 1, "\$${it.totalSale}", noneCellStyle, sheet)
            createStringCell(expenseRow, 2, "\$${it.totalExpenses?.add(it.totalPurchaseProducer)}", noneCellStyle, sheet)
            val total = it.totalSale?.minus(it.totalExpenses?.add(it.totalPurchaseProducer) ?: BigDecimal.ZERO)!!
            createStringCell(expenseRow, 3, "\$${total}", if(total.toInt() >= 0) greenCellStyle else redCellStyle, sheet)
            rowCount.count++
        }
        val expenseRow = sheet.createRow(rowCount.count)
        createStringCell(expenseRow, 0, "TOTAL", goldCellStyle, sheet)
        createStringCell(expenseRow, 1, "\$${balances.sumByDouble { it.totalSale?.toDouble() ?: 0.0}}", goldCellStyle, sheet)
        createStringCell(expenseRow, 2, "\$${balances.sumByDouble { it.totalExpenses?.toDouble() ?: 0.0}}", goldCellStyle, sheet)
        createStringCell(expenseRow, 3, "\$${balances.sumByDouble { it.totalSale?.minus(it.totalExpenses ?: BigDecimal.ZERO)?.toDouble() ?: 0.0}}", goldCellStyle, sheet)
        rowCount.count++
    }

    fun generatePurchaseReport(generalDTO: GeneralDTO): Workbook {
        generalDTO.id ?: throw MalformedObjectException("generalDto.id should not be null!!!")
        val general = generalRepository.findById(generalDTO.id)
        if (!general.isPresent) throw MalformedObjectException("No pudimos encontrar el General que estas buscando")

        val workbook: Workbook = XSSFWorkbook()
        val goldCellStyle = createCellStyle(workbook, IndexedColors.GOLD, BorderStyle.THICK)
        goldCellStyle.setFont(workbook.createFont().also { it.bold = true; it.fontHeightInPoints = 10 })
        val purpleCellStyle = createCellStyle(workbook, IndexedColors.PINK, BorderStyle.THICK)
        val greenCellStyle = createCellStyle(workbook, IndexedColors.BRIGHT_GREEN, BorderStyle.THICK)
        val redCellStyle = createCellStyle(workbook, IndexedColors.RED, BorderStyle.THICK)
        val noneCellStyle = createCellStyle(workbook, IndexedColors.WHITE, BorderStyle.THIN)
        val totalCellStyle = createCellStyle(workbook, IndexedColors.GREY_50_PERCENT, BorderStyle.MEDIUM)

        val sheet = workbook.createSheet("BALANCE")
        val rowCount = RowCount(0)
        createIncomesTable(sheet, general.get(), rowCount, goldCellStyle, purpleCellStyle, noneCellStyle, totalCellStyle)
        createOutcomesTable(sheet, general.get(), rowCount, goldCellStyle, noneCellStyle, totalCellStyle)
        createBalanceTable(sheet, general.get(), rowCount, goldCellStyle, greenCellStyle, redCellStyle, noneCellStyle)
        return workbook
    }

    fun generatePurchaseReport(): Workbook {
        val workbook: Workbook = XSSFWorkbook()
        val goldCellStyle = createCellStyle(workbook, IndexedColors.GOLD, BorderStyle.THICK)
        goldCellStyle.setFont(workbook.createFont().also { it.bold = true; it.fontHeightInPoints = 10 })
        val greenCellStyle = createCellStyle(workbook, IndexedColors.BRIGHT_GREEN, BorderStyle.THICK)
        val redCellStyle = createCellStyle(workbook, IndexedColors.RED, BorderStyle.THICK)
        val noneCellStyle = createCellStyle(workbook, IndexedColors.WHITE, BorderStyle.THIN)

        val sheet = workbook.createSheet("BALANCE")
        val rowCount = RowCount(0)
        createBalanceTable(sheet, null, rowCount, goldCellStyle, greenCellStyle, redCellStyle, noneCellStyle)
        return workbook
    }

    fun createTableDescription(sheet: Sheet, style: CellStyle) {
        val descriptionTable = sheet.createRow(1)
        createStringCell(descriptionTable, 0, "Productor/inter", style, sheet)
        createStringCell(descriptionTable, 1, "Producto", style, sheet)
        createStringCell(descriptionTable, 2, "Cantidad", style, sheet)
        createStringCell(descriptionTable, 3, "Precio venta", style, sheet)
        createStringCell(descriptionTable, 4, "Precio Productor", style, sheet)
        createStringCell(descriptionTable, 5, "Extra nosotrxs", style, sheet)
        createStringCell(descriptionTable, 6, "INGRESO Productor", style, sheet)
        createStringCell(descriptionTable, 7, "INGRESO ntro", style, sheet)
        createStringCell(descriptionTable, 8, "Ingreso total", style, sheet)
    }

    fun createOutComesTableDescription(sheet: Sheet, rowCount: RowCount, style: CellStyle) {
        val descriptionTable = sheet.createRow(rowCount.count)
        createStringCell(descriptionTable, 0, "NOMBRE", style, sheet)
        createStringCell(descriptionTable, 1, "PRODUCTO O SERVICIO", style, sheet)
        createStringCell(descriptionTable, 2, "CANT", style, sheet)
        createStringCell(descriptionTable, 3, "VALOR \$", style, sheet)
        createStringCell(descriptionTable, 4, "TOTAL", style, sheet)
        createStringCell(descriptionTable, 5, "FECHA", style, sheet)
        createStringCell(descriptionTable, 6, "FECHA SALDADO", style, sheet)
        rowCount.count++
    }

    fun createBalanceTableDescription(sheet: Sheet, rowCount: RowCount, style: CellStyle) {
        val descriptionTable = sheet.createRow(rowCount.count)
        createStringCell(descriptionTable, 0, "Fecha", style, sheet)
        createStringCell(descriptionTable, 1, "Ingresos comercializadora", style, sheet)
        createStringCell(descriptionTable, 2, "Egresos", style, sheet)
        createStringCell(descriptionTable, 3, "Balance  \$", style, sheet)
        rowCount.count++
    }

    fun createStringCell(row: Row, position: Int, value: String, style: CellStyle, sheet: Sheet, autosize: Boolean = true): Cell {
        val cell = row.createCell(position)
        cell.setCellStyle(style)
        cell.setCellValue(value)
        if(autosize) {
            sheet.autoSizeColumn(cell.columnIndex)
        } else {
            cell.cellStyle.wrapText = true
        }
        return cell
    }

    fun createNumericCell(row: Row, position: Int, value: Number?, style: CellStyle, sheet: Sheet): Cell {
        val cell = row.createCell(position)
        cell.setCellStyle(style)
        value?.let { cell.setCellValue(it.toDouble()) }

        //sheet.autoSizeColumn(cell.columnIndex)
        return cell
    }

    fun createCellStyle(workbook: Workbook, index: IndexedColors, thick: BorderStyle = BorderStyle.NONE, alignment: HorizontalAlignment = HorizontalAlignment.CENTER, fillPatter: FillPatternType = FillPatternType.BRICKS): CellStyle {
        val cellStyle = workbook.createCellStyle()
        cellStyle.fillForegroundColor = index.index
        cellStyle.setBorderBottom(thick)
        cellStyle.setBorderLeft(thick)
        cellStyle.setBorderRight(thick)
        cellStyle.setBorderTop(thick)
        cellStyle.setAlignment(alignment)
        cellStyle.setFillPattern(fillPatter)
        return cellStyle
    }
}

class UserComparator():Comparator<User?> {
    override fun compare(o1: User?, o2: User?): Int {
        return "${o1?.firstName} ${o1?.lastName}".toUpperCase().compareTo("${o2?.firstName} ${o2?.lastName}".toUpperCase())
    }
}

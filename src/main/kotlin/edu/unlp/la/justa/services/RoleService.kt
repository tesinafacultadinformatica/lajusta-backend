package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.RoleDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Role
import edu.unlp.la.justa.repositories.RoleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class RoleService(@Autowired val roleRepository: RoleRepository) {

    fun getAll(): PageableCollectionDTO {
        val roles = roleRepository.findAll().map { role -> RoleDTO.createRoleDTO(role) }.toSet()
        val totalElements = roleRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = roles)
    }

    fun save(roleDto: RoleDTO): RoleDTO {
        roleDto.id?.let { throw MalformedObjectException("role.id should be null, if not, please use update instead") }
        val newRole = roleRepository.save(Role.createRole(roleDto))
        return RoleDTO.createRoleDTO(newRole)
    }

    fun activate(id: Long): RoleDTO {
        val role = roleRepository.findById(id)
        if(!role.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        role.get().deletedAt = null
        role.get().updatedAt = Date()
        roleRepository.save(role.get())
        return RoleDTO.createRoleDTO(role.get())
    }

    fun get(id: Long) = roleRepository.findById(id).get()

    fun update(roleDto: RoleDTO): RoleDTO {
        roleDto.id ?: throw MalformedObjectException("role.id should not be null, if not, please use create instead")
        val updatedRole = roleRepository.save(Role.createRole(roleDto))
        return RoleDTO.createRoleDTO(updatedRole)
    }

    fun delete(roleDto: RoleDTO) {
        roleRepository.save(Role.createDeletedRol(roleDto))
    }

//    fun getAllActiveRoles() = roleRepository.findAllByDeleteAtNotNull()
}
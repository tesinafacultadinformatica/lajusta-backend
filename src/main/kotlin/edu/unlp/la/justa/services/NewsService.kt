package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.NewsDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.News
import edu.unlp.la.justa.repositories.NewsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.text.SimpleDateFormat
import java.util.*

@Service
class NewsService(@Autowired val newsRepository: NewsRepository,
                  @Autowired val imageService: ImageService) {

    val formatDate: SimpleDateFormat = SimpleDateFormat("dd_MM_YYYY_hh_mm_ss")

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?,properties:Set<PropertiesFilterDTO>?): PageableCollectionDTO{
        val newss = newsRepository.findAll(filterStr, rangeStd, sortStr, properties).mapNotNull { news -> NewsDTO.createNewsDTO(news) }.toSet()
        val totalElements = newsRepository.count()
        return PageableCollectionDTO(totalElements = totalElements, page = newss)
    }

    fun get(id: Long): NewsDTO? {
        val news = newsRepository.findById(id).get()
        return NewsDTO.createNewsDTO(news, news.image?.route)
    }

    fun save(newsDTO: NewsDTO): NewsDTO? {
        newsDTO.id?.let { throw MalformedObjectException("newsDTO.id should be null, please use Update instead") }
        val newImage = imageService.save(formatDate.format(Date()), newsDTO.image)
        val newNews = News.createNews(newsDTO, newImage)
        newNews ?: throw MalformedObjectException("News must not be null or empty!")
        val savedNews = newsRepository.save(newNews)
        return NewsDTO.createNewsDTO(savedNews)
    }

    fun activate(id: Long): NewsDTO {
        val news = newsRepository.findById(id)
        if(!news.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        news.get().deletedAt = null
        news.get().updatedAt = Date()
        newsRepository.save(news.get())
        return NewsDTO.createNewsDTO(news.get())
    }

    fun update(newsDTO: NewsDTO): NewsDTO? {
        newsDTO.id ?: throw MalformedObjectException("newsDTO.id should not be null, please use Save instead")
        val updatednews = News.createNews(newsDTO)
        updatednews ?: throw MalformedObjectException("News must not be null or empty!")
        updatednews.updatedAt = Date()
        return NewsDTO.createNewsDTO(newsRepository.save(updatednews))
    }

    fun delete(id: Long): NewsDTO? {
        val deletedNews = newsRepository.findById(id).get()
        deletedNews.updatedAt = Date()
        deletedNews.deletedAt = Date()
        return NewsDTO.createNewsDTO(newsRepository.save(deletedNews))
    }
}
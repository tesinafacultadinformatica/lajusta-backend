package edu.unlp.la.justa.services

import edu.unlp.la.justa.controllers.PropertiesFilterDTO
import edu.unlp.la.justa.dtos.ImageDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.dtos.ProductDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Image
import edu.unlp.la.justa.models.Product
import edu.unlp.la.justa.models.ProductCataloge
import edu.unlp.la.justa.repositories.ProductCatalogueRepository
import edu.unlp.la.justa.repositories.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import javax.transaction.Transactional

@Service
class ProductService(@Autowired val productRepository: ProductRepository,
                     @Autowired val productCatalogeRepository: ProductCatalogueRepository,
                     @Autowired val imageService: ImageService) {

    fun getAll(filterStr: String?, rangeStd:String?, sortStr: String?, properties: Set<PropertiesFilterDTO>?): Any {
        val products: Any = if(filterStr == null && rangeStd == null && sortStr == null) {
            productRepository.findAll().map { product -> ProductDTO.createProductDTO(product).also { it.images = product.images?.filter { image -> image.isMain ?: false }?.mapNotNull { image -> ImageDTO.createImageDTO(image, image.route) }?.toSet()} }.toSet()
        } else {
            val totalElements = productRepository.count()
            PageableCollectionDTO(totalElements = totalElements, page = productRepository.findAll(filterStr, rangeStd, sortStr, properties).map { product -> ProductDTO.createProductDTO(product).also { it.images = product.images?.filter { image -> image.isMain ?: false }?.mapNotNull { image -> ImageDTO.createImageDTO(image, image.route) }?.toSet()} }.toSet())
        }
        return  products
    }

    fun get(id: Long): ProductDTO {
        val producto = productRepository.findById(id).get()
        val productDto = ProductDTO.createProductDTO(producto)
        productDto.images = producto.images?.mapNotNull { ImageDTO.createImageDTO(it, it.route) }?.toSet()
        return productDto
    }

    @Transactional
    fun save(productDTO: ProductDTO): ProductDTO {
        productDTO.id?.let { throw MalformedObjectException("productDTO.id should be null, please use Update instead") }
        val newProduct = Product.createProduct(productDTO)
        productDTO.images?.let {
            val images = mutableSetOf<Image>()
            it.forEach({ image -> imageService.save(newProduct.title, image)?.let { savedImage -> images.add(savedImage) } })
            newProduct.images = images
        }
        newProduct.createdAt = Date()
        val product = productRepository.save(newProduct)
        productCatalogeRepository.save(ProductCataloge(id = null, producer = product.producer, product = product, price = BigDecimal.ZERO, quantity = 0, createdAt = Date(), updatedAt = Date()))
        return ProductDTO.createProductDTO(product)
    }

    fun activate(id: Long): ProductDTO {
        val product = productRepository.findById(id)
        if(!product.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        product.get().deletedAt = null
        product.get().updatedAt = Date()
        productRepository.save(product.get())
        return ProductDTO.createProductDTO(product.get())
    }

    @Transactional
    fun update(productDTO: ProductDTO): ProductDTO {
        productDTO.id ?: throw MalformedObjectException("producerDTO.id should be null, please use Update instead")
        val updatedProduct = productRepository.save(Product.createProduct(productDTO))
        productDTO.images?.let {
            val images = mutableSetOf<Image>()
            it.forEach { image -> if (image.id == null) imageService.save(updatedProduct.title, image)?.let { savedImage -> images.add(savedImage) }
                else imageService.update(image).let { savedImage -> images.add(savedImage) }}
            updatedProduct.images = images
        }
        updatedProduct.updatedAt = Date()
        return ProductDTO.createProductDTO(updatedProduct)
    }

    fun delete(id: Long): ProductDTO {
        val deletedProduct = productRepository.findById(id).get()
        deletedProduct.deletedAt = Date()
        return ProductDTO.createProductDTO(productRepository.save(deletedProduct))
    }
}
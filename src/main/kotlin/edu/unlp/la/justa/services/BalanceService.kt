package edu.unlp.la.justa.services

import edu.unlp.la.justa.dtos.BalanceDTO
import edu.unlp.la.justa.dtos.GeneralDTO
import edu.unlp.la.justa.dtos.PageableCollectionDTO
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Balance
import edu.unlp.la.justa.repositories.BalanceRepository
import edu.unlp.la.justa.repositories.CartRepository
import edu.unlp.la.justa.repositories.ExpenseRepository
import edu.unlp.la.justa.repositories.GeneralRepository
import edu.unlp.la.justa.repositories.PurchaseProducerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*
import javax.transaction.Transactional

@Service
class BalanceService(@Autowired val balanceRepository: BalanceRepository,
                     @Autowired val cartRepository: CartRepository,
                     @Autowired val purchaseProducerRepository: PurchaseProducerRepository,
                     @Autowired val generalRepository: GeneralRepository,
                     @Autowired val expenseRepository: ExpenseRepository) {

    fun getAll(filterStr: String?, rangeStd: String?, sortStr: String?): Any {
        val activeGeneral = generalRepository.findActive(Date())
        val balances: Any = if (filterStr == null && rangeStd == null && sortStr == null) {
            balanceRepository.findHistoryAndActual(activeGeneral?.id!!).map { BalanceDTO.createBalanceDTO(it) }.toSet()
        } else {
            val totalElements = balanceRepository.count()
            PageableCollectionDTO(totalElements = totalElements, page = balanceRepository.findAll(filterStr, rangeStd, sortStr, activeGeneral?.id).mapNotNull { BalanceDTO.createBalanceDTO(it) }.toSet())
        }
        return balances
    }

    fun getAll() = balanceRepository.findAll().map { balance -> balance }.toSet()

    private fun getBalanceTotaled(id: Long): Balance {
        val balance = balanceRepository.findById(id)
        if (!balance.isPresent) throw NotFoundException("We could not find the Id you are looking for")
        val getBalance = balance.get()
        val totalCarts = BigDecimal.ZERO
        val carts = cartRepository.findByGeneralIdAndDeletedAtNotNull(getBalance.general.id!!)
        carts.forEach { cart ->
            totalCarts.plus(cart.total ?: BigDecimal.ZERO)
        }
        getBalance.totalSale = totalCarts

        val totalExpenses = BigDecimal.ZERO
        val expenses = expenseRepository.findByGeneralIdAndDeletedAtNotNull(getBalance.general.id)
        expenses.forEach { expense ->
            totalExpenses.plus(expense.total ?: BigDecimal.ZERO)
        }
        getBalance.totalExpenses = totalExpenses

        val totalPurchases = BigDecimal.ZERO
        val purchaseProducers = purchaseProducerRepository.findByGeneralIdAndDeletedAtNotNull(getBalance.general.id)
        purchaseProducers.forEach { purchaseProducer ->
            totalPurchases.plus(purchaseProducer.total)
        }
        getBalance.totalPurchaseProducer = totalPurchases

        return getBalance
    }

    fun get(id: Long): BalanceDTO? {
        return BalanceDTO.createBalanceDTO(getBalanceTotaled(id))
    }

    @Transactional
    fun close(generalDto: GeneralDTO): BalanceDTO {
        generalDto.id ?: throw MalformedObjectException("General.id should not be null")
        val general = generalRepository.findById(generalDto.id)
        if (!general.isPresent) throw NotFoundException("General could not be found")
        general.get().closedAt = Date()
        generalRepository.save(general.get())

        val getBalance = getBalanceTotaled(general.get().balance?.id!!)
        getBalance.dateBalance = Date()
        getBalance.updatedAt = Date()
        balanceRepository.save(getBalance)
        return BalanceDTO.createBalanceDTO(getBalance)!!
    }

}
package edu.unlp.la.justa.services.email

import com.google.common.hash.Hashing
import edu.unlp.la.justa.dtos.ResetPasswordDTO
import edu.unlp.la.justa.dtos.ResetPasswordMessage
import edu.unlp.la.justa.exceptions.MalformedObjectException
import edu.unlp.la.justa.exceptions.NotFoundException
import edu.unlp.la.justa.models.Cart
import edu.unlp.la.justa.models.ResetPassword
import edu.unlp.la.justa.repositories.ProductRepository
import edu.unlp.la.justa.repositories.ResetPasswordRepository
import edu.unlp.la.justa.repositories.UserRepository
import edu.unlp.la.justa.security.EncoderFactory
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.nio.charset.StandardCharsets
import java.text.MessageFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.mail.Message
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

@Component
@Profile("prod")
class EmailServiceAWS(@Autowired val resetPasswordRepository: ResetPasswordRepository,
                      @Autowired val userRepository: UserRepository,
                      @Autowired val productRepository: ProductRepository,
                      @Autowired val encoder: EncoderFactory,
                      @Value("\${aws.mail.smtp.body}") val emailBody: String? = null,
                      @Value("\${aws.mail.smtp.purchace}") val emailPurchaseBody: String? = null,
                      @Value("\${aws.mail.smtp.url}") val host: String,
                      @Value("\${aws.mail.smtp.port}") val port: Int,
                      @Value("\${CROSS_ORIGIN}") val serverUrl: String,
                      @Value("\${CROSS_ORIGIN_WWW}") val serverWwwUrl: String,
                      @Value("\${aws.from.email.address}") val fromEmailAddress: String,
                      @Value("\${aws.mail.creadentials.user}") val AWSUser: String,
                      @Value("\${logo.url}") val logoUrl: String,
                      @Value("\${aws.mail.creadentials.password}") val AWSPassword: String) : EmailService {

    var props: Properties = System.getProperties()
    val LOG = Logger.getLogger(EmailServiceAWS::class.java)
    val dateFomatter = SimpleDateFormat("dd/MM/yyyy")
    val hoursFormatter = SimpleDateFormat("HH:mm")


    init {
        props["mail.transport.protocol"] = "smtp"
        props["mail.smtp.port"] = port
        props["mail.smtp.starttls.enable"] = "true"
        props["mail.smtp.auth"] = "true"

    }

    override fun sendRecoveryPasswordEmail(emailAddress: String): Any {
        val sha256hex = Hashing.sha256().hashString(Random().nextInt().toString(), StandardCharsets.UTF_8).toString()
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE, 30)
        val lastEmailCode = resetPasswordRepository.findById(emailAddress)
        if (lastEmailCode.isPresent) {
            lastEmailCode.get().resetCode = sha256hex
            lastEmailCode.get().timeToLive = calendar.time
            resetPasswordRepository.save(lastEmailCode.get())
        } else {
            val resetPasswordCode = ResetPassword(emailAddress, sha256hex, calendar.time)
            resetPasswordRepository.save(resetPasswordCode)
        }
        sendResetPasswordEmail(emailAddress, sha256hex)
        return ResetPasswordMessage("The confirmation code has been sent!")
    }

    override fun sendConfirmationEmail(resetPassword: ResetPasswordDTO): Any {
        val user = userRepository.findByEmail(resetPassword.email)
        val dataBaseResetCode = resetPasswordRepository.findById(resetPassword.email)
        val responseMessage: ResetPasswordMessage
        if (dataBaseResetCode.isPresent && dataBaseResetCode.get().resetCode == resetPassword.code) {
            user.encryptedPassword = encoder.encode(resetPassword.newPassword)
            userRepository.save(user)
            resetPasswordRepository.delete(dataBaseResetCode.get())
            responseMessage = ResetPasswordMessage("Password has been restored!")
        } else {
            throw MalformedObjectException("Invalid email or confirmation code, please try again")
        }
        return responseMessage
    }

    override fun sendNewPurchaseEmail(cart: Cart) {
        LOG.debug("sendNewPurchaseEmail start, sending email to ${cart.user?.email}")
        val user = userRepository.findById(cart.user?.id!!)
        if (!user.isPresent) throw NotFoundException("We could not find the user with ID ${user.get().id}")
        val productDetail = StringBuffer("""<table style="border-collapse: collapse; width: 100%;" border="1">
                                            <tbody>
                                            <tr>
                                            <td style="width: 48.8637%; text-align: center; background: #4CBE81FF;"><strong><span style="color: #ffffff;">Producto</span></strong></td>
                                            <td style="width: 22.301%; text-align: center; background: #4CBE81FF;"><strong><span style="color: #ffffff;">Cantidad</span></strong></td>
                                            <td style="width: 28.8353%; text-align: center; background: #4CBE81FF; font-color: #FFF;"><strong><span style="color: #ffffff;">Precio</span></strong></td>
                                            </tr>""")
        var total = BigDecimal.ZERO
        var totalQuantity = 0
        cart.cartProducts.forEachIndexed { index, cartProduct ->
            val product = productRepository.findById(cartProduct.product?.id!!)
            val price = cartProduct.getTotalPrice()
            totalQuantity += cartProduct.quantity
            total = total.add(price ?: BigDecimal.ZERO)
            productDetail.append("""<tr>
                                    <td style="width: 48.8637%;text-align: center; background: ${if ((index % 2) == 0) "#BAE6CEFF" else "#F3D49FFF"};"><span style="color: #646464FF;">${product.get().title}</span></td>
                                    <td style="width: 22.301%;text-align: center; background: ${if ((index % 2) == 0) "#BAE6CEFF" else "#F3D49FFF"};"><span style="color: #646464FF;">${cartProduct.quantity}</span></td>
                                    <td style="width: 28.8353%;text-align: center; background: ${if ((index % 2) == 0) "#BAE6CEFF" else "#F3D49FFF"};"><span style="color: #646464FF;">${price}</span></td>
                                    </tr>""")
        }
        productDetail.append("""<tr>
                                <td style="width: 48.8637%;text-align: center; background: #D8FFECFF;"><span style="color: #646464FF;">TOTAL</span></td>
                                <td style="width: 22.301%;text-align: center; background: #D8FFECFF;"><span style="color: #646464FF;">${totalQuantity}</span></td>
                                <td style="width: 28.8353%;text-align: center; background: #D8FFECFF;"><span style="color: #646464FF;">${total}</span></td>
                                </tr>""")
        productDetail.append("""</tbody>
                                </table>""")

        this.sendEmail(user.get().email!!, MessageFormat.format(emailPurchaseBody!!,
                "${user.get().firstName} ${user.get().lastName}",
                dateFomatter.format(cart.createdAt),
                productDetail.toString(),
                dateFomatter.format(cart.nodeAvailable?.dateOpen),
                cart.nodeAvailable?.node?.name ?: "",
                logoUrl,
                hoursFormatter.format(cart.nodeAvailable?.dateOpen) ?: "",
                hoursFormatter.format(cart.nodeAvailable?.dateClose) ?: "",
                "${cart.nodeAvailable?.node?.address?.street} ${cart.nodeAvailable?.node?.address?.number} ${cart.nodeAvailable?.node?.address?.betweenStreets}"),
                "Compra realizada con exito")
    }

    private fun sendResetPasswordEmail(emailAddress: String, code: String) {
        LOG.debug("sendResetPasswordEmail start, sending email to ${emailAddress}")
        this.sendEmail(emailAddress, emailBody?.replace("<DIRECCION>", "${serverUrl}/shop/home?emailConfirmationCode=${code}")!!, "Codigo de Verificacion")
    }

    private fun sendEmail(emailAddress: String, body: String, title: String) {
        val session = Session.getDefaultInstance(props)
        val message = MimeMessage(session);
        message.setFrom(InternetAddress(fromEmailAddress, "La Justa"))
        message.setRecipient(Message.RecipientType.TO, InternetAddress(emailAddress))
        message.setSubject(title)
        message.setContent(body, "text/html")
        val transport = session.getTransport()
        try {
            transport.connect(host, AWSUser, AWSPassword)
            transport.sendMessage(message, message.getAllRecipients())
            LOG.debug("Email sent to ${emailAddress}")
        } catch (exception: Exception) {
            LOG.debug(exception)
        } finally {
            transport.close();
        }
    }
}

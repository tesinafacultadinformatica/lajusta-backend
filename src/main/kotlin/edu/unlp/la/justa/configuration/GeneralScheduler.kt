package edu.unlp.la.justa.configuration

import edu.unlp.la.justa.repositories.BalanceRepository
import edu.unlp.la.justa.repositories.GeneralRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import java.util.*
import javax.transaction.Transactional

@Configuration
@EnableScheduling
class GeneralScheduler(
    @Autowired val generalRepository: GeneralRepository,
    @Autowired val balanceRepository: BalanceRepository
) {

    @Scheduled(fixedDelay = 43200000)
    @Transactional
    fun closeOpenBalances() {
        val generals = generalRepository.findClosableBalances()
        generals.forEach { general ->
            general.balance?.let {
                it.dateBalance = Date()
                it.updatedAt = Date()
                balanceRepository.save(it)
            }
        }
    }
}
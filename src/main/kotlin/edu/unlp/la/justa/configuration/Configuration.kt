package edu.unlp.la.justa.configuration

import edu.unlp.la.justa.security.EncoderFactory
import edu.unlp.la.justa.security.JwtAuthenticationEntryPoint
import edu.unlp.la.justa.security.JwtAuthenticationFilter
import edu.unlp.la.justa.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class Configuration(val userService: UserService, val unauthorizedHandler: JwtAuthenticationEntryPoint) : WebSecurityConfigurerAdapter() {

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

    @Autowired
    fun globalUserDetails(auth: AuthenticationManagerBuilder) = auth.userDetailsService(userService).passwordEncoder(encoder)

    @Autowired
    val jwtAuthenticationEntryPoint: JwtAuthenticationFilter? = null

    @Autowired
    val encoder: EncoderFactory? = null

    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/banner" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/cart" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/category" ).permitAll()
                .antMatchers(HttpMethod.PUT,"/api/category" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/general/**" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/reportes/**" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/news" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/node" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/producer/**" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/product/**" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/generator/pdf/purchase/**" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/unit" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/user" ).permitAll()
                .antMatchers(HttpMethod.GET,"/api/email/**" ).permitAll()
                .antMatchers(HttpMethod.POST,"/api/email/**" ).permitAll()
                .antMatchers(HttpMethod.GET,"/static/**" ).permitAll()
                .antMatchers("/api/token/*",
                        "/api/user/signup",
                        "/api/user/staff",
                        "/swagger-ui.html",
                        "/swagger-ui/**",
                        "/null/**",
                        "/swagger-resources/**",
                        "/favicon.ico",
                        "/csrf/*",
                        "/*",
                        "/v2/**",
                        "/webjars/**").permitAll()
//                .antMatchers("/api/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http
                .addFilterBefore(jwtAuthenticationEntryPoint, UsernamePasswordAuthenticationFilter::class.java)
    }
}

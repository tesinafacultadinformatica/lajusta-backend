package edu.unlp.la.justa.configuration

import edu.unlp.la.justa.repositories.ResetPasswordRepository
import edu.unlp.la.justa.repositories.UserTokenRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import java.util.*
import javax.transaction.Transactional

@Configuration
@EnableScheduling
class JWTDeletor(@Autowired val userTokenRepository: UserTokenRepository,
                 @Autowired val resetPasswordRepository: ResetPasswordRepository) {

    @Scheduled(fixedDelay = 30000)
    @Transactional
    fun deleteAllJWTTokens(){
        userTokenRepository.deleteAllByTimeToLiveBefore(Date())
    }

    @Scheduled(fixedDelay = 30000)
    @Transactional
    fun deleteAllResetPassword(){
        resetPasswordRepository.deleteAllByTimeToLiveBefore(Date())
    }
}
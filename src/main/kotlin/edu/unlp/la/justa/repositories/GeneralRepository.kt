package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.GeneralCustomRepository
import edu.unlp.la.justa.models.General
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service
import java.util.*

@Service
interface GeneralRepository : CrudRepository<General, Long>, GeneralCustomRepository {

    @Query("""from General g where g.deletedAt is null and :today between g.dateStartUpPage and g.dateDownPage""")
    fun findActive(@Param("today") today: Date?): General?

    @Query("""from General g where g.dateStartUpPage = (select MIN(gen.dateStartUpPage) from General gen where gen.deletedAt is null and  gen.dateStartUpPage >= :today)""")
    fun findNextActive(@Param("today") today: Date?): General?

    @Query("""select count(g) from General g 
        where g.id <> :id
        and (:start between g.dateStartUpPage and g.dateDownPage 
        or :end between g.dateStartUpPage and g.dateDownPage)
        and g.deletedAt is null""")
    fun findGeneralOverlaying(@Param("id") id: Long?, @Param("start") start: Date?, @Param("end") end: Date?): Int

    @Query(
        """from General g join AvailableNode an on g.id = an.general.id
              where CURRENT_DATE > an.dateClose
              and g.balance.dateBalance is null
              group by an.general.id
              having count(g.id) = (select count(an_sub.id) from AvailableNode an_sub where an_sub.general.id = g.id)"""
    )
    fun findClosableBalances(): List<General>
}
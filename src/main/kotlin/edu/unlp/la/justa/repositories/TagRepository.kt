package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.TagCustomRepository
import edu.unlp.la.justa.models.Tag
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface TagRepository : CrudRepository<Tag, Long>, TagCustomRepository {
}
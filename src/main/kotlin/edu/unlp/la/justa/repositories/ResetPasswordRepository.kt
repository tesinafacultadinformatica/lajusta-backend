package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.models.ResetPassword
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
interface ResetPasswordRepository : CrudRepository<ResetPassword, String> {
    fun deleteAllByTimeToLiveBefore(date: Date)
}
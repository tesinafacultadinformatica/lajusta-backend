package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.ExpenseCustomRepository
import edu.unlp.la.justa.models.Expense
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface ExpenseRepository : CrudRepository<Expense, Long>, ExpenseCustomRepository {
    fun findByGeneralIdAndDeletedAtNotNull(generalId: Long): List<Expense>

    fun findByGeneralId(generalId: Long): List<Expense>
}
package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.BannerCustomRepository
import edu.unlp.la.justa.models.Banner
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service

@Service
interface BannerRepository : CrudRepository<Banner, Long>, BannerCustomRepository {

    @Query("""UPDATE Banner SET deletedAt = null WHERE id = :id""")
    fun activate(@Param("id") id: Long)

    fun findAllByDeletedAtIsNull(): Set<Banner>
}
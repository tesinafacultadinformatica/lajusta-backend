package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.PurchaseProducerCustomRepository
import edu.unlp.la.justa.models.PurchaseProducer
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface PurchaseProducerRepository : CrudRepository<PurchaseProducer, Long>, PurchaseProducerCustomRepository {
    fun findByGeneralIdAndDeletedAtNotNull(generalId: Long): List<PurchaseProducer>

    fun findByGeneralIdAndDeletedAtNull(generalId: Long): List<PurchaseProducer>

    fun findByProductIdAndGeneralId(productId: Long, generalId: Long): PurchaseProducer?
}


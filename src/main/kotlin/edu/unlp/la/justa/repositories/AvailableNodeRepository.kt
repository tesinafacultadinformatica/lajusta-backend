package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.models.AvailableNode
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface AvailableNodeRepository : CrudRepository<AvailableNode, Long> {
}
package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.StaffCustomRepository
import edu.unlp.la.justa.models.Staff
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface StaffRepository : CrudRepository<Staff, Long>, StaffCustomRepository {
}
package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.models.UserToken
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
interface UserTokenRepository : CrudRepository<UserToken, Long> {
    fun findByToken(token: String): UserToken

    fun findByUsername(username: String): UserToken?

    fun deleteAllByTimeToLiveBefore(date: Date)
}
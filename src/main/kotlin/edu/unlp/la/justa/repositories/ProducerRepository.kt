package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.ProducerCustomRepository
import edu.unlp.la.justa.models.Producer
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface ProducerRepository : CrudRepository<Producer, Long>, ProducerCustomRepository {
}
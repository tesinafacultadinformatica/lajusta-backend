package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.CategoryCustomRepository
import edu.unlp.la.justa.models.Category
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface CategoryRepository : CrudRepository<Category, Long>, CategoryCustomRepository {
}
package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.ProductCustomRepository
import edu.unlp.la.justa.models.Product
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface ProductRepository : CrudRepository<Product, Long>, ProductCustomRepository {
}
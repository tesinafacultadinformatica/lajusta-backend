package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.models.ProductCataloge
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface ProductCatalogueRepository : CrudRepository<ProductCataloge, Long> {
    fun findByProductId(productId: Long): List<ProductCataloge>
}


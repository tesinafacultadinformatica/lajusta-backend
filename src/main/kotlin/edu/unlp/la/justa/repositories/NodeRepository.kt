package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.NodeCustomRepository
import edu.unlp.la.justa.models.Node
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface NodeRepository : CrudRepository<Node, Long>, NodeCustomRepository {
}
package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.NewsCustomRepository
import edu.unlp.la.justa.models.News
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service

@Service
interface NewsRepository : CrudRepository<News, Long>, NewsCustomRepository {

    @Query("""UPDATE News SET deletedAt = null WHERE id = :id""")
    fun activate(@Param("id") id: Long)
}
package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.CartCustomRepository
import edu.unlp.la.justa.models.Cart
import edu.unlp.la.justa.models.General
import edu.unlp.la.justa.models.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service

@Service
interface CartRepository : CrudRepository<Cart, Long>, CartCustomRepository {
    @Query(value = """SELECT * FROM cart c WHERE c.user_id = :userId ORDER BY c.created_at desc limit 1""", nativeQuery = true)
    fun findCartByUser(@Param(value = "userId") userId: Long): Cart

    fun countByGeneral(general: General): Long

    fun countByUser(user: User): Long
    
    fun findByGeneralIdAndDeletedAtNotNull(generalId: Long): List<Cart>

    fun findCartByGeneralId(generalId: Long): List<Cart>

    fun findCartByGeneralAndCanceledFalse(general: General): List<Cart>
}

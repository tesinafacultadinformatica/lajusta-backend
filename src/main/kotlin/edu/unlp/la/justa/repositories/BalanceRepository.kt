package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.BalanceCustomRepository
import edu.unlp.la.justa.models.Balance
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Service

@Service
interface BalanceRepository : CrudRepository<Balance, Long>, BalanceCustomRepository {
    @Query("from Balance bal where bal.dateBalance is not null OR bal.general.id = :generalId")
    fun findHistoryAndActual(@Param("generalId") generalId: Long): List<Balance>
}

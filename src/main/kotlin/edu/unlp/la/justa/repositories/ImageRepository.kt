package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.models.Image
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface ImageRepository : CrudRepository<Image, Long>
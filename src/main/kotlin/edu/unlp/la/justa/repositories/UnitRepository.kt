package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.UnitCustomRepository
import edu.unlp.la.justa.models.Unit
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface UnitRepository : CrudRepository<Unit, Long>, UnitCustomRepository {
}
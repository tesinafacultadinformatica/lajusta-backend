package edu.unlp.la.justa.repositories

import edu.unlp.la.justa.customRepositories.UserCustomRepository
import edu.unlp.la.justa.models.User
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Service

@Service
interface UserRepository : CrudRepository<User, Long>, JpaSpecificationExecutor<User>, UserCustomRepository {
    fun findByEmail(email: String): User

    @Query("FROM User u INNER JOIN UserToken ut ON u.email = ut.username WHERE ut.token = :token")
    fun findUserByToken(token: String): User
}
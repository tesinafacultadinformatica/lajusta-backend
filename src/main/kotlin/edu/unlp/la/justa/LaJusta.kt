package edu.unlp.la.justa

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import springfox.documentation.swagger2.annotations.EnableSwagger2


@SpringBootApplication
@EnableSwagger2
@EnableWebMvc
class LaJusta

fun String.cutProperties(char: Char) = this.split(char)[0] to this.split(char)[1]

fun main(args: Array<String>) {
	runApplication<LaJusta>(*args)
}

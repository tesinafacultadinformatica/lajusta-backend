package edu.unlp.la.justa.models


import edu.unlp.la.justa.dtos.PurchaseProducerDTO
import org.hibernate.annotations.DynamicUpdate
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@DynamicUpdate
class PurchaseProducer (
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @ManyToOne  val general: General,
        @ManyToOne val product: Product? = null,
        @ManyToOne val producer: Producer? = null,
        var quantity: Int = 0,
        val aditional: Int = 0,
        var price: BigDecimal = BigDecimal.ZERO,
        var differencial: BigDecimal = BigDecimal.ZERO,
        var total: BigDecimal = BigDecimal.ZERO,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) {

        companion object {
            fun createPurchaseProducer(purchaseProducerDTO: PurchaseProducerDTO): PurchaseProducer {
                return PurchaseProducer(id = purchaseProducerDTO.id,
                    general = General.createGeneral(purchaseProducerDTO.general!!),
                    product = Product.createProduct(purchaseProducerDTO.product!!),
                    producer = Producer.createProducer(purchaseProducerDTO.producer!!),
                    quantity= purchaseProducerDTO.quantity,
                    aditional = purchaseProducerDTO.aditional,
                    price = purchaseProducerDTO.price!!,
                    total = (purchaseProducerDTO.quantity + purchaseProducerDTO.aditional).toBigDecimal() * purchaseProducerDTO.price!!
               )
            }
        }
    }
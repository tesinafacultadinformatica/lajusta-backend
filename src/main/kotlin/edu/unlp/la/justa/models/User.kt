package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.UserDTO
import org.hibernate.annotations.BatchSize
import org.hibernate.annotations.DynamicUpdate
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.math.BigDecimal
import java.time.LocalDate
import java.time.Month
import java.time.ZoneId
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.NamedQueries
import javax.persistence.NamedQuery
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.OrderBy

@Entity
@DynamicUpdate
class User(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
        @OneToOne var role: Role? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var encryptedPassword: String? = null,
        var email: String? = null,
        val description: String? = null,
        @OneToOne var image: Image? = null,
        var phone: String? = null,
        val age: Int? = null,
        @OneToOne(cascade = [CascadeType.ALL]) val address: Address? = null,
        @OneToOne(cascade = [CascadeType.ALL]) val deliveryAddress: Address? = null,
        val emailVerifiedAt: Date? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) : UserDetails {

    companion object {
        fun createUser(userDTO: UserDTO?): User? {
            var user: User? = null
            userDTO?.let {
                user = User(id = it.id,
                        role = Role(id = it.role),
                        firstName = it.firstName,
                        lastName = it.lastName,
                        encryptedPassword = it.encryptedPassword,
                        email = it.email,
                        description = it.description,
                        image = if (it.image != null) Image.createImage(it.image) else null,
                        phone = it.phone,
                        address = Address.createAddress(it.address),
                        deliveryAddress = Address.createAddress(it.deliveryAddress),
                        emailVerifiedAt = it.emailVerifiedAt)
            }
            return user
        }

        fun createUser(userDTO: UserDTO?, image: Image?): User? {
            var user: User? = null
            userDTO?.let {
                user = User(id = it.id,
                        role = Role(id = 2),
                        firstName = it.firstName,
                        lastName = it.lastName,
                        encryptedPassword = it.encryptedPassword,
                        email = it.email,
                        description = it.description,
                        image = image,
                        phone = it.phone,
                        address = Address.createAddress(it.address),
                        deliveryAddress = Address.createAddress(it.deliveryAddress),
                        emailVerifiedAt = it.emailVerifiedAt)
            }
            return user
        }
    }


    fun isPersonal() = role?.id != 2L

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        TODO("Not yet implemented")
    }

    override fun isEnabled(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getUsername() = email

    override fun isCredentialsNonExpired(): Boolean {
        TODO("Not yet implemented")
    }

    override fun getPassword() = encryptedPassword

    override fun isAccountNonExpired(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isAccountNonLocked(): Boolean {
        TODO("Not yet implemented")
    }

}

package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.ProductDTO
import org.hibernate.annotations.DynamicUpdate
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@DynamicUpdate
class Product(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        val title: String? = null,
        val description: String? = null,
        val brand: String? = null,
        val unitQuantity: Int? = null,
        val unitDescription: String? = null,
        val buyPrice: BigDecimal? = null,
        val price: BigDecimal? = null,
        val needCold: Boolean? = null,
        var stock: Int? = null,
        val isPromotion: Boolean? = null,
        val percent: Int? = null,
        var deletedAt: Date? = null,
        var createdAt: Date? = null,
        var updatedAt: Date? = null,
        @ManyToOne val producer: Producer? = null,
        @ManyToOne val unit: Unit? = null,
        @ManyToOne val user: User? = null,
        @ManyToMany(fetch = FetchType.EAGER) @JoinTable(name = "product_categories") val categories: Set<Category>? = null,
        @ManyToMany(fetch = FetchType.EAGER) @JoinTable(name = "product_image") var images: Set<Image>? = null) {
    companion object {
        fun createProduct(productDTO: ProductDTO): Product {
            return Product(id = productDTO.id,
                    user = User(id = productDTO.user?.id),
                    producer = if (productDTO.producer != null) Producer( id = (productDTO.producer as Int).toLong()) else null,
                    categories = productDTO.categories?.map { Category(it.id) }?.toSet(),
                    title = productDTO.title,
                    description = productDTO.description,
                    brand = productDTO.brand,
                    unit = if(productDTO.unit == null) null else Unit((productDTO.unit as Int).toLong()),
                    unitQuantity = productDTO.unitQuantity,
                    unitDescription = productDTO.unitDescription,
                    buyPrice = productDTO.buyPrice,
                    price = productDTO.price,
                    needCold = productDTO.needCold,
                    stock = productDTO.stock,
                    isPromotion = productDTO.isPromotion,
                    percent = productDTO.percent)
        }
    }
}
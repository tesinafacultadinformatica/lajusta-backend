package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.ProducerDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.*
import javax.persistence.*

@Entity
@DynamicUpdate
class Producer(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        val firstName: String? = null,
        val lastName: String? = null,
        val email: String? = null,
        val origin: String? = null,
        val description: String? = null,
        val phone: String? = null,
        val youtube: String? = null,
        val isCompany: Boolean = false,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null,
        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "producer") var productCataloge: Set<ProductCataloge>? = null,
        @ManyToMany(fetch = FetchType.EAGER) @JoinTable(name = "producer_image") var images: Set<Image>? = null,
        @ManyToMany(fetch = FetchType.EAGER) @JoinTable(name = "producer_tag_producer") var tags: Set<Tag>? = null,
        @OneToOne(cascade = [CascadeType.ALL]) val address: Address? = null,
        @OneToMany(fetch = FetchType.EAGER, mappedBy = "producer") var products: Set<Product>? = null) {
    companion object {

        fun createProducer(producerDTO: ProducerDTO): Producer {
            return Producer(id = producerDTO.id,
                    firstName = producerDTO.name,
                    lastName = producerDTO.lastName,
                    email = producerDTO.email,
                    origin = producerDTO.origin,
                    description = producerDTO.description,
                    phone = producerDTO.phone,
                    address = Address.createAddress(producerDTO.address),
                    isCompany = producerDTO.isCompany,
                    youtube = producerDTO.youtubeVideoId,
                    tags = producerDTO.tags?.map { Tag(it.id, it.description) }?.toSet(),
                    productCataloge = producerDTO.productCataloge?.mapNotNull { ProductCataloge.createProductCataloge(it) }?.toSet(),
                    products = producerDTO.products?.map { Product.createProduct(it) }?.toSet()
            )
        }
    }
}
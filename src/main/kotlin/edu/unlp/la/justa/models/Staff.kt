package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.StaffDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.*
import javax.persistence.*

@Entity
@DynamicUpdate
class Staff(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @ManyToOne var user: User? = null,
        val description: String? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) {

    companion object {
        fun createStaff(staffDTO: StaffDTO?): Staff? {
            var staff = Staff()
            staffDTO?.let {
                staff = Staff(it.id,
                        User(id = it.user),
                        it.description)
            }
            return staff
        }
    }
}
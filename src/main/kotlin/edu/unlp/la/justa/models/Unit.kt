package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.UnitDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
@DynamicUpdate
class Unit(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        val code: String? = null,
        val description: String? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) {

    companion object {
        fun createUnit(unitDTO: UnitDTO?): Unit? {
            var unit = Unit()
            unitDTO?.let {
                unit = Unit(it.id, it.code, it.description)
            }
            return unit
        }
    }
}
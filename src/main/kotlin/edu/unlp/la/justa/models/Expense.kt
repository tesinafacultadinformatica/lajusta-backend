package edu.unlp.la.justa.models


import edu.unlp.la.justa.dtos.ExpenseDTO
import org.hibernate.annotations.DynamicUpdate
import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
@DynamicUpdate
class Expense(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @ManyToOne var general: General? = null,
        @ManyToOne val staff: Staff? = null,
        val description: String? = null,
        val quantity: Int? = null,
        val price: BigDecimal? = null,
        var total: BigDecimal? = null,
        val date: Date? = null,
        val balanceDate: Date? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null
) {

    companion object {
        fun createExpense(expenseDTO: ExpenseDTO?): Expense? {
            var expense: Expense? = null
            expenseDTO?.let {
                expense = Expense(id = it.id, general = null, staff = Staff(id = (it.staff as Int).toLong()), quantity = it.quantity, description = it.description, price = it.price, total = it.total,
                        date = it.date, balanceDate = it.balanceDate)
            }
            return expense
        }
    }
}
package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.AddressDTO
import edu.unlp.la.justa.dtos.AvailableNodeDTO
import edu.unlp.la.justa.services.GeneralService
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import kotlin.collections.LinkedHashMap

@Entity
class AvailableNode(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @ManyToOne var node: Node? = null,
        @ManyToOne var general: General? = null,
        var dateOpen: Date? = null,
        var dateClose: Date? = null) {
    companion object {
        fun createAvailableNode(availableNodeDto: AvailableNodeDTO?): AvailableNode? {
            var availableNode: AvailableNode? = null
            availableNodeDto?.let {
                val calendar = Calendar.getInstance()
                val openDate = GeneralService.getOpenAndCloseDate(calendar, availableNodeDto.day ?: Date(), availableNodeDto.dateTimeFrom ?: "00:00")
                val closeDate = GeneralService.getOpenAndCloseDate(calendar, availableNodeDto.day ?: Date(), availableNodeDto.dateTimeTo ?: "00:00")
                availableNode = AvailableNode(
                    id = it.id,
                    node = Node( id = ((it.node as LinkedHashMap<*, *>).get("id") as Int).toLong(),
                        address= Address.createAddress((it.node as LinkedHashMap<*, *>).get("address") as LinkedHashMap<*,*>)),
                    dateOpen = openDate,
                    dateClose = closeDate)
            }
            return availableNode
        }
    }
}



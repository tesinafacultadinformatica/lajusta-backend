package edu.unlp.la.justa.models

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class ResetPassword(
  @Id val email: String? = null,
  var resetCode: String? = null,
  var timeToLive: Date? = null
)
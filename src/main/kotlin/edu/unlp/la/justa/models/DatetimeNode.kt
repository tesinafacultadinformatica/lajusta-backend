package edu.unlp.la.justa.models

import java.util.Date

class DatetimeNode(val dateTime: Date? = null)
package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.NodeDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
@DynamicUpdate
class Node(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        var name: String? = null,
        @OneToOne(cascade = [CascadeType.ALL]) val address: Address? = null,
        @OneToOne var image: Image? = null,
        val description: String? = null,
        val phone: String? = null,
        val hasFridge: Boolean? = false,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) {

    companion object {
        fun createNode(nodeDTO: NodeDTO?): Node? {
            var node = Node()
            nodeDTO?.let {
                node = Node(it.id, it.name, Address.createAddress(it.address), if (it.image != null) Image.createImage(it.image) else null,
                        it.description, it.phone, it.hasFridge)
            }
            return node
        }

        fun createNode(nodeDTO: NodeDTO?, image: Image?): Node? {
            var node = Node()
            nodeDTO?.let { node = Node(it.id, it.name, Address.createAddress(it.address), image, it.description, it.phone, it.hasFridge) }
            return node
        }
    }
}
package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.BannerDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
@DynamicUpdate
class Banner(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        val title: String? = null,
        val subtitle: String? = null,
        val text: String? = null,
        @OneToOne val image: Image? = null,
        val url: String? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) {

    companion object {
        fun createBanner(bannerDTO: BannerDTO?): Banner? {
            var banner = Banner()
            bannerDTO?.let {
                banner = Banner(it.id, it.title, it.subtitle, it.text, Image.createImage(it.image), it.url)
            }
            return banner
        }

        fun createBanner(bannerDTO: BannerDTO?, image: Image?): Banner? {
            var banner = Banner()
            bannerDTO?.let { banner = Banner(it.id, it.title, it.subtitle, it.text, image, it.url) }
            return banner
        }
    }
}
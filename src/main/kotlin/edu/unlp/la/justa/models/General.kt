package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.GeneralDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Entity
@DynamicUpdate
class General(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @OneToOne var user: User? = null,
        var dateDownPage: Date? = null,
        val dateStartUpPage: Date? = null,
        @OneToOne(mappedBy = "general") val balance: Balance? = null,
        var closedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null,
        var deletedAt: Date? = null,
        @OneToMany(cascade = [CascadeType.ALL], mappedBy = "general") var nodes: List<AvailableNode>? = null) {
    companion object {
        fun createGeneral(generalDTO: GeneralDTO): General {
            return General(generalDTO.id, if (generalDTO.user != null) User.createUser(generalDTO.user) else null, generalDTO.dateDownPage, generalDTO.dateActivePage)
        }
    }
}
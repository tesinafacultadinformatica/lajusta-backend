package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.NewsletterDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
@DynamicUpdate
class Newsletter(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        val email: String? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) {

    companion object {
        fun createNewsletter(newsletterDTO: NewsletterDTO?): Newsletter? {
            var newsletter = Newsletter()
            newsletterDTO?.let {
                newsletter = Newsletter(it.id, it.email)
            }
            return newsletter
        }
    }
}
package edu.unlp.la.justa.models


import edu.unlp.la.justa.dtos.BalanceDTO
import org.hibernate.annotations.DynamicUpdate
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@DynamicUpdate
class Balance(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
    @OneToOne  val general: General,
    var totalSale: BigDecimal? = BigDecimal.ZERO,
    var totalExpenses: BigDecimal? = BigDecimal.ZERO,
    var totalPurchaseProducer: BigDecimal? = BigDecimal.ZERO,
    var dateBalance: Date? = null,
    var openDate: Date? = null,
    var deletedAt: Date? = null,
    var updatedAt: Date? = null,
    var createdAt: Date? = null
    ){
        companion object {
            fun createBalance(balanceDTO: BalanceDTO): Balance {
                return Balance(id = balanceDTO.id,
                        general = General.createGeneral(balanceDTO.general!!),
                        totalSale= balanceDTO.totalSale,
                        totalExpenses = balanceDTO.totalExpenses,
                        totalPurchaseProducer = balanceDTO.totalPurchaseProducer,
                        dateBalance = balanceDTO.dateBalance,
                        openDate = balanceDTO.openDate
               )
            }
        }
    }
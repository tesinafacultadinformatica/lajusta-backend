package edu.unlp.la.justa.models

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class UserToken(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        val username: String,
        var token: String,
        var timeToLive: Date? = null)
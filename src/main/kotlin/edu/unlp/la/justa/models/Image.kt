package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.ImageDTO
import org.hibernate.annotations.DynamicUpdate
import java.util.Date
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
@DynamicUpdate
class Image(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        var route: String? = null,
        var isMain: Boolean? = false,
        val type: String? = null,
        var createdAt: Date? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null
) {
    companion object {
        fun createImage(imageDTO: ImageDTO?): Image? {
            var image = Image()
            imageDTO?.let { image = Image(it.id, null, it.isMain, it.type) }
            return image
        }
    }
}
package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.CategoryDTO
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToOne

@Entity
class   Category(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @ManyToOne val parent: Category? = null,
     //   @OneToMany(mappedBy = "parent") val subCategories: Set<Category>? = null,
        val level: Int = 0,
        @OneToOne(cascade = [CascadeType.ALL]) var image: Image? = null,
        val name: String? = null,
        @ManyToMany(mappedBy = "categories") val products: Set<Product>? = null,
        var createdAt: Date? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null
) {
    companion object {
        fun createCategory(categoryDTO: CategoryDTO): Category {
            return Category(id = categoryDTO.id,
                    name = categoryDTO.name,
                    level = categoryDTO.level ?: 0,
                    parent = if (categoryDTO.parent != null && categoryDTO.parent.id != null ) Category(id = categoryDTO.parent.id) else null,
                   // subCategories = if (categoryDTO.subCategories != null) categoryDTO.subCategories.map { Category(id = it.id) }.toSet() else null,
                    image = Image.createImage(categoryDTO.image),
                    createdAt = Date(),
                    deletedAt = null,
                    updatedAt = Date())
        }

        fun createCategoriesList(categoriesDTO: Set<CategoryDTO>): Set<Category> {
            return categoriesDTO.map { createCategory(it) }.toSet()
        }
    }
}
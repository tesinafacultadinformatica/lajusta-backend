package edu.unlp.la.justa.models

import com.fasterxml.jackson.annotation.JsonIgnore
import edu.unlp.la.justa.dtos.CartProductDTO
import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class CartProduct(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @JsonIgnore @ManyToOne var cart: Cart? = null,
        @ManyToOne val product: Product? = null,
        val quantity: Int,
        var price: BigDecimal? = null,
        var buyPrice: BigDecimal? = null,
        val isCanceled: Boolean = false) {
    companion object {
        fun createCartProduct(cartProductDTO: CartProductDTO): CartProduct {
            return CartProduct(cartProductDTO.id,
                    if (cartProductDTO.cart != null) Cart.createCart(cartProductDTO.cart) else null,
                    if(cartProductDTO.product != null) Product.createProduct(cartProductDTO.product) else null,
                    cartProductDTO.quantity,
                    cartProductDTO.price,
                    cartProductDTO.buyPrice,
                    cartProductDTO.isCanceled)
        }

        fun createCartProductsSet(cartProducts: Set<CartProductDTO>): Set<CartProduct> {
            return cartProducts.map { createCartProduct(it) }.toSet()
        }
    }
    fun getPriceOfProduct(): BigDecimal?{
        return if (cart?.staffDiscount == true) buyPrice else price

    }

    fun getTotalPrice(): BigDecimal?{
        return getPriceOfProduct()?.multiply(quantity.toBigDecimal())
    }
}
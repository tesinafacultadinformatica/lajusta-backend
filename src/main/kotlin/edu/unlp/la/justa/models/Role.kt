package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.RoleDTO
import org.hibernate.annotations.DynamicUpdate
import org.springframework.security.core.GrantedAuthority
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
@DynamicUpdate
class Role(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        val description: String? = null,
        val createdAt: Date? = null,
        var updatedAt: Date? = null,
        var deletedAt: Date? = null): GrantedAuthority {

    companion object {
        fun createRole(roleDTO: RoleDTO): Role {
            return Role(roleDTO.id, roleDTO.description, Date(), Date(), null)
        }

        fun createDeletedRol(roleDTO: RoleDTO): Role {
            return Role(roleDTO.id, roleDTO.description, Date(), Date(), Date())
        }
    }

    override fun getAuthority() = description?.toUpperCase()
}
package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.AddressDTO
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class Address(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        var street: String? = null,
        var number: String? = null,
        var floor: String? = null,
        var apartment: String? = null,
        var betweenStreets: String? = null,
        var description: String? = null,
        var latitude: Double? = null,
        var longitude: Double? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null) {

    companion object {
        fun createAddress(addressDTO: AddressDTO?): Address? {
            var address: Address? = null
            addressDTO?.let {
                address = Address(id = it.id, street = it.street, number = it.number, floor = it.floor, apartment = it.apartment,
                        betweenStreets = it.betweenStreets, description = it.description, latitude = it.latitude, longitude = it.longitude)
            }
            return address
        }
        fun createAddress(list: LinkedHashMap<*, *> ): Address? {
            list.get("id").let {
                val address = Address(
                    id = (it as Int).toLong()
                )
                list.get("street").let { street -> address.street = street as String? }
                list.get("betweenStreets").let { betweenStreets -> address.betweenStreets = betweenStreets as String? }
                list.get("number").let { number -> address.number = number as String? }
                list.get("latitude").let { latitude -> address.latitude = latitude as Double? }
                list.get("longitude").let { longitude ->  address.longitude = longitude as Double? }
                return address
            }
            return null

        }
    }
}
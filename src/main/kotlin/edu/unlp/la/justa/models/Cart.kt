package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.CartDTO
import org.hibernate.annotations.JoinFormula
import java.math.BigDecimal
import java.time.LocalDate
import java.time.Month
import java.time.ZoneId
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
class Cart(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @ManyToOne val user: User? = null,
        @ManyToOne var general: General? = null,
        @ManyToOne var nodeAvailable: AvailableNode? = null,
        @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], mappedBy = "cart") var cartProducts: Set<CartProduct>,
        var saleDate: Date? = null,
        var total: BigDecimal? = null,
        val possibleDeliveryDate: Date? = null,
        var staffDiscount: Boolean = false,
        var canceled: Boolean = false,
        val observation: String? = null,
        var deletedAt: Date? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null
) {
    companion object {
        fun createCart(cartDTO: CartDTO): Cart {
            return Cart(
                id = cartDTO.id,
                user = User.createUser(cartDTO.user),
//                    general = General.createGeneral(cartDTO.general),
                nodeAvailable = AvailableNode.createAvailableNode(cartDTO.nodeDate),
                saleDate = cartDTO.saleDate,
                observation = cartDTO.observation,
                canceled = cartDTO.canceled,
                cartProducts = CartProduct.createCartProductsSet(cartDTO.cartProducts),
                staffDiscount = cartDTO.staffDiscount,
                total = cartDTO.total
            )
        }
    }

    fun getTotalPrice(): BigDecimal? {
        return cartProducts.stream().map { getTotalPriceOfProduct(it) }.reduce { acc: BigDecimal?, prodPrice: BigDecimal? -> acc?.add(prodPrice) }.orElse(
            BigDecimal.ZERO)
    }

    fun getPriceOfProduct(cartProduct: CartProduct): BigDecimal? = cartProduct.getPriceOfProduct()

    fun getTotalPriceOfProduct(cartProduct: CartProduct): BigDecimal? = cartProduct.getTotalPrice()



}
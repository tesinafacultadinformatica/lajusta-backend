package edu.unlp.la.justa.models

import edu.unlp.la.justa.dtos.ProductCatalogeDTO
import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "producer_product_cataloge")
class ProductCataloge(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null,
        @OneToOne val product: Product? = null,
        @ManyToOne val producer: Producer? = null,
        var price: BigDecimal? = null,
        var quantity: Int? = null,
        var updatedAt: Date? = null,
        var createdAt: Date? = null
) {
    companion object {
        fun createProductCataloge(productCatalogeDto: ProductCatalogeDTO?): ProductCataloge? {
            var productCataloge: ProductCataloge? = null
            productCatalogeDto?.let {
                productCataloge = ProductCataloge(
                        id = it.id,
                        price = it.price,
                        quantity = it.quantity,
                        product = Product(id = it.product?.id)
                )
            }
            return productCataloge
        }
    }
}
package edu.unlp.la.justa.dtos

import edu.unlp.la.justa.models.Staff
import org.jetbrains.annotations.NotNull
import java.util.*

class StaffDTO(
        var id: Long? = null,
        val user: Long? = null,
        val deletedAt: Date? = null,
        @NotNull val description: String? = null) {

    companion object {
        fun createStaffDTO(staff: Staff?): StaffDTO? {
            var staffDto = StaffDTO()
            staff?.let { staffDto = StaffDTO(it.id, it.user?.id, it.deletedAt, it.description) }
            return staffDto
        }
    }
}
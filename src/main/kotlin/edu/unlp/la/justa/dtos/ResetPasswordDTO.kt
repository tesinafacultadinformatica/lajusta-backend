package edu.unlp.la.justa.dtos

class ResetPasswordDTO(val email: String, val code: String,val newPassword: String? = null,val oldPassword: String? = null)
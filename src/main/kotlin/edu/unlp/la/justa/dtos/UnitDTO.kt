package edu.unlp.la.justa.dtos

import edu.unlp.la.justa.models.Unit
import org.jetbrains.annotations.NotNull
import java.util.*

class UnitDTO(
        var id: Long? = null,
        var deletedAt: Date? = null,
        @NotNull val code: String? = null,
        @NotNull val description: String? = null) {

    companion object {
        fun createUnitDTO(unit: Unit?): UnitDTO? {
            var unitDto = UnitDTO()
            unit?.let { unitDto = UnitDTO(id = it.id,
                    code = it.code,
                    description = it.description,
                    deletedAt = it.deletedAt)
            }
            return unitDto
        }
    }
}
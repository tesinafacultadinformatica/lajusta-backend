package edu.unlp.la.justa.dtos

import edu.unlp.la.justa.models.Banner
import org.jetbrains.annotations.NotNull
import java.util.*

class BannerDTO(
        var id: Long? = null,
        val title: String? = null,
        val subtitle: String? = null,
        val text: String? = null,
        val deletedAt: Date? = null,
        @NotNull val image: ImageDTO? = null,
        val url: String? = null) {

    companion object {
        fun createBannerDTO(banner: Banner?): BannerDTO {
            var bannerDto = BannerDTO()
            banner?.let {
                bannerDto = BannerDTO(id = it.id,
                        title = it.title,
                        subtitle = it.subtitle,
                        image = ImageDTO(id = it.image?.id, value = it.image?.route),
                        text = it.text,
                        url = it.url,
                        deletedAt = it.deletedAt)
            }
            return bannerDto
        }
    }
}
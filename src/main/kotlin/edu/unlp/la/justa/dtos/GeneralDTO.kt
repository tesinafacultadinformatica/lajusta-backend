package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonFormat
import edu.unlp.la.justa.models.General
import org.jetbrains.annotations.NotNull
import java.util.*

class GeneralDTO(
        val id: Long? = null,
        @NotNull val user: UserDTO? = null,
        var activeNodes: Set<AvailableNodeDTO>? = null,
        var deletedAt: Date? = null,
        var canClose: Boolean = false,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'")
        val dateDownPage: Date? = null,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'")
        val dateActivePage: Date? = null) {
    companion object {
        fun createGeneralDTO(general: General?): GeneralDTO? {
            var generalDto = GeneralDTO()
            general?.let {
                val calendar = Calendar.getInstance()
                generalDto = GeneralDTO(
                        id = it.id,
                        user = UserDTO(id = it.user?.id, name = "${it.user?.firstName} ${it.user?.lastName}"),
                        activeNodes = it.nodes?.map {
                            calendar.time = it.dateOpen
                            val hourOpen = calendar.get(Calendar.HOUR_OF_DAY)
                            calendar.time = it.dateClose
                            val hourClose = calendar.get(Calendar.HOUR_OF_DAY)
                            AvailableNodeDTO(id = it.id, node = NodeDTO(id = it.node?.id), dateTimeFrom = "${hourOpen}:00", dateTimeTo = "${hourClose}:00", day = it.dateClose)
                        }?.toSet(),
                        dateDownPage = it.dateDownPage,
                        dateActivePage = it.dateStartUpPage,
                        deletedAt = it.deletedAt)
            }
            return generalDto
        }
    }
}
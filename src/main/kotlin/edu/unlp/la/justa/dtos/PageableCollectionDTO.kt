package edu.unlp.la.justa.dtos

class PageableCollectionDTO(val totalElements: Long, val page: Set<Any>)
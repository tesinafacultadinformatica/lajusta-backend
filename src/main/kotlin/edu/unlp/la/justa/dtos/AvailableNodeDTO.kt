package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

class AvailableNodeDTO(var id: Long? = null,
                       var node: Any? = null,
                       @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'") var day: Date? = null,
                       var dateTimeFrom: String? = null,
                       var dateTimeTo: String? = null)
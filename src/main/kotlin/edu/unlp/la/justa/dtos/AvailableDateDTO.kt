package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

class AvailableDateDTO(
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'")
        val openDate: Date?,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'")
        val closeDate: Date?
)
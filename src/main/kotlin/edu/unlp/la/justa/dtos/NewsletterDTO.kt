package edu.unlp.la.justa.dtos

import edu.unlp.la.justa.models.Newsletter
import org.jetbrains.annotations.NotNull

class NewsletterDTO(
        var id: Long? = null,
        @NotNull val email: String? = null) {

    companion object {
        fun createNewsletterDTO(newsletter: Newsletter?): NewsletterDTO? {
            var newsletterDto = NewsletterDTO()
            newsletter?.let { newsletterDto = NewsletterDTO(it.id, it.email) }
            return newsletterDto
        }
    }
}
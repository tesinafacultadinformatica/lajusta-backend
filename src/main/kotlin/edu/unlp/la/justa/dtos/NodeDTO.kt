package edu.unlp.la.justa.dtos

import edu.unlp.la.justa.models.Node
import org.jetbrains.annotations.NotNull
import java.util.*

class NodeDTO(
        var id: Long? = null,
        @NotNull val name: String? = null,
        @NotNull val address: AddressDTO? = null,
        val image: ImageDTO? = null,
        val description: String? = null,
        val phone: String? = null,
        val deletedAt: Date? = null,
        @NotNull val hasFridge: Boolean? = null) {

    companion object {
        fun createNodeDTO(node: Node?, sendImage: Boolean = true): NodeDTO {
            var nodeDto = NodeDTO()
            node?.let {
                nodeDto = NodeDTO(id = it.id,
                        name = it.name,
                        image = if(sendImage) ImageDTO.createImageDTO(it.image) else null,
                        address = AddressDTO.createAddressDTO(it.address),
                        description = it.description,
                        phone = it.phone,
                        hasFridge = it.hasFridge,
                        deletedAt = it.deletedAt
                )
            }
            return nodeDto
        }

        fun createNodeDTO(node: Node?, base64: String?): NodeDTO {
            var nodeDto = NodeDTO()
            node?.let {
                nodeDto = NodeDTO(id = it.id,
                        name = it.name,
                        image = ImageDTO(id = it.image?.id, value = base64),
                        address = AddressDTO.createAddressDTO(it.address),
                        description = it.description,
                        phone = it.phone,
                        hasFridge = it.hasFridge,
                        deletedAt = it.deletedAt)
            }
            return nodeDto
        }
    }
}
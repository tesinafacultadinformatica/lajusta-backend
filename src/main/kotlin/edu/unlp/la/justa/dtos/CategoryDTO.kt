package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.Category
import org.jetbrains.annotations.NotNull
import java.util.*

@JsonInclude(JsonInclude.Include.ALWAYS)
class CategoryDTO(
        var id: Long? = null,
        @NotNull var name: String? = null,
        var image: ImageDTO? = null,
        val level: Int? = null,
        val parent: CategoryDTO? = null,
        val deletedAt: Date? = null
) {
    companion object {
        fun createCategoryDTO(category: Category): CategoryDTO {
            return CategoryDTO(id = category.id,
                    name = category.name,
                    image = ImageDTO.createImageDTO(category.image),
                    parent = if (category.parent != null) CategoryDTO(id = category.parent.id) else null,
                    deletedAt = category.deletedAt)
        }
    }
}
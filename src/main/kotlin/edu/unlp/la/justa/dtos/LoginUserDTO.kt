package edu.unlp.la.justa.dtos

class LoginUserDTO(val userName: String, val userPassword: String)
package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.Cart
import edu.unlp.la.justa.models.User
import java.util.*

@JsonInclude(JsonInclude.Include.ALWAYS)
class UserDTO(
        val id: Long? = null,
        val role: Long? = null,
        val firstName: String? = null,
        val lastName: String? = null,
        val name: String? = null,
        val encryptedPassword: String? = null,
        val email: String? = null,
        val description: String? = null,
        val image: ImageDTO? = null,
        val phone: String? = null,
        val address: AddressDTO? = null,
        val deliveryAddress: AddressDTO? = null,
        val cart: CartDTO? = null,
        val age: Int? = null,
        val emailVerifiedAt: Date? = null,
        val deletedAt: Date? = null) {
    companion object {
        fun createUserDTO(user: User?, cart: Cart? = null): UserDTO? {
            var userDto: UserDTO? = null
            user?.let {
                userDto = UserDTO(id = it.id,
                        firstName = it.firstName,
                        lastName = it.lastName,
                        email = it.email,
                        image = if (it.image != null) ImageDTO.createImageDTO(it.image) else null,
                        address = AddressDTO.createAddressDTO(it.address),
                        deliveryAddress = AddressDTO.createAddressDTO(it.deliveryAddress),
                        phone = it.phone,
                        cart = CartDTO.createCartDTO(cart),
                        role = it.role?.id,
                        age = it.age,
                        description = it.description,
                        deletedAt = it.deletedAt)
            }
            return userDto
        }

        fun createUserDTO(user: User?, base64: String?): UserDTO? {
            var userDto: UserDTO? = null
            user?.let {
                userDto = UserDTO(id = it.id,
                        firstName = it.firstName,
                        lastName = it.lastName,
                        email = it.email,
                        image = if (it.image != null) ImageDTO.createImageDTO(it.image, base64) else null,
                        address = AddressDTO.createAddressDTO(it.address),
                        deliveryAddress = AddressDTO.createAddressDTO(it.deliveryAddress),
                        phone = it.phone,
                        role = it.role?.id,
                        age = it.age,
                        description = it.description,
                        deletedAt = it.deletedAt)
            }
            return userDto
        }
    }
}
package edu.unlp.la.justa.dtos


import edu.unlp.la.justa.models.*
import org.jetbrains.annotations.NotNull
import java.math.BigDecimal
import java.util.*

class PurchaseProducerDTO(
        var id: Long? = null,
        @NotNull val general: GeneralDTO? = null,
        @NotNull val product: ProductDTO? = null,
        @NotNull val producer: ProducerDTO? = null,
        val quantity: Int = 0,
        val aditional: Int = 0,
        var price: BigDecimal? = null,
        var total: BigDecimal? = null,
        val deletedAt: Date? = null) {

    companion object {
        fun createPurchaseProducerDTO(purchaseProducer: PurchaseProducer?): PurchaseProducerDTO? {
            var purchaseProducerDTO = PurchaseProducerDTO()
            purchaseProducer?.let { purchaseProducerDTO = PurchaseProducerDTO(id = it.id,
                    general = GeneralDTO(id = it.general.id),
                    product = ProductDTO(id = it.product?.id, title = it.product?.title),
                    producer = ProducerDTO(id = it.product?.producer?.id, name = it.product?.producer?.firstName),
                    quantity = it.quantity,
                    aditional = it.aditional,
                    price = it.price,
                    total = it.total,
                    deletedAt = it.deletedAt)
            }
            return purchaseProducerDTO
        }
    }
}
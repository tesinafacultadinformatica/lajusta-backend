package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.Address

@JsonInclude(JsonInclude.Include.NON_NULL)
class AddressDTO(
        val id: Long? = null,
        val street: String? = null,
        val number: String? = null,
        val floor: String? = null,
        val apartment: String? = null,
        val betweenStreets: String? = null,
        val description: String? = null,
        val latitude: Double? = null,
        val longitude: Double? = null
) {
    companion object {
        fun createAddressDTO(address: Address?): AddressDTO? {
            var addressDTO: AddressDTO? = null
            address?.let {
                addressDTO = AddressDTO(id = it.id, street = it.street, number = it.number, floor = it.floor,
                        apartment = it.apartment, betweenStreets = it.betweenStreets, description = it.description, latitude = it.latitude, longitude = it.longitude)
            }
            return addressDTO
        }
    }
}
package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.ProductCataloge
import java.math.BigDecimal

@JsonInclude(JsonInclude.Include.ALWAYS)
class ProducerYieldDTO(
        val producer: Long? = null,
        val products: Set<ProductCatalogeDTO>? = null
)
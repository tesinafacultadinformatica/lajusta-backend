package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.ProductCataloge
import java.math.BigDecimal

@JsonInclude(JsonInclude.Include.ALWAYS)
class ProductCatalogeDTO(
        val id: Long? = null,
        val product: ProductDTO? = null,
        val price: BigDecimal? = null,
        val quantity: Int? = null
) {
    companion object {
        fun createProductCatalogeDTO(productCataloge: ProductCataloge?): ProductCatalogeDTO? {
            var productCatalogeDto: ProductCatalogeDTO? = null
            productCataloge?.let {
                productCatalogeDto = ProductCatalogeDTO(
                        id = it.id,
                        price = it.price,
                        quantity = it.quantity,
                        product = ProductDTO.createProductDTO(it.product!!)
                )
            }
            return productCatalogeDto
        }
    }
}
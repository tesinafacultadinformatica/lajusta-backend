package edu.unlp.la.justa.dtos

import edu.unlp.la.justa.models.News
import org.jetbrains.annotations.NotNull
import java.util.*

class NewsDTO(
        var id: Long? = null,
        val title: String? = null,
        val subtitle: String? = null,
        val text: String? = null,
        val deletedAt: Date? = null,
        @NotNull val image: ImageDTO? = null,
        val url: String? = null) {

    companion object {
        fun createNewsDTO(news: News?): NewsDTO {
            var newsDto = NewsDTO()
            news?.let {
                newsDto = NewsDTO(id = it.id,
                        title = it.title,
                        subtitle = it.subtitle,
                        image = ImageDTO(id = it.image?.id, value = it.image?.route),
                        text = it.text,
                        url = it.url,
                        deletedAt = it.deletedAt)
            }
            return newsDto
        }

        fun createNewsDTO(news: News?, base64: String?): NewsDTO {
            var newsDto = NewsDTO()
            news?.let {
                newsDto = NewsDTO(id = it.id,
                        title = it.title,
                        subtitle = it.subtitle,
                        image = ImageDTO.createImageDTO(it.image, base64),
                        text = it.text,
                        url = it.url,
                        deletedAt = it.deletedAt)
            }
            return newsDto
        }
    }
}
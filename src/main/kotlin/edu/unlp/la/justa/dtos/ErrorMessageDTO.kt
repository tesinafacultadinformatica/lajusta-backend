package edu.unlp.la.justa.dtos

import java.util.*

class ErrorMessageDTO(val status: Int, val message: String?, val path: String, val timestamp: Date)
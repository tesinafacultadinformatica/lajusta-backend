package edu.unlp.la.justa.dtos

class ResetPasswordMessage(val message: String, val code: String? = null)
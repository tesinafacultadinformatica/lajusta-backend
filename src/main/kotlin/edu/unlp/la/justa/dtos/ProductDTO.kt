package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.Product
import org.jetbrains.annotations.NotNull
import java.math.BigDecimal
import java.util.*

@JsonInclude(JsonInclude.Include.ALWAYS)
class ProductDTO(
        val id: Long? = null,
        @NotNull val user: UserDTO? = null,
        @NotNull val producer: Any? = null,
        @NotNull val categories: Set<CategoryDTO>? = null,
        @NotNull val title: String? = null,
        val description: String? = null,
        val brand: String? = null,
        val unit: Any? = null,
        val unitQuantity: Int? = null,
        val unitDescription: String? = null,
        val buyPrice: BigDecimal? = null,
        val price: BigDecimal? = null,
        val needCold: Boolean? = null,
        var stock: Int? = null,
        val isPromotion: Boolean? = null,
        val percent: Int? = null,
        val deletedAt: Date? = null,
        var images: Set<ImageDTO>? = null
) {
    companion object {
        fun createProductDTO(product: Product): ProductDTO {
            return ProductDTO(id = product.id,
                    user = UserDTO(id = product.user?.id, firstName = product.user?.firstName),
                    producer = ProducerDTO(id = product.producer?.id, name = product.producer?.firstName),
                    title = product.title,
                    description = product.description,
                    brand = product.brand,
                    images = product.images?.map { ImageDTO(id = it.id, value = it.route) }?.toSet(),
                    unitQuantity = product.unitQuantity,
                    unit = UnitDTO(id = product.unit?.id, code = product.unit?.code, description = product.unit?.description),
                    unitDescription = product.unitDescription,
                    price = product.price,
                    stock = product.stock,
                    buyPrice = product.buyPrice,
                    isPromotion = product.isPromotion,
                    percent = product.percent,
                    needCold = product.needCold,
                    categories = product.categories?.map { CategoryDTO(id = it.id, name = it.name, image = ImageDTO(id = it.image?.id, value = it.image?.route)) }?.toSet(),
                    deletedAt = product.deletedAt)
        }
    }
}
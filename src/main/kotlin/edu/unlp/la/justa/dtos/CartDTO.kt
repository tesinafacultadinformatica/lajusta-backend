package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonFormat
import edu.unlp.la.justa.models.Cart
import org.jetbrains.annotations.NotNull
import java.math.BigDecimal
import java.util.*

class CartDTO(
        val id: Long? = null,
        @NotNull val user: UserDTO? = null,
        @NotNull val general: GeneralDTO? = null,
        @NotNull val nodeDate: AvailableNodeDTO? = null,
        @NotNull val cartProducts: Set<CartProductDTO>,
        val staffDiscount: Boolean = false,
        val total: BigDecimal? = null,
        val observation: String? = null,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'")
        val saleDate: Date? = null,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'")
        val posibleDeliveryDate: Date? = null,
        val canceled: Boolean = false,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.000'Z'")
        val deletedAt: Date? = null
) {
    companion object {
        fun createCartDTO(cart: Cart?): CartDTO? {
            val calendar = Calendar.getInstance()
            var hourOpen:Int? = null
            var hourClose: Int? = null
            var cartDto: CartDTO? = null
            cart?.let {
                cart.nodeAvailable?.let {
                    calendar.time = it.dateOpen
                    hourOpen = calendar.get(Calendar.HOUR_OF_DAY)
                    calendar.time = it.dateClose
                    hourClose = calendar.get(Calendar.HOUR_OF_DAY)
                }
                cartDto = CartDTO(id = cart.id,
                        user = UserDTO(id = cart.user?.id, firstName = cart.user?.firstName),
                        general = GeneralDTO(id = cart.general?.id),
                        nodeDate = AvailableNodeDTO(id = cart.nodeAvailable?.id,node = NodeDTO(id = cart.nodeAvailable?.node?.id, name = cart.nodeAvailable?.node?.name, address =  AddressDTO.createAddressDTO(cart.nodeAvailable?.node?.address)),
                                dateTimeFrom = "${hourOpen}:00", dateTimeTo = "${hourClose}:00", day = cart.nodeAvailable?.dateClose),
                        saleDate = cart.saleDate,
                        cartProducts = cart.cartProducts.map {
                            CartProductDTO(it.id,
                                null,
                                if(it.product != null) ProductDTO( id = it.product.id, title = it.product.title, price = it.product.price, buyPrice = it.product.buyPrice) else null,
                                it.quantity,
                                it.price,
                                it.buyPrice,
                                it.isCanceled)
                        }.toSet(),
                        staffDiscount = it.staffDiscount,
                        total = cart.total,
                        posibleDeliveryDate = cart.possibleDeliveryDate,
                        canceled = cart.canceled,
                        observation = cart.observation,
                        deletedAt = cart.deletedAt)
            }
            return cartDto
        }
    }
}
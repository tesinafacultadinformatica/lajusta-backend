package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.Producer
import org.jetbrains.annotations.NotNull
import java.util.*

@JsonInclude(JsonInclude.Include.ALWAYS)
class ProducerDTO(
        val id: Long? = null,
        @NotNull val name: String? = null,
        val lastName: String? = null,
        val email: String? = null,
        val origin: String? = null,
        val description: String? = null,
        val images: Set<ImageDTO>? = null,
        val phone: String? = null,
        val youtubeVideoId: String? = null,
        val deletedAt: Date? = null,
        val products: Set<ProductDTO>? = null,
        val productCataloge: Set<ProductCatalogeDTO>? = null,
        val tags: Set<TagDTO>? = null,
        @NotNull val address: AddressDTO? = null,
        @NotNull val isCompany: Boolean = false) {

    companion object {
        fun createProducerDTO(producer: Producer): ProducerDTO {
            return ProducerDTO(id = producer.id,
                    name = producer.firstName,
                    email = producer.email,
                    images = producer.images?.map { ImageDTO(id = it.id, value = it.route) }?.toSet(),
                    address = AddressDTO.createAddressDTO(producer.address),
                    origin = producer.origin,
                    description = producer.description,
                    phone = producer.phone,
                    isCompany = producer.isCompany,
                    youtubeVideoId = producer.youtube,
                    deletedAt = producer.deletedAt,
                    tags = producer.tags?.map { TagDTO(it.id, it.name) }?.toSet(),
                    products = producer.products?.map { ProductDTO.createProductDTO(it) }?.toSet(),
                    productCataloge = null
            )
        }
    }
}
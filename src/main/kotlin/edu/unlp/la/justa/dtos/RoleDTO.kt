package edu.unlp.la.justa.dtos

import edu.unlp.la.justa.models.Role
import org.jetbrains.annotations.NotNull

class RoleDTO(
        val id: Long? = null,
        @NotNull val description: String? = null) {

    companion object{
        fun createRoleDTO(role: Role) : RoleDTO{
            return RoleDTO(role.id, role.description)
        }
    }
}
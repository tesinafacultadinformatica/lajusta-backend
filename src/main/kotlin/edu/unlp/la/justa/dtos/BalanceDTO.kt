package edu.unlp.la.justa.dtos


import edu.unlp.la.justa.models.Balance
import org.jetbrains.annotations.NotNull
import java.math.BigDecimal
import java.util.*

class BalanceDTO(
    var id: Long? = null,
    @NotNull val general: GeneralDTO? = null,
    val totalSale: BigDecimal? = null,
    val totalExpenses: BigDecimal? = null,
    val totalPurchaseProducer: BigDecimal? = null,
    val dateBalance: Date? = null,
    val openDate: Date? = null,
    val deletedAt: Date? = null) {

    companion object {
        fun createBalanceDTO(balance: Balance?): BalanceDTO? {
            var balanceDTO = BalanceDTO()
            balance?.let { balanceDTO = BalanceDTO(id = it.id,
                    general = GeneralDTO(id = it.general.id),
                    totalSale = it.totalSale,
                    totalExpenses = it.totalExpenses,
                    totalPurchaseProducer = it.totalPurchaseProducer,
                    dateBalance = it.dateBalance,
                    openDate = it.openDate,
                    deletedAt = it.deletedAt)
            }
            return balanceDTO
        }
    }
}
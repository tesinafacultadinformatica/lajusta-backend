package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import edu.unlp.la.justa.models.Image
import org.jetbrains.annotations.NotNull

@JsonInclude(JsonInclude.Include.NON_NULL)
class ImageDTO(
        val id: Long? = null,
        @NotNull val value: String? = null,
        @NotNull val name: String? = null,
        @NotNull val type: String? = null,
        val isMain: Boolean? = null
) {
    companion object {
        fun createImageDTO(image: Image?): ImageDTO? {
            var imageDto: ImageDTO? = null
            image?.let { imageDto = ImageDTO(id = it.id, type = it.type, value = it.route) }
            return imageDto
        }

        fun createImageDTO(image: Image?, base64: String?): ImageDTO? {
            var imageDto = ImageDTO()
            image?.let { imageDto = ImageDTO(id = image.id, value = base64, isMain = image.isMain, type = image.type) }
            return imageDto
        }
    }
}
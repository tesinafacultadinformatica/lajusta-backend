package edu.unlp.la.justa.dtos

import org.jetbrains.annotations.NotNull
import java.math.BigDecimal

class CartProductDTO(
        val id: Long? = null,
        val cart: CartDTO? = null,
        @NotNull val product: ProductDTO? = null,
        @NotNull val quantity: Int,
        @NotNull val price: BigDecimal? = null,
        @NotNull val buyPrice: BigDecimal? = null,
        val isCanceled: Boolean = false
)
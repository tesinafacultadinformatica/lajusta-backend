package edu.unlp.la.justa.dtos


import edu.unlp.la.justa.models.Expense
import org.jetbrains.annotations.NotNull
import java.math.BigDecimal
import java.util.*

class ExpenseDTO(
        var id: Long? = null,
        val general: GeneralDTO? = null,
        @NotNull val staff: Any? = null,
        val quantity: Int? = null,
        val description: String? = null,
        var price: BigDecimal? = null,
        var total: BigDecimal? = null,
        val date: Date? = null,
        val balanceDate: Date? = null,
        val deletedAt: Date? = null) {

    companion object {
        fun createExpenseDTO(expense: Expense?): ExpenseDTO? {
            var expenseDTO = ExpenseDTO()
            expense?.let { expenseDTO = ExpenseDTO(id = it.id, general = GeneralDTO(id = it.general?.id), staff = StaffDTO(id = it.staff?.id, description = it.staff?.description ), quantity = it.quantity,
                    description = it.description, price = it.price, total = it.total, date = it.date, balanceDate = it.balanceDate, deletedAt = it.deletedAt)
            }
            return expenseDTO
        }
    }
}
package edu.unlp.la.justa.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import org.jetbrains.annotations.NotNull

@JsonInclude(JsonInclude.Include.ALWAYS)
class TagDTO(val id: Long? = null, @NotNull val description: String)